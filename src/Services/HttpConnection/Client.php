<?php

namespace Mobly\MarketplaceSdk\Services\HttpConnection;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use League\Csv\Reader;
use Spatie\ArrayToXml\ArrayToXml;

class Client
{
    /**
     * @var int
     */
    protected $statusCode;

    /**
     * @param int $statusCode
     */
    public function setStatusCode(int $statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param Request $request
     * @param array $contents
     * @return array|mixed
     * @throws \League\Csv\Exception
     */
    public function jsonRequest(Request $request, array $contents)
    {
        $request->addOption(['json' => $contents]);
        return $this->request($request);
    }

    /**
     * @param Request $request
     * @param array $contents
     * @return array|mixed
     * @throws \League\Csv\Exception
     */
    public function xmlRequest(Request $request, array $contents)
    {
        $xmlContent = $xmlContent = ArrayToXml::convert($contents, 'import', true, 'utf-8');
        $request->addOption(['body' => $xmlContent]);

        return $this->request($request);
    }

    /**
     * @param Request $request
     * @param string $field
     * @param string $filename
     * @param string $fileContent
     * @return array|mixed
     * @throws \League\Csv\Exception
     */
    public function fileRequest(Request $request, string $field, string $filename, string $fileContent)
    {
        $request->addOption(['multipart' => [
            [
                'name' => $field,
                'filename' => $filename,
                'contents' => $fileContent,
            ],
        ]]);

        return $this->request($request);
    }

    /**
     * @param Request $request
     * @param array $files
     * @return array|mixed
     * @throws \League\Csv\Exception
     */
    public function multipleFilesRequest(Request $request, array $files)
    {
        $multipartContent = [];
        foreach ($files as $file) {
            $multipartContent[] = [
                'name' => 'files',
                'filename' => $file['filename'],
                'contents' => $file['contents'],
            ];
        }

        $request->addOption(['multipart' => $multipartContent]);
        return $this->request($request);
    }

    /**
     * @param Request $request
     * @param float $connect
     * @param float $timeout
     * @return array|mixed|\SimpleXMLElement
     * @throws \Exception
     */
    public function request(Request $request, $connect = 6.0, $timeout = 60.0)
    {
        try {
            $client = new GuzzleClient([
                'connect_timeout' => $connect,
                'timeout' => $timeout
            ]);

            $response = $client->request($request->getMethod(), $request->getUrl(), $request->getOptions());

            $this->setStatusCode($response->getStatusCode());

            $content = $response->getBody()->getContents();
            if ($this->isJSON($content)) {
                return json_decode($content);
            }

            if ($this->isValidXml($content)) {
                return simplexml_load_string($content);
            }

            return $this->parse_csv($content);

        } catch (GuzzleException $ex) {
            $content = $ex->getMessage();

            if (
                !empty($ex->getResponse())
                && !empty($ex->getResponse()->getBody())
                && !empty($ex->getResponse()->getBody()->getContents())
            ) {
                $ex->getResponse()->getBody()->seek(0);
                $content = json_decode($ex->getResponse()->getBody()->getContents(), true);
                $ex->getResponse()->getBody()->seek(0);

                if (isset($content['Errors']) && count($content['Errors']) > 0) {
                    $content = $content['Errors'][0]['Message'];
                }

                if (isset($content['details'])) {
                    $content = json_encode($content['details']['json']);
                }

                if (is_array($content)) {
                    $content = json_encode($content);
                }
            }

            throw new \Exception($content, $ex->getCode());
        }
    }

    /**
     * @param $string
     * @return bool
     */
    protected function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true))
        && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

    /**
     * @param $content
     * @return bool
     */
    protected function isValidXml($content)
    {
        $content = trim($content);
        if (empty($content)) {
            return false;
        }

        if (stripos($content, '<!DOCTYPE html>') !== false) {
            return false;
        }

        libxml_use_internal_errors(true);
        simplexml_load_string($content);
        $errors = libxml_get_errors();
        libxml_clear_errors();

        return empty($errors);
    }

    /**
     * @param $content
     * @return array
     * @throws \League\Csv\Exception
     */
    protected function parse_csv($content)
    {
        $results = [];
        if ($content != null) {
            $reader = Reader::createFromString($content);
            $reader->setDelimiter(';');
            $reader->setHeaderOffset(0);
            foreach ($reader->getRecords() as $record) {
                $results[] = $record;
            }
        }
        return $results;
    }
}
