<?php

namespace Mobly\MarketplaceSdk\Services\HttpConnection;

class Request
{
    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var array
     */
    protected $options;

    /**
     * Request constructor.
     * @param string $method
     * @param string|null $url
     * @param array $options
     */
    public function __construct(string $method, string $url = null, array $options = [])
    {
        $this->method = $method;
        $this->url = $url;
        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param string $method
     * @return Request
     */
    public function setMethod(string $method): Request
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param array $options
     * @return Request
     */
    public function setOptions(array $options): Request
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @param string $url
     * @return Request
     */
    public function setUrl(string $url): Request
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param string $authKey
     */
    public function setAuthKey(string $authKey)
    {
        $this->addOption(['headers' => [
            'Authorization' => $authKey,
        ]]);
    }

    /**
     * @param array $option
     */
    public function addOption(array $option)
    {
        $optionKey = key($option);

        if (isset($this->options[$optionKey])) {
            $this->options[$optionKey] = array_merge($this->options[$optionKey], $option[$optionKey]);
        } else {
            $this->options = array_merge($this->options, $option);
        }
    }
}