<?php

namespace Mobly\MarketplaceSdk\Services\V2;

use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use GuzzleHttp\Client as GuzzleClient;

class Client
{
    public function request(Request $request, $connect = 6.0, $timeout = 60.0)
    {
        $client = new GuzzleClient([
            'connect_timeout' => $connect,
            'timeout' => $timeout
        ]);

        return $client->request($request->getMethod(), $request->getUrl(), $request->getOptions());
    }
}