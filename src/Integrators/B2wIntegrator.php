<?php

namespace Mobly\MarketplaceSdk\Integrators;

use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Mobly\MarketplaceSdk\Integrators\B2w\Order;
use Mobly\MarketplaceSdk\Integrators\B2w\Price;
use Mobly\MarketplaceSdk\Integrators\B2w\Product;
use Mobly\MarketplaceSdk\Integrators\B2w\Stock;
use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use Mobly\MarketplaceSdk\Validators\B2w\B2wValidator;
use Mobly\MarketplaceSdk\Validators\B2w\PriceValidator;
use Mobly\MarketplaceSdk\Validators\B2w\ProductValidator;
use Mobly\MarketplaceSdk\Validators\B2w\Stockalidator;
use Mobly\MarketplaceSdk\Validators\B2w\StockValidator;

class B2wIntegrator extends IntegratorAbstract
{
    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function integrate(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        if ($b2wValidator->validate()) {

            $productsResults = [
                'success' => [],
            ];

            $productValidator = new ProductValidator();
            $product = new Product($this, $productValidator);
            $formattedProducts = $product->formatProducts($data);

            $client = new Client();

            foreach ($formattedProducts as $sku => $products) {
                foreach ($products as $formattedProduct) {
                    try {
                        $request = new Request($httpMethod, $data['request']['url']);
                        $request->setOptions([
                            'headers' => $data['request']['headers'],
                            'json' => ['product' => $formattedProduct],
                        ]);

                        $response = $client->request($request);

                        $productsResults['success'][$sku][] = (array)$response;
                    } catch (\Exception $exception) {
                        unset($formattedProduct[$sku]);
                        $this->addErrorsWithKey($exception->getMessage(), $sku);
                        continue;
                    }
                }
            }

            $productsResults['errors'] = $this->getErrors();

            return $productsResults;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);

        if ($b2wValidator->validate()) {

            $productsResults = [
                'success' => [],
            ];

            $productValidator = new ProductValidator();

            $product = new Product($this, $productValidator);
            $formattedProducts = $product->formatProducts($data);

            foreach ($formattedProducts as $sku => $products) {
                foreach ($products as $formattedProduct) {
                    try {
                        $request = new Request($httpMethod, $this->_makeUrl($formattedProduct['sku'],
                            $data['request']['url']));

                        $request->setOptions([
                            'headers' => $data['request']['headers'],
                            'json' => ['product' => $formattedProduct],
                        ]);

                        $client = new Client();

                        $response = $client->request($request);

                        $productsResults['success'][$sku][] = (array)$response;
                    } catch (\Exception $exception) {
                        unset($formattedProduct[$sku]);
                        $this->addErrorsWithKey($exception->getMessage(), $sku);
                        continue;
                    }
                }
            }

            $productsResults['errors'] = $this->getErrors();

            return $productsResults;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function linkSkus(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        if ($b2wValidator->validate()) {

            $productsResults = [
                'success' => [],
            ];


            try {
                $request = new Request($httpMethod, $data['request']['url']);

                $contentBody = (object) [
                    "skus"=> $data['products'],
                    "sale_system"=> "B2W",
                    "type"=> "link",
                    "specifications"=> [],
                    "previous_specifications"=> []
                ];

                $request->setOptions([
                    'headers' => $data['request']['headers'],
                    'json' => $contentBody,
                ]);

                $client = new Client();

                $response = $client->request($request);

                $productsResults['success'][] = (array)$response;
            } catch (\Exception $exception) {
                $this->addErrorsWithKey($exception->getMessage(), $data['products']);
            }

            $productsResults['errors'] = $this->getErrors();

            return $productsResults;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function checkProduct(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => [],
        ];

        $client = new Client();
        try {
            $request = new Request($httpMethod,
                $this->_makeUrl($data['product']['sku'], $data['request']['url']));

            $request->addOption([
                'headers' => $data['request']['headers'],
            ]);

            $response = $client->request($request);
            if($response->qty > 0 && $response->status == 'disabled') {
                throw new \Exception('Product active');
            }

            $productsResults['success'][$data['product']['sku']] = (array)$response;
        } catch (\Exception $exception) {
            $productsResults['errors'][$data['product']['sku']] = ['message' => $exception->getMessage()];
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     * @throws \Exception
     */
    public function getStatus(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        if ($b2wValidator->validate()) {

            $request = new Request($httpMethod, $this->_makeUrl(null, $data['request']['url']));
            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $client = new Client();

            $response = $client->request($request);

            $order = [json_decode(json_encode($response), true)];

            $orderTransf = new Order();
            $response = $orderTransf->transformOrders($order);

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getOrders(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        if ($b2wValidator->validate()) {
            $request = new Request($httpMethod, $this->_makeUrl(null, $data['request']['url']));

            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $params = [];
            if (!empty($data['request']['params']['statuses'])) {
                $params['filters[statuses][]'] = $data['request']['params']['statuses'];
            }

            if(!empty($data['request']['params']['page'])) {
                $params['page'] = $data['request']['params']['page'];
            }

            if(!empty($data['request']['params']['per_page'])) {
                $params['per_page'] = $data['request']['params']['per_page'];
            }

            if(!empty($data['request']['params']['date'])) {
                $params['filters[start_date]'] = $data['request']['params']['date'];
            }

            if(count($params)) {
                $request->addOption(['query' => $params]);
            }

            $client = new Client();

            $rawResponse = $client->request($request);
            $orders = [json_decode(json_encode($rawResponse), true)];

            $orderTransf = new Order();
            $response = $orderTransf->transformOrders($orders[0]['orders']);
            $response['raw'] = $rawResponse;

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     * @throws \Exception
     */
    public function getOrdersQueue(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        if ($b2wValidator->validate()) {
            $request = new Request($httpMethod, $this->_makeUrl(null, $data['request']['url']));

            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $client = new Client();

            $rawResponse = $client->request($request);
            $orders = !empty($rawResponse) ? [json_decode(json_encode($rawResponse), true)] : [];

            $orderTransf = new Order();
            $response = $orderTransf->transformOrders($orders);
            $response['raw'] = $rawResponse;


            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function deleteOrderQueue(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        if ($b2wValidator->validate()) {

            $request = new Request($httpMethod, $this->_makeUrl($data['order'], $data['request']['url']));
            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $client = new Client();

            $response = $client->request($request);

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     * @throws \Exception
     */
    public function getOrder(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        if ($b2wValidator->validate()) {

            $request = new Request($httpMethod, $this->_makeUrl($data['order'], $data['request']['url']));
            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $client = new Client();

            $rawResponse = $client->request($request);

            $order = [json_decode(json_encode($rawResponse), true)];

            $orderTransf = new Order();
            $response = $orderTransf->transformOrders($order);
            $response['raw'] = $rawResponse;

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function sendInvoice(string $httpMethod, array $data)
    {
        return $this->simpleRequest($httpMethod, $data);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function setOrderToShipped(string $httpMethod, array $data)
    {
        return $this->simpleRequest($httpMethod, $data);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function setOrderToDelivered(string $httpMethod, array $data)
    {
        try {
            return $this->simpleRequest($httpMethod, $data);
        } catch (\Exception $exception) {
            if (json_decode($exception->getMessage())->error === 'Transição inválida: DELIVERED -> DELIVERED') {
                return [
                    'status' => 'complete'
                ];
            }

            throw new \Exception($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function getSyncErrors(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        if ($b2wValidator->validate()) {

            $request = new Request($httpMethod, $this->_makeUrl(null, $data['request']['url']));
            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $client = new Client();

            $response = $client->request($request);
            $response = [json_decode(json_encode($response), true)];

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function patchSyncErrors(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        if ($b2wValidator->validate()) {

            $request = new Request($httpMethod, $this->_makeUrl(null, $data['request']['url']));
            $request->setOptions([
                'headers' => $data['request']['headers'],
                'json' => $data['data'],
            ]);

            $client = new Client();

            $response = $client->request($request);
            $response = [json_decode(json_encode($response), true)];

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function getURLs(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        if ($b2wValidator->validate()) {

            $request = new Request($httpMethod, $this->_makeUrl(null, $data['request']['url']));
            $request->setOptions([
                'headers' => $data['request']['headers'],
                'query' => $data['request']['params'],
            ]);

            $client = new Client();

            $response = $client->request($request);
            $response = [json_decode(json_encode($response), true)];

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function deleteProducts(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        $b2wValidator->validate();

        $productsResults = [
            'success' => [], 'errors' => [],
        ];

        $sku = $data['request']['data']['sku'];

        $request = new Request($httpMethod);
        $request->setOptions(['headers' => $data['request']['headers']]);
        $request->setUrl($this->_makeUrl($sku, $data['request']['url']));

        $client = new Client();

        try {
            $response = $client->request($request);

            $productsResults['success'][$sku][] = (array)$response;
        } catch (\Exception $exception) {
            $productsResults['errors'][$sku][] = ['message' => $exception->getMessage()];
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    private function simpleRequest(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        $b2wValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $data['request']['data'],
        ]);

        $client = new Client();

        $response = $client->request($request);

        return $response;
    }

    /**
     * @param $replaceParam
     * @param $url
     * @return mixed
     */
    private function _makeUrl($replaceParam, $url)
    {
        return str_replace('%s', $replaceParam, $url);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getProducts(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        if ($b2wValidator->validate()) {
            $request = new Request($httpMethod, $this->_makeUrl(null, $data['request']['url']));

            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $params = [];
            $params['page'] = 0;
            if(!empty($data['request']['params']['page'])) {
                $params['page'] = $data['request']['params']['page'];
            }

            if(!empty($data['request']['params']['per_page'])) {
                $params['per_page'] = $data['request']['params']['per_page'];
            }

            if(count($params)) {
                $request->addOption(['query' => $params]);
            }

            $client = new Client();

            $rawResponse = $client->request($request);
            $products = [json_decode(json_encode($rawResponse), true)];

            if(count($products)) {
                $productValidator = new ProductValidator();
                $productsTransfer = new Product($this, $productValidator);
                $response['products'] = $productsTransfer->transfProductsStore($products);
                $response['total'] = !empty($products[0]['total']) ? $products[0]['total'] : 0;
            }

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function fetchProductsByPageCursor(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);

        if (!$b2wValidator->validate()) {
            return [];
        }

        $requestData = $data['request'];
        $fetchProductsRequest = new Request($httpMethod, $this->_makeUrl(null, $requestData['url']));

        $fetchProductsRequest->setOptions([
            'headers' => $requestData['headers'],
        ]);

        $cursor = Arr::get($requestData, 'params.cursor', false);

        if($cursor !== false) {
            $fetchProductsRequest->addOption(['query' => ['cursor' => $cursor]]);
        }

        $client = new Client();

        $rawResponse = $client->request($fetchProductsRequest);
        $fetchProductsResponse = json_decode(json_encode($rawResponse), true);

        if (empty($fetchProductsResponse)) {
            return [];
        }

        $productValidator = new ProductValidator();
        $productsTransfer = new Product($this, $productValidator);

        return [
            'products' => $productsTransfer->transfProductsStore([$fetchProductsResponse]),
            'total' => Arr::get($fetchProductsResponse, 'total', 0),
            'next' => Arr::get($fetchProductsResponse, 'next', ''),
        ];
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function getProduct(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);
        if ($b2wValidator->validate()) {
            $request = new Request($httpMethod, $this->_makeUrl(null, $data['request']['url']));

            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $params = [];
            $params['page'] = 0;
            if(!empty($data['request']['params']['page'])) {
                $params['page'] = $data['request']['params']['page'];
            }

            if(!empty($data['request']['params']['per_page'])) {
                $params['per_page'] = $data['request']['params']['per_page'];
            }

            if(count($params)) {
                $request->addOption(['query' => $params]);
            }

            $client = new Client();

            $rawResponse = $client->request($request);
            $products = [json_decode(json_encode($rawResponse), true)];

            if(count($products)) {
                $productValidator = new ProductValidator();
                $productsTransfer = new Product($this, $productValidator);
                $response['product_b2w'] = $productsTransfer->transfProductStore($products);
                $response['total'] = !empty($products[0]['total']) ? $products[0]['total'] : 0;
            }

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function storeAuth(string $httpMethod, array $data)
    {
        $request = new Request($httpMethod, $data['url']);
        $request->setOptions($data['options']);

        $client = new Client();

        $rawResponse = $client->request($request);
        $arrayResponse = json_decode(json_encode($rawResponse), true);

        if ($client->getStatusCode() === Response::HTTP_OK) {
            return $arrayResponse;
        }

        throw new \Exception(
            sprintf('Error requesting auth on store. MSG[%s]', json_encode($arrayResponse)),
            $client->getStatusCode()
        );
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function productActions(string $httpMethod, array $data)
    {
        $request = new Request($httpMethod, $data['url']);
        $request->setOptions($data['options']);

        $client = new Client();

        $rawResponse = $client->request($request);
        $arrayResponse = json_decode(json_encode($rawResponse), true);

        if ($client->getStatusCode() === Response::HTTP_CREATED) {
            return $arrayResponse;
        }

        throw new \Exception(
            sprintf('Error making product actions on store. MSG[%s]', json_encode($arrayResponse)),
            $client->getStatusCode()
        );
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function integratePrice(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);

        if ($b2wValidator->validate()) {

            $productsResults = [
                'success' => [],
            ];

            $productValidator = new PriceValidator();

            $product = new Price($this, $productValidator);
            $formattedProducts = $product->formatPrices($data);

            foreach ($formattedProducts as $sku => $products) {
                foreach ($products as $formattedProduct) {
                    try {
                        $request = new Request($httpMethod, $this->_makeUrl($formattedProduct['sku'],
                            $data['request']['url']));

                        $request->setOptions([
                            'headers' => $data['request']['headers'],
                            'json' => ['product' => $formattedProduct],
                        ]);

                        $client = new Client();

                        $response = $client->request($request);

                        $productsResults['success'][$sku][] = (array)$response;
                    } catch (\Exception $exception) {
                        unset($formattedProduct[$sku]);
                        $this->addErrorsWithKey($exception->getMessage(), $sku);
                        continue;
                    }
                }
            }

            $productsResults['errors'] = $this->getErrors();

            return $productsResults;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function integrateStocks(string $httpMethod, array $data)
    {
        $b2wValidator = new B2wValidator($data);

        if ($b2wValidator->validate()) {

            $productsResults = [
                'success' => [],
            ];

            $productValidator = new StockValidator();

            $product = new Stock($this, $productValidator);
            $formattedProducts = $product->formatStocks($data);

            foreach ($formattedProducts as $sku => $products) {
                foreach ($products as $formattedProduct) {
                    try {
                        $request = new Request($httpMethod, $this->_makeUrl($formattedProduct['sku'],
                            $data['request']['url']));

                        $request->setOptions([
                            'headers' => $data['request']['headers'],
                            'json' => ['product' => $formattedProduct],
                        ]);

                        $client = new Client();

                        $response = $client->request($request);

                        $productsResults['success'][$sku][] = (array)$response;
                    } catch (\Exception $exception) {
                        unset($formattedProduct[$sku]);
                        $this->addErrorsWithKey($exception->getMessage(), $sku);
                        continue;
                    }
                }
            }

            $productsResults['errors'] = $this->getErrors();

            return $productsResults;
        }
    }
}