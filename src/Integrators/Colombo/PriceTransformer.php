<?php

namespace Mobly\MarketplaceSdk\Integrators\Colombo;

use Carbon\Carbon;

class PriceTransformer
{
    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function transform(array $data)
    {
        $product = array_pop($data['products']);
        foreach ($product['skus'] as $sku) {
            $formattedProduct[$sku['sku']] = [
                'price' => $this->getSkuPrice($sku),
            ];
            return $formattedProduct;
        }

    }

    private function getSkuPrice($sku)
    {
        if(empty($sku['special_price']))
            return $sku['price'];

        if(Carbon::now() >= Carbon::parse($sku['special_price_start'])
            && Carbon::now() < Carbon::parse($sku['special_price_end']))
            return $sku['special_price'];

        return $sku['price'];
    }

}