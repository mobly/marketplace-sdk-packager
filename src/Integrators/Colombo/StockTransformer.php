<?php

namespace Mobly\MarketplaceSdk\Integrators\Colombo;

class StockTransformer
{
    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function transform(array $data)
    {
        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {
                $formattedProduct[$sku['sku']] = [
                    'stock' => $sku['quantity'],
                ];
                return $formattedProduct;
            }
        }

    }

}