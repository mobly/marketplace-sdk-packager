<?php

namespace Mobly\MarketplaceSdk\Integrators\Colombo;

use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use mysql_xdevapi\Exception;

class Auth
{

    private $url;
    private $apiTokenId;
    private $apiKey;

    /**
     * Auth constructor.
     * @param $apiTokenId
     * @param $apiKey
     */
    public function __construct($url, $apiTokenId, $apiKey)
    {
        $this->url = $url;
        $this->apiTokenId = $apiTokenId;
        $this->apiKey = $apiKey;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getApiTokenId()
    {
        return $this->apiTokenId;
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param $httpMethod
     * @return array|mixed
     * @throws \Exception
     */
    public function getAccessToken($httpMethod)
    {
        $request = new Request($httpMethod, $this->url);

        $request->addOption(array(
            'json' => array(
                'apiTokenId' => $this->getApiTokenId(),
                'apiKey' => $this->getApiKey(),
            )
        ));

        $client = new Client();
        $response = $client->request($request);
        return $response;
    }
}