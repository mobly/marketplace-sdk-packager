<?php

namespace Mobly\MarketplaceSdk\Integrators\Colombo;

class StatusTransformer
{
    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function transform(array $data)
    {
        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {
                $formattedProduct[$sku['sku']] = [
                    'status' => $sku['status'] ? 'ENABLED' : 'DISABLED',
                ];
                return $formattedProduct;
            }
        }

    }

}