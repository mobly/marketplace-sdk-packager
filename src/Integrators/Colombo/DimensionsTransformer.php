<?php

namespace Mobly\MarketplaceSdk\Integrators\Colombo;

class DimensionsTransformer
{
    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function transform(array $data)
    {
        foreach ($data['products'] as $product) {

            try {

                $formattedDimension = [
                    'deliveryTimeDays' => $product['chain_time'] ?? 1,
                    'height' => $product['dimensions']['height'],
                    'length' => $product['dimensions']['length'],
                    'weight' => $product['dimensions']['weight'],
                    'width' => $product['dimensions']['width'],
                ];

                return $formattedDimension;

            } catch (\Exception $exception) {
                throw $exception;
            }

        }

    }

}