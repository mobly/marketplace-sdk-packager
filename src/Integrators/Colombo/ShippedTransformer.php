<?php

namespace Mobly\MarketplaceSdk\Integrators\Colombo;

use Carbon\Carbon;

class ShippedTransformer
{
    /**
     * @param $item
     * @return array
     * @throws \Exception
     */
    public function transform($order)
    {
        $trackings = [];

        foreach ($order->items as $item) {
            try {
                $trackings[] =
                    (object)[
                        "trackingDate" => $item->occurrence_date ?
                            date("Y-m-d\TH:i:s.000\Z", strtotime($item->occurrence_date)) :
                            date(
                                "Y-m-d\TH:i:s.000\Z",
                                strtotime(Carbon::now('America/Sao_Paulo')->toDateTimeString())
                            ),
                        "orderTrackingLink" => $item->url ?? '',
                    ];

            } catch (\Exception $exception) {
                throw $exception;
            }
        }

        $formattedShipped = [
            "dateUpdated" => date(
                "Y-m-d\TH:i:s.000\Z",
                strtotime(Carbon::now('America/Sao_Paulo')->toDateTimeString())
            ),
            "trackings" => $trackings
        ];

        return $formattedShipped;
    }
}