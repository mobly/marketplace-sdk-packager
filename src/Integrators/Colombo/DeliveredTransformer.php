<?php

namespace Mobly\MarketplaceSdk\Integrators\Colombo;

use Carbon\Carbon;

class DeliveredTransformer
{
    /**
     * @param $item
     * @return array
     * @throws \Exception
     */
    public function transform($order)
    {

        $formattedDelivered = [
            "dateUpdated" => date(
                "Y-m-d\TH:i:s.000\Z",
                strtotime(Carbon::now('America/Sao_Paulo')->toDateTimeString())
            ),
        ];

        return $formattedDelivered;
    }
}