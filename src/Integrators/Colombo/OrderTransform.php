<?php

namespace Mobly\MarketplaceSdk\Integrators\Colombo;

use Illuminate\Http\Response;
use Mobly\MarketplaceSdk\Helpers\Helper;

class OrderTransform
{
    /**
     * @param array $orders
     * @return mixed
     * @throws \Exception
     */
    public function transformOrders(array $orders)
    {
        $result['orders'] = [];

        try {
            foreach ($orders as $order) {

                list($firstName, $lastName) = Helper::formatName($order['customer']['name']);

                $orderTransform = [
                    'store_order_number'      => $order["orderId"],
                    'full_store_order_number' => $order["orderId"],
                    'store_last_updated_at'   => $order["status"]["dateUpdated"],
                    'store_order_status'      => $order["status"]["status"],
                    'customer_first_name'     => $firstName,
                    'customer_last_name'      => $lastName,
                    'customer_document'       => $order["customer"]["cpf_cnpj"],
                    'customer_email'          => "crf{$order["customer"]["cpf_cnpj"]}@mktcolombo.com.br",
                    'customer_phone'          => "{$order["customer"]["ddd"]}{$order["customer"]["phone"]}",
                    'customer_gender'         => null,
                    'billing_postcode'        => $order["shippingAddress"]["postcode"],
                    'billing_street'          => $order["shippingAddress"]["street"],
                    'billing_street_number'   => $order["shippingAddress"]["number"],
                    'billing_neighborhood'    => $order["shippingAddress"]["neighborhood"],
                    'billing_complement'      => $order["shippingAddress"]["complement"],
                    'billing_city'            => $order["shippingAddress"]["city"],
                    'billing_state'           => $order["shippingAddress"]["state"],
                    'billing_country'         => 'BR',
                    'shipping_postcode'       => $order["shippingAddress"]["postcode"],
                    'shipping_street'         => $order["shippingAddress"]["street"],
                    'shipping_street_number'  => $order["shippingAddress"]["number"],
                    'shipping_neighborhood'   => $order["shippingAddress"]["neighborhood"],
                    'shipping_complement'     => $order["shippingAddress"]["complement"],
                    'shipping_city'           => $order["shippingAddress"]["city"],
                    'shipping_state'          => $order["shippingAddress"]["state"],
                    'shipping_country'        => 'BR',
                    'shipping_price'          => $order["shippingValue"],
                    'total_price'             => $order["costTotalOrdered"],
                    'items'                   => [],
                ];

                foreach ($order['itemsOrder'] as $item) {
                    $orderTransform['items'][] = [
                        'store_item_id'     => $item['skuSellerId'],
                        'sku'               => $item['skuSellerId'],
                        'sku_name'          => '',
                        'unit_price'        => $item['unitaryValue'],
                        'shipping_price'    => !empty($item['totalValue']) ? $item['totalValue'] : 0,
                        'quantity'          => $item['quantity'],
                        'total_commission'  => $item['commissionAmount'],
                    ];
                }

                $result['orders'][] = $orderTransform;
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $result;
    }
}