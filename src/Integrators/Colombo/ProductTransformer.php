<?php

namespace Mobly\MarketplaceSdk\Integrators\Colombo;

use Carbon\Carbon;

class ProductTransformer
{
    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function transform(array $data)
    {
        $product = array_pop($data['products']);
        foreach ($product['skus'] as $sku) {

            try {

                $productName = $product['name'];
                if (!empty($productSku['superAttribute']))
                    $productName = sprintf('%s - %s', $product['name'], $productSku['superAttribute']);

                $images = [];
                foreach ($product['images'] as $image) $images[] = (object)['picture' => $image];

                $storeAttributes = [];
                foreach ($product['store_attributes'] as $attribute) {
                    $storeAttributes[] = (object)[
                        'attributeId' => $attribute['external_id'],
                        'attributeValue' => $attribute['external_value_id'], // Todo: Verificar se vai ser o id mesmo ou value
                    ];
                }

                $price = $this->getSkuPrice($sku);

                $skus[] = (object)[
                    'color' => array_key_exists('Cor', $product['attributes']) ? $product['attributes']['Cor'] : '',
                    'deliveryTimeDays' => $product['chain_time'] ?? 1,
                    'ean' => $sku['ean'],
                    'height' => $product['dimensions']['height'],
                    'length' => $product['dimensions']['length'],
                    'price' => $price,
                    'productItemPictures' => $images,
                    'skuSellerId' => $sku['sku'],
                    'stock' => $sku['quantity'],
                    'weight' => $product['dimensions']['weight'],
                    'width' => $product['dimensions']['width'],
                ];

                $formattedProduct = [
                    'brand' => $product['brand'],
                    'description' => strip_tags($product['description']),
                    'groupId' => $product['store_category_external_id'],
                    'model' => $product['attributes']['Modelo'],
                    'name' => $productName,
                    'productAttributeValues' => $storeAttributes,
                    'productItemVariations' => $skus,
                ];

                return $formattedProduct;

            } catch (\Exception $exception) {
                throw $exception;
            }

        }

    }

    private function getSkuPrice($sku)
    {
        if(empty($sku['special_price']))
            return $sku['price'];

        if(Carbon::now() >= Carbon::parse($sku['special_price_start'])
            && Carbon::now() < Carbon::parse($sku['special_price_end']))
            return $sku['special_price'];

        return $sku['price'];
    }
}