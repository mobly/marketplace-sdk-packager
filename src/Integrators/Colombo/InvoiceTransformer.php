<?php

namespace Mobly\MarketplaceSdk\Integrators\Colombo;

use Carbon\Carbon;

class InvoiceTransformer
{
    /**
     * @param $item
     * @return array
     * @throws \Exception
     */
    public function transform($item)
    {
        try {

            $formattedInvoice = [
                "dateUpdated" => date(
                    "Y-m-d\TH:i:s.000\Z",
                    strtotime(Carbon::now('America/Sao_Paulo')->toDateTimeString())
                ),
                "invoices" => [
                    (object)[
                        "invoiceDate" => $item->occurrence_date ?
                            date("Y-m-d\TH:i:s.000\Z", strtotime($item->occurrence_date)) :
                            date(
                                "Y-m-d\TH:i:s.000\Z",
                                strtotime(Carbon::now('America/Sao_Paulo')->toDateTimeString())
                            ),
                        "invoiceNumber" => $item->nfe,
                        "invoicedKey" => $item->nfe_access_key,
                    ]
                ]
            ];

            return $formattedInvoice;

        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}