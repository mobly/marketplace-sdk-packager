<?php

namespace Mobly\MarketplaceSdk\Integrators;

use Mobly\MarketplaceSdk\Integrators\Walmart\ProductTransformer;
use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use Mobly\MarketplaceSdk\Validators\Walmart\ProductValidator;
use Mobly\MarketplaceSdk\Validators\Walmart\WalmartValidator;

class WalmartIntegrator extends IntegratorAbstract
{
    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function integrate(string $httpMethod, array $data)
    {
        $response = [];

        $wallmartValidator = new WalmartValidator($data);
        if ($wallmartValidator->validate()) {

            $productValidator = new ProductValidator();
            $product = new ProductTransformer($this, $productValidator);
            $formattedProducts = $product->transform($data);

            $client = new Client();
            $request = new Request($httpMethod, $data['request']['url']);
            $request->setOptions([
                'headers' => $data['request']['headers'],
                'json' => ['offer' => $formattedProducts],
            ]);

            $response = (array)$client->request($request);
            $response['errors'] = $this->getErrors();

        }
        return $response;
    }

    public function getStatus(string $httpMethod, array $data)
    {
        $magaluValidator = new WalmartValidator($data);
        $magaluValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        $client = new Client();
        $response = $this->formatResponseStatusProduct($client->request($request));

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool
     */
    public function integrateStocks(string $httpMethod, array $data)
    {
        $walmartValidator = new WalmartValidator($data);
        $walmartValidator->validate();

        $productsResults = [];

        $client = new Client();

        $product = array_pop($data['products']);
        foreach ($product['skus'] as $sku) {
            try {
                $request = new Request($httpMethod, $this->_makeUrl(
                    $data['request']['url'],
                    [
                        $data['request']['seller_id'],
                        !empty($sku['store_sku'][0]) ? $sku['store_sku'][0] : $sku['sku'],
                        $sku['quantity'],
                    ]
                ));

                $request->setOptions([
                    'headers' => $data['request']['headers'],
                ]);

                $response = $client->request($request);

                $productsResults['success'][$sku['sku']][] = (array)$response;
            } catch (\Exception $exception) {
                $this->addErrorsWithKey($exception->getMessage(), $sku);
                continue;
            }
        }

        $productsResults['errors'] = $this->getErrors();

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function checkProduct(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => [],
        ];

        $client = new Client();
        $product = array_pop($data['products']);
        foreach ($product['skus'] as $sku) {
            try {
                $request = new Request($httpMethod, $this->_makeUrl($data['request']['url'], [$data['request']['seller_id'], $sku['sku']]));

                $request->addOption([
                    'headers' => $data['request']['headers'],
                ]);

                $response = $client->request($request);
                if ($response->status == 'active') {
                    throw new \Exception('Product active');
                }

                $productsResults['success'][$sku['sku']][] = (array)$response;
            } catch (\Exception $exception) {
                $productsResults['errors'][$sku['sku']][] = ['message' => $exception->getMessage()];
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool
     */
    public function update(string $httpMethod, array $data)
    {
        $wallmartValidator = new WalmartValidator($data);
        if ($wallmartValidator->validate()) {
            $productsResults = [];

            $productValidator = new ProductValidator();
            $product = new ProductTransformer($this, $productValidator);
            $formattedProducts = $product->transform($data);

            $client = new Client();
            foreach ($formattedProducts as $sku) {
                try {
                    $request = new Request($httpMethod, $this->_makeUrl(
                        $data['request']['url'],
                        [
                            $data['request']['seller_id'],
                            $sku['sellerSKU'],
                            $sku['quantity'],
                        ]
                    ));

                    $request->setOptions([
                        'headers' => $data['request']['headers'],
                        'json' => $sku,
                    ]);

                    $response = $client->request($request);

                    $productsResults['success'][$sku['sellerSKU']][] = (array)$response;
                } catch (\Exception $exception) {
                    $this->addErrorsWithKey($exception->getMessage(), $sku);
                    continue;
                }
            }

            $productsResults['errors'] = $this->getErrors();
            return $productsResults;
        }

        return false;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool
     */
    public function integratePrice(string $httpMethod, array $data)
    {
        $walmartValidator = new WalmartValidator($data);
        $walmartValidator->validate();

        $productsResults = [];

        $client = new Client();

        $product = array_pop($data['products']);
        foreach ($product['skus'] as $sku) {
            try {
                $request = new Request($httpMethod, $this->_makeUrl(
                    $data['request']['url'],
                    [
                        $data['request']['seller_id'],
                        !empty($sku['store_sku'][0]) ? $sku['store_sku'][0] : $sku['sku'],
                        empty($sku['special_price']) ? $sku['price'] : $sku['special_price'],
                        $sku['price']
                    ]
                ));

                $request->setOptions([
                    'headers' => $data['request']['headers'],
                ]);

                $response = $client->request($request);

                $productsResults['success'][$sku['sku']][] = (array)$response;
            } catch (\Exception $exception) {
                $this->addErrorsWithKey($exception->getMessage(), $sku);
                continue;
            }
        }

        $productsResults['errors'] = $this->getErrors();

        return $productsResults;
    }

    public function getOrder(string $httpMethod, array $data)
    {
        // TODO
    }

    public function getOrders(string $httpMethod, array $data)
    {
        // TODO
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function setOrderToShipped(string $httpMethod, array $data)
    {
        return $this->simpleRequest($httpMethod, $data);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    private function simpleRequest(string $httpMethod, array $data)
    {
        $walmartValidator = new WalmartValidator($data);
        if ($walmartValidator->validate()) {

            $request = new Request($httpMethod, $this->_makeUrl(
                $data['request']['url'],
                [
                    $data['request']['seller_id'],
                    $data['request']['orderId'],
                ]
            ));

            $request->setOptions([
                'headers' => $data['request']['headers'],
                'json' => $data['request']['data'],
            ]);

            $client = new Client();

            $response = $client->request($request);

            return $response;
        }
    }

    /**
     * @param $replaceParam
     * @param $url
     * @return mixed
     */
    private function _makeUrl(string $url, array $replaceParam)
    {
        return vsprintf($url, $replaceParam);
    }

    /**
     * @param $response
     * @return array
     */
    public function formatResponseStatusProduct($response)
    {
        $res = [
            'success' => true,
            'status' => $response->status,
        ];

        return $res;
    }
}
