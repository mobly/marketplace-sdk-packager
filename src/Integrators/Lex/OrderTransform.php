<?php

namespace Mobly\MarketplaceSdk\Integrators\Lex;


use Illuminate\Http\Response;
use Mobly\MarketplaceSdk\Helpers\Helper;

class OrderTransform
{
    /**
     * @param array $orders
     * @return mixed
     * @throws \Exception
     */
    public function transformOrders(array $orders)
    {
        $result['orders'] = [];

        try {

            foreach ($orders as $order) {

                $customerFieldsFormatted = Helper::checkCustomerAddressParameter($order);
                $customerFieldsFormatted['billing_state'] = Helper::stateToUF($customerFieldsFormatted['billing_state']);
                $shippingState = Helper::stateToUF($order->shipping_address->province);

                $orderTransform = [
                    'store_order_number' => $order->id,
                    'store_last_updated_at' => $order->updated_at ?? date('Y-m-d H:i:s'),
                    'store_order_status' => $order->payment_status,
                    'customer_first_name' => $order->customer->name,
                    'customer_last_name' => '',
                    'customer_document' => $customerFieldsFormatted['customer_document'],
                    'customer_email' => $customerFieldsFormatted['customer_email'],
                    'customer_phone' => $customerFieldsFormatted['customer_phone'],
                    'customer_phone2' => null,
                    'customer_gender' => null,
                    'billing_postcode' => $customerFieldsFormatted['billing_postcode'],
                    'billing_street' => $customerFieldsFormatted['billing_street'],
                    'billing_street_number' => $customerFieldsFormatted['billing_street_number'],
                    'billing_neighborhood' => $customerFieldsFormatted['billing_neighborhood'],
                    'billing_complement' => !empty($order->customer->billing_floor) ? $order->customer->billing_floor : '',
                    'billing_city' => $customerFieldsFormatted['billing_city'],
                    'billing_state' => $customerFieldsFormatted['billing_state'],
                    'billing_country' => 'BR',
                    'shipping_postcode' => $order->shipping_address->zipcode,
                    'shipping_street' => $order->shipping_address->address,
                    'shipping_street_number' => $order->shipping_address->number,
                    'shipping_neighborhood' => $order->shipping_address->locality,
                    'shipping_complement' => !empty($order->shipping_address->floor) ? $order->shipping_address->floor : '',
                    'shipping_city' => $order->shipping_address->city,
                    'shipping_state' => $shippingState,
                    'shipping_country' => 'BR',
                    'shipping_price' => $order->shipping_cost_customer,
                    'total_price' => $order->total,
                    'items' => []
                ];

                foreach ($order->products as $item) {
                    for($i=1; $i<=$item->quantity;$i++) {
                        $orderTransform['items'][] = [
                            'store_item_id' => "{$item->id}_{$i}",
                            'sku' => $item->sku,
                            'sku_name' => $item->name,
                            'unit_price' => $item->price,
                            'shipping_price' => 0,
                            'quantity' => 1,
                            'total_commission' => 0,
                        ];
                    }
                }

                $extra = [];

                $extra['StorePurchasedDate'] = $order->created_at;

                $extra['StoreEstimatedDeliveryDate'] = $order->shipping_min_days;

                if(isset($order->payment_details) && isset($order->payment_details->method)) {
                    $extra['payment_method'] = $order->payment_details->method;
                }
                if(isset($order->payment_details) && isset($order->payment_details->installments)) {
                    $extra['payment_installments'] = $order->payment_details->installments;
                }
                
                $extra['storeDiscount'] = $order->discount;

                $orderTransform['extra'] = json_encode($extra);
                $result['orders'][] = $orderTransform;
            }

        } catch (\Exception $e) {

            throw new \Exception($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $result;
    }
}
