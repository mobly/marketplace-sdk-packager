<?php

namespace Mobly\MarketplaceSdk\Integrators;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Mobly\MarketplaceSdk\Helpers\Helper;
use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use Mobly\MarketplaceSdk\Validators\Carrefour\OfferValidator;
use Mobly\MarketplaceSdk\Validators\Carrefour\PriceValidator;
use Mobly\MarketplaceSdk\Validators\Carrefour\ProductValidator;
use Mobly\MarketplaceSdk\Validators\Carrefour\CarrefourValidator;

class CarrefourIntegrator extends IntegratorAbstract
{
    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     */
    public function integrate(string $method, array $data)
    {
        $carrefourValidator = new CarrefourValidator($data);
        if ($carrefourValidator->validate()) {
            $fileContent = null;

            if (!empty($data['products'])) {
                $fileContent = $this->transform($data['products']);
            } else if (!empty($data['offers'])) {
                $fileContent = $this->transformOffer($data['offers']);
            }

            if (null != $fileContent) {
                $request = new Request($method, $data['request']['url']);
                $request->setAuthKey($data['request']['Authorization']);

                $client = new Client();

                if (!empty($data['offers'])) {
                    $request->addOption(['multipart' => [
                        [
                            'name' => 'import_mode',
                            'contents' => 'NORMAL',
                        ],
                    ]]);
                }

                $response = (array)$client->fileRequest($request, 'file', 'products.csv', $fileContent);
            }
            $response['errors'] = $this->getErrors();
            return $response;
        }
    }

    /**
     * @param array $products
     * @return string
     * @throws \League\Csv\CannotInsertRecord
     */
    protected function transform(array $products)
    {
        $result = [];
        foreach ($products as $sku => $product) {
            if ($this->isValidProduct($product)) {
                foreach ($product['skus'] as $productSku) {
                    $productName = $product['name'];
                    if (!empty($productSku['superAttribute'])) {
                        $productName = sprintf('%s - %s', $product['name'], $productSku['superAttribute']);
                    }

                    $result[] = [
                        'product-title' => $productName,
                        'category-code' => 'seller-category',
                        'sku' => $sku,
                        'product-sku' => $productSku['sku'],
                        'certificacoes' => 'Não informado',
                        'codigo-certificacao' => 'Não informado',
                        'ean' => $productSku['ean'],
                        'weight' => !empty($product['weight']) ? $product['weight'] : 1,
                        'height' => !empty($product['height']) ? $product['height'] : 1,
                        'width' => !empty($product['width']) ? $product['width'] : 1,
                        'depth' => !empty($product['depth']) ? $product['depth'] : 1,
                        'variantImage1' => $product['images'][0],
                        'variantImage2' => !empty($product['images'][1]) ? $product['images'][1] : null,
                        'variantImage3' => !empty($product['images'][2]) ? $product['images'][2] : null,
                        'variantImage4' => !empty($product['images'][3]) ? $product['images'][3] : null,
                        'variantImage5' => !empty($product['images'][4]) ? $product['images'][4] : null,
                        'variant-voltage' => 1,
                        'description' => strip_tags($product['description']),
                        'seller-atributte' => $this->attributeParser($product['attributes']),

                        /// [SM-851] add fields
                        /// @TODO after create field ANATEL in BOB update this field
                        '32495' => 'Não se aplica', /// Código da Homologação ANATEL
                        '32485' => 'Não se aplica', // Código do MAPA
                        '32481' => 'Não se aplica', // Código da ANVISA
                    ];
                }
            }
        }

        return Helper::getCsvFromArray($result);
    }

    /**
     * @param array $offers
     * @param null $operation
     * @return string
     * @throws \League\Csv\CannotInsertRecord
     */
    protected function transformOffer(array $offers, $operation = null)
    {
        $operation = $operation ?? 'update';
        $results = [];
        foreach ($offers as $offer) {
            if ($this->isValidOffer($offer)) {
                $results[] = [
                    'sku' => $offer['sku'],
                    'product-id' => $offer['sku'],
                    'product-id-type' => 'SHOP_SKU',
                    'description' => null,
                    'internal-description' => null,
                    'price' => $offer['price'],
                    'price-additional-info' => null,
                    'quantity' => $offer['quantity'],
                    'min-quantity-alert' => $offer['min_quantity_alert'] ?? 1,
                    'state' => 11, //hardcoded, for more info consult Carrefour documentation
                    'available-start-date' => null,
                    'available-end-date' => null,
                    'discount-price' => null,
                    'discount-start-date' => null,
                    'discount-end-date' => null,
                    'update-delete' => $offer['update_delete'] ?? $operation
                ];
            }
        }

        return Helper::getCsvFromArray($results);
    }

    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws \League\Csv\Exception
     * @throws \Exception
     */
    public function getStatus(string $method, array $data)
    {
        if (empty($data['request']['url'])) {
            throw new \Exception('the field \'endpoint_url\' is required and cannot be empty',
                Response::HTTP_BAD_REQUEST);
        }

        if (empty($data['request']['Authorization'])) {
            throw new \Exception('the field \'Authorization\' is required and cannot be empty',
                Response::HTTP_BAD_REQUEST);
        }

        $client = new Client();

        $request = new Request($method, $data['request']['url']);
        $request->setAuthKey($data['request']['Authorization']);

        return $client->request($request);
    }

    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws \League\Csv\Exception
     * @throws \Exception
     */
    public function getOrders(string $method, array $data)
    {
        $carrefourValidator = new CarrefourValidator($data);
        if ($carrefourValidator->validate()) {

            $request = new Request($method, $data['request']['url']);
            $request->setAuthKey($data['request']['Authorization']);

            if (!empty($data['params']) && is_array($data['params'])) {
                $request->addOption(['query' => $data['params']]);
            }

            if (!empty($data['order_aditional_fields']) && is_string($data['order_aditional_fields'])) {
                $request->addOption(['query' => ['order_aditional_fields' => $data['order_aditional_fields']]]);
            }

            $client = new Client();
            $rawResponse = $client->request($request);

            $response = $this->transformOrders($rawResponse);
            $response['raw'] = $rawResponse;

            return $response;
        }
    }

    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws \League\Csv\Exception
     */
    public function getOrder(string $method, array $data)
    {
        return $this->getOrders($method, $data);
    }

    /**
     * @param \stdClass $response
     * @return array
     * @throws \Exception
     */
    protected function transformOrders(\stdClass $response)
    {
        try {
            $results['orders'] = [];

            foreach ($response->orders as $order) {

                $addition = [];

                logConsoleInfo('Parsing order ' . $order->order_id);

                foreach ($order->order_additional_fields as $additional_fields => $additional) {
                    $addition[$additional->code] = $additional->value;
                }

                $addition['customer-cpf'] = $addition['customer-cpf'] ?? '';

                if (!count($addition)) {
                    throw new \Exception('Additional required');
                }

                $resolvedName = Helper::resolveFirstLastName([$order->customer->firstname, $order->customer->lastname]);
                $resolveStreet = Helper::formarterBillingAndShippingAddressNumber($addition, $order->customer);

                if (!$addition['customer-cpf']){
                    $resolvedName = ['NI', 'NI'];
                }

                $orderTransform = [
                    'store_order_number' => $order->order_id,
                    'store_order_status' => $order->order_state,
                    'store_last_updated_at' => $order->last_updated_date ?? date('Y-m-d H:i:s'),
                    'customer_first_name' => empty($resolvedName[0]) ? 'NI' : $resolvedName[0],
                    'customer_last_name' => empty($resolvedName[1]) ? 'NI' : $resolvedName[1], 
                    'customer_document' => $addition['customer-cpf'] ? preg_replace('/[^0-9x]/i', '', $addition['customer-cpf'] ) : 'NI',
                    'customer_email' => "crf-".($order->order_id)."@mktcarrefour.com.br",
                    'customer_phone' => preg_replace('/[^0-9]/', '', $addition['tel-number'] ?? 'NI'),
                    'customer_gender' => '',
                    'billing_postcode' => preg_replace('/[^0-9]/', '', $order->customer->billing_address->zip_code),
                    'billing_street' => $resolveStreet['customer']['billing_street'],
                    'billing_street_number' => $resolveStreet['customer']['billing_street_number'],
                    'billing_neighborhood' => $resolveStreet['customer']['billing_neighborhood'],
                    'billing_complement' => $order->customer->billing_address->street_1,
                    'billing_city' => $order->customer->billing_address->city,
                    'billing_state' => $order->customer->billing_address->state,
                    'billing_country' => 'BR',
                    'shipping_postcode' => preg_replace('/[^0-9]/', '', $order->customer->shipping_address->zip_code),
                    'shipping_street' => $resolveStreet['shipping']['shipping_street'],
                    'shipping_street_number' => $resolveStreet['shipping']['shipping_street_number'],
                    'shipping_neighborhood' => $resolveStreet['shipping']['shipping_neighborhood'],
                    'shipping_complement' => $order->customer->shipping_address->street_1,
                    'shipping_city' => $order->customer->shipping_address->city,
                    'shipping_state' => $order->customer->shipping_address->state,
                    'shipping_country' => 'BR',
                    'shipping_price' => $order->shipping_price,
                    'total_price' => $order->total_price,
                    'items' => [],
                ];

                foreach ($order->order_lines as $line) {
                    for ($i = 1; $i <= $line->quantity; $i++) {
                        $orderTransform['items'][] = [
                            'store_item_id' => "{$line->order_line_id}_{$i}",
                            'sku' => $line->offer_sku,
                            'sku_name' => $line->product_title,
                            'unit_price' => $line->price_unit,
                            'shipping_price' => $line->shipping_price / $line->quantity,
                            'quantity' => 1,
                            'total_commission' => $line->total_commission / $line->quantity,
                        ];
                    }
                }

                $extra['StorePurchasedDate'] = isset($order->acceptance_decision_date) ? $order->acceptance_decision_date : $order->created_date;

                if (isset($order->customer_debited_date) && !empty($order->customer_debited_date)) {
                    $extra['StoreApprovedDate'] = isset($order->customer_debited_date) ? $order->customer_debited_date : '';
                }
                $extra['StoreEstimatedDeliveryDate'] = isset($order->leadtime_to_ship) ? $order->leadtime_to_ship : '';
                if (isset($addition['scheduled-delivery'])) {
                    $extra['StoreScheduledDeliveryDate'] = $addition['scheduled-delivery'];
                }

                if (isset($order->payment_type) && !empty($order->payment_type)) {
                    $extra['payment_name'] = $order->payment_type;
                }
                $orderTransform['extra'] = json_encode($extra);

                $results['orders'][] = $orderTransform;
            }

            return $results;
        } catch (\Exception $e) {
            logConsoleError($e->getMessage() . ' on ' . $e->getFile() . ':' . $e->getLine());
            //throw new \Exception($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getDefaultRequest(string $method, string $url, string $apiKey)
    {
        $request = new Request($method, $url);
        $request->addOption([
            'headers' => [
                'Authorization' => $apiKey,
                'Accept' => 'application/json',
            ]
        ]);

        return $request;
    }

    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws \League\Csv\Exception
     */
    public function sendTrackingCode(string $method, array $data)
    {
        $carrefourValidator = new CarrefourValidator($data);

        if ($carrefourValidator->validate()) {
            $url = $data['request']['url'];
            $apiKey = $data['request']['Authorization'];

            $request = $this->getDefaultRequest($method, $url, $apiKey);

            $client = new Client();

            unset($data['request']);

            return $client->jsonRequest($request, $data);
        }
    }

    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws \League\Csv\Exception
     * @throws \Exception
     */
    public function sendInvoice(string $method, array $data)
    {
        $url = $data['request']['url'];
        $apiKey = $data['request']['Authorization'];

        $request = $this->getDefaultRequest($method, $url, $apiKey);

        $data = array_filter($data, function ($key) {
            return 'order_additional_fields' === $key;
        }, ARRAY_FILTER_USE_KEY);

        $client = new Client();

        return $client->jsonRequest($request, $data);
    }

    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     */
    public function deleteProducts(string $method, array $data)
    {
        $carrefourValidator = new CarrefourValidator($data);
        if ($carrefourValidator->validate()) {
            $fileContent = null;

            if (!empty($data['offers'])) {
                $fileContent = $this->transformOffer($data['offers'], 'delete');

                if (!empty($fileContent)) {
                    $request = new Request($method, $data['request']['url']);
                    $request->setAuthKey($data['request']['Authorization']);

                    $client = new Client();

                    $request->addOption(['multipart' => [
                        [
                            'name' => 'import_mode',
                            'contents' => 'NORMAL',
                        ],
                    ]]);

                    return $client->fileRequest($request, 'file', 'products.csv', $fileContent);
                }
            }
        }
    }

    /**
     * @param array $attributes
     * @return string
     */
    protected function attributeParser(array $attributes)
    {
        $attrString = '';

        foreach ($attributes as $key => $value) {
            $attrString .= "{$key}:{$value}|";
        }

        $attrString = substr($attrString, 0, -1);

        return $attrString;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \League\Csv\Exception
     */
    public function setOrderToDelivered(string $httpMethod, array $data)
    {
        $request = new Request($httpMethod, $data['request']['url']);
        $request->setAuthKey($data['request']['Authorization']);

        $data = array_filter($data, function ($key) {
            return 'order_additional_fields' === $key;
        }, ARRAY_FILTER_USE_KEY);

        $client = new Client();

        $data['order_additional_fields'] = [$data['order_additional_fields']];
        return $client->jsonRequest($request, $data);
    }

    /**
     * @param array $product
     * @return bool
     */
    public function isValidProduct(array $product)
    {
        $result = true;
        try {
            $productValidator = new ProductValidator();
            $productValidator->setData($product);
            $productValidator->validate();
        } catch (ValidationException $exception) {
            foreach ($product['skus'] as $sku) {
                $this->addErrorsWithKey($exception->getMessage(), $sku['sku']);
            }
            $result = false;
        }
        return $result;
    }

    /**
     * @param array $offer
     * @return bool
     */
    public function isValidOffer(array $offer)
    {
        $result = true;
        try {
            $offerValidator = new OfferValidator();
            $offerValidator->setData($offer);
            $offerValidator->validate();
        } catch (ValidationException $exception) {
            $this->addErrorsWithKey($exception->getMessage(), $offer['sku']);
            $result = false;
        }
        return $result;
    }

    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws \League\Csv\Exception
     */
    public function getOffer(string $method, array $data)
    {
        $carrefourValidator = new CarrefourValidator($data);
        $carrefourValidator->validate();

        $request = new Request($method, $data['request']['url']);
        $request->setAuthKey($data['request']['Authorization']);

        if (is_array($data['params']) && !empty($data['params'])) {
            $request->addOption(['query' => $data['params']]);
        }

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws \Exception
     */
    public function getProducts(string $httpMethod, array $data)
    {
        $magaluValidator = new CarrefourValidator($data);

        $magaluValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->setAuthKey($data['request']['Authorization']);
        $request->addOption([
            'query' => $data['request']['params'],
        ]);

        return (new Client())->request($request);
    }

    /**
     * @param string $method
     * @param array $data
     * @return array
     */
    public function integratePrice(string $method, array $data)
    {
        $leroyValidator = new CarrefourValidator($data);
        if (!$leroyValidator->validate()) {
            return [];
        }

        $content = $this->transformPrices($data['prices']);

        if (!$content) {
            return [
                'errors' => $this->getErrors()
            ];
        }
        $request = new Request($method, $data['request']['url']);
        $request->setAuthKey($data['request']['Authorization']);

        $client = new Client();
        $response = (array) $client->fileRequest($request, 'file', 'prices.csv', $content);
        $response['errors'] = $this->getErrors();
        return $response;
    }

    /**
     * @param array $products
     * @return string
     */
    protected function transformPrices(array $products)
    {
        $result = [];

        foreach ($products as $sku => $product) {
            $result = array_merge($result, $this->parseProductDataPrice($product['skus']));
        }

        return Helper::getCsvFromArray($result);
    }

    /**
     * @param array $data
     * @return array
     */
    protected function parseProductDataPrice(array $data)
    {
        $results = [];
        foreach ($data as $price) {
            if ($this->isValidPrice($price)) {

                $arrPrice = [
                    'offer-sku' => $price['sku'],
                    'price' => $price['price'],
                ];

                if(!empty($price['special_price'])) {
                    $arrDiscount = [
                        'discount-price' => $price['special_price'], //PREÇO POR
                        'discount-end-date' => Carbon::parse($price['special_price_end'])->toAtomString(),
                        'discount-start-date' => Carbon::parse($price['special_price_start'])->toAtomString()
                    ];

                    $arrPrice = array_merge($arrPrice, $arrDiscount);
                }

                $results[] = $arrPrice;

            }
        }

        return $results;
    }

    /**
     * @param array $price
     * @return bool
     */
    public function isValidPrice(array $price)
    {
        $result = true;
        try {
            $priceValidator = new PriceValidator($price);
            $priceValidator->validate();
        } catch (ValidationException $exception) {
            $this->addErrorsWithKey($exception->getMessage(), $price['sku']);
            $result = false;
        }
        return $result;
    }
}