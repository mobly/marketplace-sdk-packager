<?php

namespace Mobly\MarketplaceSdk\Integrators\Cnova;

class StatusTransformer
{
    const DEFAULT_CNOVA_SITES = ['CD', 'CB', 'PF', 'LA', 'HP', 'EX', 'PA', 'BT'];

    /**
     * In these new endpoints, acronyms (CB, PF and EX) are no longer used as in the
     * v2 endpoints, but numeric codes per flag, so the codes
     * current are:
     * 2 - EXTRA
     * 3 - BAHIA HOUSES
     * 4 - POINT
     */
    const DEFAULT_CNOVA_SITES_V4 = ["2", "3", "4"];

    /**
     * @param array $data
     * @return array
     */
    public function transform(array $sku)
    {
        $transformedStatus = [];
        foreach (self::DEFAULT_CNOVA_SITES_V4 as $site) {

            $transformedStatus[] = [
                'idSite' => $site,
                'status' => ($sku['quantity'] > 0 ? ($sku['status'] == 'inactive' ? 'N' : 'Y') : 'N')
            ];
        }

        $data = [
            'idSkuLojista' => $sku['sku'],
            'status' => $transformedStatus
        ];

        return $data;
    }
}