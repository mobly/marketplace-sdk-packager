<?php

namespace Mobly\MarketplaceSdk\Integrators\Cnova;

class PriceTransformer
{
    const DEFAULT_CNOVA_SITE = 'EX';

    /**
     * @param array $sku
     * @return array
     */
    public function transform(array $sku)
    {
        $transformedPrices[] = [
            'skuSellerId' => $sku['sku'],
            'prices' => [
                [
                    'default' => $sku['price'],
                    'offer' => !empty($sku['special_price'])
                        ? $sku['special_price']
                        : $sku['price'],
                    'site' => self::DEFAULT_CNOVA_SITE,
                ],
            ],
        ];


        return $transformedPrices;
    }

    /**
     * @param array $sku
     * @return array
     */
    public function transformDecrease(array $sku)
    {
        $sku['special_price'] = !empty($sku['special_price']) ? $sku['special_price'] : $sku['price'];
        $transformedPrices = [
            'skuSellerId' => $sku['sku'],
            'default' => (float)round($sku['price'], 2),
            'offer' => (float)round($sku['special_price'], 2),
            'site' => self::DEFAULT_CNOVA_SITE
        ];
        return $transformedPrices;
    }


    /**
     * @param array $data
     * @return array
     */
    public function transformCheckPrices(array $data)
    {
        $transformedPrices = [];
        $skusWithErrors = [];

        foreach ($data['skus'] as $sku) {
            $sku = (array)$sku;
            try {
                if (!empty($sku['errors'])) {

                    $message = '';
                    foreach ($sku['errors'] as $erro) {
                        $message .= $erro->code . " || ";
                        $message .= $erro->type . " || ";
                        $message .= $erro->message . " || ";
                        $message .= $erro->skuSellerId;
                    }

                    throw new \Exception($message);
                }

                $transformedPrices[$sku['skuSellerId']] = [
                    'skuSellerId' => $sku['skuSellerId'],
                ];
            } catch (\Exception $exception) {
                $skusWithErrors[$sku['skuSellerId']][] = ['message' => $exception->getMessage()];
            }
        }

        return [$transformedPrices, $skusWithErrors];
    }

    /**
     * NEW FUNCTION FOR UPDATE PRICE IN API VERSION V4 TO STORE VIA VAREJO
     *
     * @param array $sku
     * @return array
     */
    public function transformDecreaseV4(array $sku)
    {
        $sku['special_price'] = !empty($sku['special_price']) ? $sku['special_price'] : $sku['price'];

        $transformedPrices = [
            'idSkuLojista' => $sku['sku'],
            'preco' => [
                'oferta' => (float)round($sku['special_price'], 2),
                'padrao' => (float)round($sku['price'], 2)
            ]
        ];

        return $transformedPrices;
    }

    /**
     * NEW FUNCTION FOR UPDATE PRICE TO API VERSION V4 TO STORE VIA VAREJO
     *
     * @param array $sku
     * @return array
     */
    public function transformV4(array $sku)
    {
        $sku['special_price'] = !empty($sku['special_price']) ? $sku['special_price'] : $sku['price'];

        $transformedPrices = [
            'idSkuLojista' => $sku['sku'],
            'preco' => [
                'oferta' => $sku['special_price'],
                'padrao' => $sku['price']
            ]
        ];

        return $transformedPrices;
    }

}