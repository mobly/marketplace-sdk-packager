<?php


namespace Mobly\MarketplaceSdk\Integrators\Cnova;



class Order
{
    const COUNTRY_MAP = [
        'BRA' => 'BR'
    ];

    /**
     * @param array $orders
     * @return mixed
     * @throws \Exception
     */
    public function transformOrders(array $orders)
    {
        $result['orders'] = [];

        try {
            foreach ($orders as $order) {

                $phone = null;
                if (!empty($order['customer']['phones'][0]['number'])) {
                    $phone = $order['customer']['phones'][0]['number'];
                }

                list($firstName, $lastName) = $this->formatName($order['customer']['name']);

                $orderTransform = [
                    'store_order_number' => $order['id'],
                    'store_last_updated_at' => $order['updatedAt'] ?? date('Y-m-d H:i:s'),
                    'store_order_status' => $order['status'],
                    'customer_first_name' => empty($order['shipping']) ? 'NI' : $firstName,
                    'customer_last_name' => empty($order['shipping']) ? 'NI' : $lastName,
                    'customer_document' => $order['customer']['documentNumber'],
                    'customer_email' => $order['customer']['email'],
                    'customer_phone' => $phone,
                    'customer_gender' => null,
                    'billing_postcode' => $order['billing']['zipCode'],
                    'billing_street' => $order['billing']['address'],
                    'billing_street_number' => $order['billing']['number'],
                    'billing_neighborhood' => $order['billing']['quarter'],
                    'billing_complement' => empty($order['billing']['complement']) ? '' : $order['billing']['complement'],
                    'billing_city' => $order['billing']['city'],
                    'billing_state' => $order['billing']['state'],
                    'billing_country' => array_key_exists($order['billing']['countryId'], self::COUNTRY_MAP)
                        ? self::COUNTRY_MAP[$order['billing']['countryId']]
                        : $order['billing']['countryId'],
                    'shipping_postcode' => empty($order['shipping']['zipCode']) ? 'NI' : $order['shipping']['zipCode'],
                    'shipping_street' => empty($order['shipping']['address']) ? 'NI' : $order['shipping']['address'],
                    'shipping_street_number' => empty($order['shipping']['number']) ? 'NI' : $order['shipping']['number'],
                    'shipping_neighborhood' => empty($order['shipping']['quarter']) ? 'NI' : $order['shipping']['quarter'],
                    'shipping_complement' => empty($order['shipping']['complement']) ? '' : $order['shipping']['complement'],
                    'shipping_city' => empty($order['shipping']['city']) ? 'NI' : $order['shipping']['city'],
                    'shipping_state' => empty($order['shipping']['state']) ? 'NI' : $order['shipping']['state'],
                    'shipping_country' => $this->formatShippingCountry($order),
                    'shipping_price' => empty($order['freight']['actualAmount']) ? 'NI' : $order['freight']['actualAmount'],
                    'total_price' => $order['totalAmount'],
                    'items' => [],
                    'site' => $order['site'],
                ];

                $itemCheckpoint = [];
                foreach ($order['items'] as $item) {
                    $orderTransform['items'][] = [
                        'store_item_id' => $item['id'],
                        'sku' => $item['skuSellerId'],
                        'sku_name' => $item['name'],
                        'unit_price' => $item['salePrice'],
                        'shipping_price' => !empty($item['freight']['actualAmount']) ? $item['freight']['actualAmount'] : 0,
                        'quantity' => 1,
                        'total_commission' => 0,
                    ];

                    $itemCheckpoint[] = $item['id'];
                }

                $extra['StorePurchasedDate'] = isset($order['purchasedAt']) ? $order['purchasedAt'] : '';
                if (isset($order['approvedAt']) && !empty($order['approvedAt'])) {
                    $extra['StoreApprovedDate'] = isset($order['approvedAt']) ? $order['approvedAt'] : '';
                }
                if(isset($order['shippingInformationsList'])) {
                    $extra['StoreEstimatedDeliveryDate'] = isset($order['shippingInformationsList']['scheduledDate']) ? $order['shippingInformationsList']['scheduledDate'] : '';
                }

                if (isset($order['promisedDeliveryDate'])) {
                    $extra['StoreEstimatedDeliveryDate'] = isset($order['promisedDeliveryDate']) ? $order['promisedDeliveryDate'] : '';
                }

                // merging totalDiscountAmount if isset on $extra array
                if (isset($order['totalDiscountAmount'])) {
                    $extra['totalDiscountAmount'] = isset($order['totalDiscountAmount']) ? $order['totalDiscountAmount'] : '';
                }

                $orderTransform['extra'] = json_encode($extra);
                $orderTransform['promisedDeliveryDate'] = isset($order['shippingInformationsList']['scheduledDate']) ? $order['shippingInformationsList']['scheduledDate'] : '';
                $orderTransform['approvedAt'] = $order['approvedAt'] ?? '';
                $orderTransform['purchasedAt'] = $order['purchasedAt'] ?? '';

                if (count($itemCheckpoint) !== count(array_unique($itemCheckpoint))) {
                    continue;
                }

                $result['orders'][] = $orderTransform;
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 500);
        }

        return $result;
    }

    /**
     * @param string $name
     * @return array
     */
    private function formatName(string $name)
    {
        $nameExploded = explode(' ', ucwords(strtolower(trim($name))));

        return array($nameExploded[0], last($nameExploded));
    }

    /**
     * @param $order
     * @return mixed|string
     */
    private function formatShippingCountry($order)
    {
        if (!empty($order['shipping']['countryId'])
            && array_key_exists($order['shipping']['countryId'], self::COUNTRY_MAP)) {
            return self::COUNTRY_MAP[$order['shipping']['countryId']];
        }

        return 'BR';
    }
}