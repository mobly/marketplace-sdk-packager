<?php

namespace Mobly\MarketplaceSdk\Integrators\Cnova;

class StockTransformer
{
    /**
     * @param array $data
     * @param array $product
     * @param array $sku
     * @return array
     */
    public function transform(array $data, array $product, array $sku)
    {
        $transformedStocks = [
            'quantity' => $sku['quantity'],
            'crossDockingTime' => $product['chain_time'] ?? 1,
            'warehouse' => $data['request']['warehouse_id'],
        ];

        return $transformedStocks;
    }

    /**
     * @param array $sku
     * @return array
     */
    public function transformStockV4(array $sku)
    {
        $data = [
            'idSkuLojista' => $sku['sku'],
            'estoque' => [
                'quantidade' => $sku['quantity'],
                'tempoDePreparacao' => $sku['chain_time']
            ]
        ];

        return $data;
    }
}