<?php

namespace Mobly\MarketplaceSdk\Integrators\Cnova;

use Illuminate\Support\Arr;
use Mobly\MarketplaceSdk\Helpers\Helper;

class ProductTransformer
{
    /**
     * @param array $data
     * @return array
     */
    public function transform(array $data)
    {
        $formattedProducts = [];
        $skusWithErrors = [];

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {
                try {
                    $formattedProducts[] = $this->transformProductV4($product, $sku);
                } catch (\Exception $exception) {
                    $skusWithErrors[$sku['sku']][] = ['message' => $exception->getMessage()];
                }
            }
        }

        return [$formattedProducts, $skusWithErrors];
    }

    /**
     * @param $sku
     * @return string|null
     */
    public function getSpecialPrice($sku)
    {
        if (!empty($sku['special_price'])) {
            return Helper::moneyFormat($sku['special_price']);
        }

        return null;
    }

    /**
     * @param array $product
     * @param array $sku
     * @return array
     */
    private function transformProductV2(array $product, array $sku)
    {
        $productName = Helper::_concatTitle($product['name'], $sku['superAttribute']);
        $productName = Helper::removeInvalidWords($productName);

        $images = [];
        foreach ($product['images'] as $image) {
            $images[] = $image;
        }

        $attributes = [];
        foreach ($product['attributes'] as $key => $attribute) {
            $attributes[] = [
                'name' => $key,
                'value' => $attribute,
            ];
        }

        $formattedProduct = [
            'skuSellerId' => $sku['sku'],
            'title' => $productName,
            'description' => Helper::removeInvalidWords(strip_tags($product['description'])),
            'brand' => $product['brand'],
            'images' => $images,
            'categories' => [$this->transformCategoryV2($product['path_category'])],
            'gtin' => [
                $sku['ean'] ?? '',
            ],
            'price' => [
                'default' => $sku['price'],
                'offer' => $this->getSpecialPrice($sku) > 0 ? $this->getSpecialPrice($sku) : $sku['price'],
            ],
            'stock' => [
                'quantity' => $sku['quantity'],
                'crossDockingTime' => $product['chain_time'] ?? 1,
            ],
            'dimensions' => [
                'weight' => $product['dimensions']['weight'],
                'length' => $product['dimensions']['length'],
                'width' => $product['dimensions']['width'],
                'height' => $product['dimensions']['height'],
            ],
            'attributes' => $attributes,
        ];

        return $formattedProduct;
    }

    /**
     * @param array $product
     * @param array $sku
     * @return array
     * @throws \Exception
     */
    private function transformProductV4(array $product, array $sku)
    {
        $productName = Helper::_concatTitle($product['name'], $sku['superAttribute']);
        $productName = Helper::removeInvalidWords($productName);

        $images = [];
        foreach ($product['images'] as $image) {

            if ($this->validateImageSize($image)) {
                $images[] = $image;
            }
        }

        if (!count($images)) {
            throw new \Exception("No valid images were found (images must be between 600 and 1000 pixels [width/height])");
        }

        if (count($images) > 4) {
            $images = array_slice($images, 0, 4);
        }

        $attributesVariation = [];
        $attributesProduct = [];

        if (!empty($product['store_attributes_variation'])) {

            foreach ($product['store_attributes_variation'] as $key => $attribute) {

                $attributesVariation[] = [
                    'idUda' => $attribute['external_id'],
                    'valor' => $attribute['value']
                ];
            }
        }

        if (!empty($product['store_attributes_product'])) {

            foreach ($product['store_attributes_product'] as $key => $attribute) {
                $attributesProduct[] = [
                    'idUda' => $attribute['external_id'],
                    'valor' => $attribute['value']
                ];
            }
        }

        $formattedProduct = [
            'idItem' => $sku['sku_id'],
            'titulo' => $productName,
            'idCategoria' => $product['store_category_external_id'],
            'descricao' => Helper::removeInvalidWords(strip_tags($product['description'])),
            'atributos' => $attributesProduct,
            'garantia' => empty($product['warranty']) ? 3 : $product['warranty'],
            'marca' => $product['brand'],
            'skus' => [
                [
                    'idSkuLojista' => $sku['sku'],
                    'gtin' => $sku['ean'] ?? '',
                    'imagens' => $images,
                    'preco' => [
                        'oferta' => !empty($this->getSpecialPrice($sku))
                            ? $this->getSpecialPrice($sku)
                            : Helper::moneyFormat($sku['price']),
                        'padrao' => Helper::moneyFormat($sku['price'])
                    ],
                    'estoque' => [
                        'quantidade' => $sku['quantity'],
                        'tempoDePreparacao' => $product['chain_time'] ?? 1
                    ],
                    'dimensao' => [
                        'altura' => !empty($product['height']) ? $product['height'] : 1,
                        'largura' => !empty($product['width']) ? $product['width'] : 1,
                        'profundidade' => !empty($product['length']) ? $product['length'] : 1,
                        'peso' => !empty($product['weight']) ? $product['weight'] : 1
                    ],
                    'atributos' => $attributesVariation
                ]
            ]
        ];

        return $formattedProduct;
    }

    /**
     * @param string $categoryPath
     * @return mixed
     */
    private function transformCategoryV2(string $categoryPath)
    {
        $categoryPath = ltrim($categoryPath, '##');
        $categoryPath = rtrim($categoryPath, '##');

        return str_replace('##', '>', $categoryPath);
    }

    /**
     * @param $image
     * @return bool
     */
    private function validateImageSize($image)
    {
        list($width, $heigth) = $this->getimgsize($image);

        if (($width >= 600 && $width <= 1000) && ($heigth >= 600 && $heigth <= 1000)) {
            return true;
        }

        return false;
    }

    /**
     * Get Image Size
     *
     * @param string $url
     * @param string $referer
     * @return array
     */
    function getimgsize($url, $referer = '')
    {
        // Set headers
        $headers = array('Range: bytes=0-131072');
        if (!empty($referer)) {
            array_push($headers, 'Referer: ' . $referer);
        }

        // Get remote image
        $ch = curl_init();
        $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);

        $data = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errno = curl_errno($ch);
        curl_close($ch);

        // Get network stauts
        if ($http_status != 200) {
            echo 'HTTP Status[' . $http_status . '] Errno [' . $curl_errno . ']';
            return [0, 0];
        }

        // Process image
        $image = imagecreatefromstring($data);
        $dims = [imagesx($image), imagesy($image)];
        imagedestroy($image);

        return $dims;
    }

    /**
     * @param $cnovaProducts
     * @return array
     */
    public function transfProductsStore($cnovaProducts)
    {
        $formattedProducts = [];

        foreach ($cnovaProducts as $product) {
            $formattedProducts[] = [
                'sku' => Arr::get($product, 'skuSellerId', null),
                'store_sku' => Arr::get($product, 'skuSiteId', null),
                'stock' => Arr::get($product, 'stocks.0.quantity', 0),
                'price' => Arr::get($product, 'prices.0.default', 0),
                'promotional_price' => Arr::get($product, 'prices.0.offer', 0)
            ];
        }

        return $formattedProducts;
    }
}


