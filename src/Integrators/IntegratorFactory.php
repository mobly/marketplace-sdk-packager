<?php

namespace Mobly\MarketplaceSdk\Integrators;

use Illuminate\Http\Response;

class IntegratorFactory
{
    const STORE_INTEGRATOR_CLASS = [
        'CRF' => CarrefourIntegrator::class,
        'AMZ' => AmazonIntegrator::class,
        'MLB' => MercadoLivreIntegrator::class,
        'B2W' => B2wIntegrator::class,
        'NVC' => CnovaIntegrator::class,
        'MGL' => MagaluIntegrator::class,
        'WLM' => WalmartIntegrator::class,
        'CLB' => ColomboIntegrator::class,
        'BUS' => BuscapeIntegrator::class,
        'EYD' => EasyDecorIntegrator::class,
        'MLG' => MercadoLivreGuldiIntegrator::class,
        'LRY' => LeroyIntegrator::class,
        'GPA' => GpaIntegrator::class,
        'LEX' => LexIntegrator::class,
        'KEV' => KevIntegrator::class,
    ];

    /**
     * @param string $storeCode
     * @return mixed
     * @throws \Exception
     */
    public static function createIntegrator(string $storeCode)
    {
        $className = self::STORE_INTEGRATOR_CLASS[$storeCode] ?? null;

        if (null != $className && class_exists($className)) {
            $storeIntegratorInstance = new $className();

            if ($storeIntegratorInstance instanceof IntegratorAbstract) {
                return $storeIntegratorInstance;
            }
        }

        throw new \Exception('Integrator not found for the informed store', Response::HTTP_NOT_FOUND);
    }
}