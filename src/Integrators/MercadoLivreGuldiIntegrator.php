<?php

namespace Mobly\MarketplaceSdk\Integrators;

class MercadoLivreGuldiIntegrator extends MercadoLivreIntegrator
{
    /**
     * MercadoLivreGuldiIntegrator constructor.
     */
    public function __construct()
    {
        $this->price = new MercadoLivreGuldi\Price();
        $this->product = new MercadoLivreGuldi\Product();
        $this->stock = new MercadoLivreGuldi\Stock();
    }
}