<?php
/**
 * Created by PhpStorm.
 * User: hermes
 * Date: 07/05/19
 * Time: 17:46
 */

namespace Mobly\MarketplaceSdk\Integrators;

use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use Mobly\MarketplaceSdk\Validators\EasyDecor\EasyDecorValidator;
use Mobly\MarketplaceSdk\Validators\EasyDecor\ProductValidator;
use Mobly\MarketplaceSdk\Integrators\EasyDecor\Product;

class EasyDecorIntegrator extends IntegratorAbstract
{

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     */
    public function integrate(string $httpMethod, array $data)
    {
        $easyDecorValidator = new EasyDecorValidator($data);
        $easyDecorValidator->validate();

        $productsResults = [
            'success' => array(),
        ];

        $productValidator = new ProductValidator();

        $product = new Product($this, $productValidator);

        $formattedProducts = $product->formatProducts($data);

        $client = new Client();

        foreach ($formattedProducts as $sku => $products) {
            foreach ($products as $formattedProduct) {
                try {
                    usleep(100000);

                    $request = new Request($httpMethod, $data['request']['url']);
                    $request->setOptions([
                        'headers' => $data['request']['headers'],
                        'json' => ['product' => $formattedProduct],
                    ]);

                    $response = $client->request($request);

                    $productsResults['success'][$sku][] = (array)$response;
                } catch (\Exception $exception) {
                    unset($formattedProduct[$sku]);
                    $this->addErrorsWithKey($exception->getMessage(), $sku);
                    continue;
                }
            }
        }

        $productsResults['errors'] = $this->getErrors();

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(string $httpMethod, array $data)
    {
        $b2wValidator = new EasyDecorValidator($data);

        if ($b2wValidator->validate()) {

            $productsResults = [
                'success' => [],
            ];

            $productValidator = new ProductValidator();

            $product = new Product($this, $productValidator);
            $formattedProducts = $product->formatProducts($data);

            foreach ($formattedProducts as $sku => $products) {
                foreach ($products as $formattedProduct) {
                    try {
                        $request = new Request($httpMethod, $this->_makeUrl($formattedProduct['sku'],
                            $data['request']['url']));

                        $request->setOptions([
                            'headers' => $data['request']['headers'],
                            'json' => ['product' => $formattedProduct],
                        ]);

                        $client = new Client();

                        $response = $client->request($request);

                        $productsResults['success'][$sku][] = (array)$response;
                    } catch (\Exception $exception) {
                        unset($formattedProduct[$sku]);
                        $this->addErrorsWithKey($exception->getMessage(), $sku);
                        continue;
                    }
                }
            }

            $productsResults['errors'] = $this->getErrors();

            return $productsResults;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed|void
     */
    public function getStatus(string $httpMethod, array $data)
    {
        // TODO: Implement getStatus() method.
    }


    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed|void
     */
    public function getOrders(string $httpMethod, array $data)
    {
        // TODO: Implement getOrders() method.
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed|void
     */
    public function getOrder(string $httpMethod, array $data)
    {
        // TODO: Implement getOrder() method.
    }

    /**
     * @param $replaceParam
     * @param $url
     * @return mixed
     */
    private function _makeUrl($replaceParam, $url)
    {
        return str_replace('%s', $replaceParam, $url);
    }
}