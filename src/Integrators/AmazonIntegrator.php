<?php

namespace Mobly\MarketplaceSdk\Integrators;

use League\Csv\Exception;
use Mobly\MarketplaceSdk\Helpers\Helper;
use Mobly\MarketplaceSdk\Integrators\Amazon\Auth;
use Mobly\MarketplaceSdk\Integrators\Amazon\CreateCondenseProductFeed;
use Mobly\MarketplaceSdk\Integrators\Amazon\CreateDeleteProductFeed;
use Mobly\MarketplaceSdk\Integrators\Amazon\CreateImagesFeed;
use Mobly\MarketplaceSdk\Integrators\Amazon\CreateInventoryFeed;
use Mobly\MarketplaceSdk\Integrators\Amazon\CreateOrderAcknowledgementFeed;
use Mobly\MarketplaceSdk\Integrators\Amazon\CreateOrderFulfillmentFeed;
use Mobly\MarketplaceSdk\Integrators\Amazon\CreatePriceFeed;
use Mobly\MarketplaceSdk\Integrators\Amazon\CreateProductFeed;
use Mobly\MarketplaceSdk\Integrators\Amazon\FeedFactory;
use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Exception\GuzzleException;

class AmazonIntegrator extends IntegratorAbstract
{
    const ORDERS_ENDPOINT = '/Orders/2013-09-01';

    const PROCESSING_CODE_RESULT_NOT_READY = 'FeedProcessingResultNotReady';

    protected $auth = null;

    public function __construct()
    {
        $this->auth = new Auth();
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws Exception
     */
    public function integrate(string $httpMethod, array $data)
    {
        $feed = $this->clientFeed(new CreateProductFeed(), $data);

        $response = $this->submitFeed($httpMethod, $feed, $data);

        $responseArray = json_decode(json_encode($response), true);

        return [
            'import_id' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo']['FeedSubmissionId'],
            'body' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo'],
            'errors' => $this->getErrors()
        ];
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getStatus(string $httpMethod, array $data)
    {
        try {
            $params = $data['request']['params'];

            $params['Signature'] = $this->auth->signParameters($params, $data['request']['SecretKey'], $httpMethod);

            $data['request']['url'] .= '?' . http_build_query($params, null, '&',
                    PHP_QUERY_RFC3986);

            $request = new Request($httpMethod, $data['request']['url']);
            $request->addOption(['headers' => ['Content-Type' => 'text/xml; charset=iso-8859-1']]);

            $client = new \Mobly\MarketplaceSdk\Services\V2\Client();

            $response = $client->request($request);

            $content = $response->getBody()->getContents();

            return simplexml_load_string($content);

        } catch (GuzzleException $exception) {

            $content = $exception->getResponse()->getBody()->getContents();

            throw new \Exception($content, $exception->getCode());
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function integrateImage(string $httpMethod, array $data)
    {
        $feed = $this->clientFeed(new CreateImagesFeed(), $data);

        $response = $this->submitFeed($httpMethod, $feed, $data);

        $responseArray = json_decode(json_encode($response), true);

        return [
            'import_id' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo']['FeedSubmissionId'],
            'body' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo'],
            'errors' => $this->getErrors()
        ];
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function integrateInventory(string $httpMethod, array $data)
    {
        $feed = $this->clientFeed(new CreateInventoryFeed(), $data);

        $response = $this->submitFeed($httpMethod, $feed, $data);

        $responseArray = json_decode(json_encode($response), true);

        return [
            'import_id' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo']['FeedSubmissionId'],
            'body' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo'],
        ];
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function integratePrice(string $httpMethod, array $data)
    {
        $feed = $this->clientFeed(new CreatePriceFeed(), $data);

        $response = $this->submitFeed($httpMethod, $feed, $data);

        $responseArray = json_decode(json_encode($response), true);

        return [
            'import_id' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo']['FeedSubmissionId'],
            'body' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo'],
        ];
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function integrateCondenseProduct(string $httpMethod, array $data)
    {
        $feed = $this->clientFeed(new CreateCondenseProductFeed(), $data);

        $response = $this->submitFeed($httpMethod, $feed, $data);

        $responseArray = json_decode(json_encode($response), true);

        return [
            'import_id' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo']['FeedSubmissionId'],
            'body' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo'],
        ];
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     * @throws Exception
     * @throws \Exception
     */
    public function getOrder(string $httpMethod, array $data)
    {
        $params = $data['request']['params'];
        $params['Signature'] = $this->auth->signParameters($params, $data['request']['SecretKey'], $httpMethod,
            self::ORDERS_ENDPOINT);

        $data['request']['url'] .= '?' . http_build_query($params, null,
                '&', PHP_QUERY_RFC3986);

        $request = new Request($httpMethod, $data['request']['url']);

        $client = new Client();
        $rawResponse = $client->request($request);

        $response = json_decode(json_encode($rawResponse), true);

        if (empty($response['GetOrderResult']['Orders']['Order'])) {
            throw new \Exception('Order(s) not found');
        }

        if (!empty($response['GetOrderResult']['Orders']['Order'][0])) {
            $response = $this->transformOrders($response['GetOrderResult']['Orders']['Order']);
        } else {
            $response = $this->transformOrders([$response['GetOrderResult']['Orders']['Order']]);
        }

        $response['raw'] = $rawResponse;
        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function getOrders(string $httpMethod, array $data)
    {
        $params = $data['request']['params'];
        $params['Signature'] = $this->auth->signParameters($params, $data['request']['SecretKey'], $httpMethod,
            self::ORDERS_ENDPOINT);
        $data['request']['url'] .= '?' . http_build_query($params, null, '&',
                PHP_QUERY_RFC3986);

        $request = new Request($httpMethod, $data['request']['url']);

        $client = new Client();
        $rawResponse = $client->request($request);

        $parsedResponse = json_decode(json_encode($rawResponse), true);
        $orders = [];
        $nextToken = '';
        if (isset($parsedResponse['ListOrdersResult']['Orders'])) {
            $nextToken = $parsedResponse['ListOrdersResult']['NextToken'] ?? false;
            $orders = $parsedResponse['ListOrdersResult']['Orders']['Order'] ?? [];
        }

        if (isset($parsedResponse['ListOrdersByNextTokenResult']['Orders'])) {
            $nextToken = $parsedResponse['ListOrdersByNextTokenResult']['NextToken'] ?? false;
            $orders = $parsedResponse['ListOrdersByNextTokenResult']['Orders']['Order'] ?? [];
        }

        $response = $this->transformOrders($orders, $nextToken);
        $response['raw'] = $rawResponse;
        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     * @throws Exception
     * @throws \Exception
     */
    public function getOrderItems(string $httpMethod, array $data)
    {
        $params = $data['request']['params'];
        $params['Signature'] = $this->auth->signParameters($params, $data['request']['SecretKey'], $httpMethod,
            self::ORDERS_ENDPOINT);
        $data['request']['url'] .= '?' . http_build_query($params, null, '&',
                PHP_QUERY_RFC3986);

        $request = new Request($httpMethod, $data['request']['url']);

        $client = new Client();
        $rawResponse = $client->request($request);

        $parsedResponse = json_decode(json_encode($rawResponse), true);

        $nextToken = '';
        $orderItems = [];

        if (isset($parsedResponse['ListOrderItemsResult']['OrderItems'])) {
            $nextToken = $parsedResponse['ListOrderItemsResult']['NextToken'] ?? false;
            $orderItems = $parsedResponse['ListOrderItemsResult']['OrderItems']['OrderItem'];
        }

        if (isset($parsedResponse['ListOrderItemsByNextTokenResult']['OrderItems'])) {
            $nextToken = $parsedResponse['ListOrderItemsByNextTokenResult']['NextToken'] ?? false;
            $orderItems = $parsedResponse['ListOrderItemsByNextTokenResult']['OrderItems']['OrderItem'];
        }

        $response = $this->transformOrderItems([$orderItems], $nextToken);
        $response['raw'] = $rawResponse;
        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function integrateOrderFulfillment(string $httpMethod, array $data)
    {
        $feed = $this->clientFeed(new CreateOrderFulfillmentFeed(), $data);

        unset($data['request']['orders']);
        $response = $this->submitFeed($httpMethod, $feed, $data);

        $responseArray = json_decode(json_encode($response), true);

        return [
            'import_id' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo']['FeedSubmissionId'],
            'body' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo'],
        ];
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function integrateOrderAcknowledgement(string $httpMethod, array $data)
    {
        $feed = $this->clientFeed(new CreateOrderAcknowledgementFeed(), $data);

        $response = $this->submitFeed($httpMethod, $feed, $data);

        $responseArray = json_decode(json_encode($response), true);

        return [
            'import_id' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo']['FeedSubmissionId'],
            'body' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo'],
        ];
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function deleteProducts(string $httpMethod, array $data)
    {
        $feed = $this->clientFeed(new CreateDeleteProductFeed(), $data);

        $response = $this->submitFeed($httpMethod, $feed, $data);

        $responseArray = json_decode(json_encode($response), true);

        return [
            'import_id' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo']['FeedSubmissionId'],
            'body' => $responseArray['SubmitFeedResult']['FeedSubmissionInfo'],
        ];
    }

    /**
     * @param array $orders
     * @param bool $nextToken
     * @return mixed
     * @throws \Exception
     */
    private function transformOrders(array $orders, $nextToken = false)
    {
        $result['nextToken'] = $nextToken;
        $result['orders'] = [];

        // check if it is a multidimensional array and fix it if not
        if (!is_array(current($orders))) {
            $orders = array($orders);
        }

        try {
            foreach ($orders as $order) {

                if (empty($order)) {
                    continue;
                }

                $buyerName = $this->_formatName($order);

                $formattedAddress = !empty($order['ShippingAddress']['AddressLine1'])
                    ? Helper::getFormattedAddress($order['ShippingAddress']['AddressLine1'])
                    : null;

                $orderTransform = [
                    'store_order_number' => $order['AmazonOrderId'],
                    'store_order_status' => $order['OrderStatus'],
                    'customer_first_name' => empty($buyerName[0]) ? 'NI' : $buyerName[0],
                    'customer_last_name' => empty($buyerName[1]) ? 'NI' : $buyerName[1],
                    'customer_document' => !empty($order['BuyerTaxInfo'])
                        ? preg_replace('/[^0123456789x]/i', '', $order['BuyerTaxInfo']['TaxClassifications']['TaxClassification']['Value'])
                        : 'NI',
                    'customer_email' => $order['BuyerEmail'] ?? 'NI',
                    'customer_phone' => !empty($order['ShippingAddress'])
                    && !empty($order['ShippingAddress']['Phone'])
                        ? preg_replace('/[^0-9x]/i', '', $order['ShippingAddress']['Phone'])
                        : 'NI',
                    'customer_gender' => '',
                    'billing_postcode' => !empty($order['ShippingAddress'])
                        ? preg_replace('/[^0-9]/', '', $order['ShippingAddress']['PostalCode'])
                        : 'NI',
                    'billing_street' => !empty($formattedAddress) ? $formattedAddress['street'] : 'NI',
                    'billing_street_number' => !empty($formattedAddress) ? $formattedAddress['number'] : 'NI',
                    'billing_neighborhood' => !empty($order['BuyerCounty']) ? $order['BuyerCounty'] : 'NI',
                    'billing_city' => !empty($order['ShippingAddress'])
                        ? $order['ShippingAddress']['City'] ?? 'NI'
                        : 'NI',
                    'billing_state' => !empty($order['ShippingAddress'])
                        ? $order['ShippingAddress']['StateOrRegion'] ?? 'NI'
                        : 'NI',
                    'billing_country' => !empty($order['ShippingAddress'])
                        ? $order['ShippingAddress']['CountryCode'] ?? 'NI'
                        : 'NI',
                    'shipping_postcode' => !empty($order['ShippingAddress'])
                        ? preg_replace('/[^0-9]/', '', $order['ShippingAddress']['PostalCode'])
                        : 'NI',
                    'billing_complement' => !empty($order['ShippingAddress']['AddressLine2'])
                        ? $order['ShippingAddress']['AddressLine2']
                        : 'NI',
                    'shipping_street' => !empty($formattedAddress) ? $formattedAddress['street'] : 'NI',
                    'shipping_street_number' => !empty($formattedAddress) ? $formattedAddress['number'] : 'NI',
                    'shipping_neighborhood' => !empty($order['BuyerCounty']) ? $order['BuyerCounty'] : 'NI',
                    'shipping_complement' => !empty($order['ShippingAddress']['AddressLine2'])
                        ? $order['ShippingAddress']['AddressLine2']
                        : 'NI',
                    'shipping_city' => !empty($order['ShippingAddress'])
                        ? $order['ShippingAddress']['City'] ?? 'NI'
                        : 'NI',
                    'shipping_state' => !empty($order['ShippingAddress'])
                        ? $order['ShippingAddress']['StateOrRegion'] ?? 'NI'
                        : 'NI',
                    'shipping_country' => !empty($order['ShippingAddress'])
                        ? $order['ShippingAddress']['CountryCode'] ?? 'NI'
                        : 'NI',
                    'shipping_price' => 0, // AMZ não traz esta informação por order
                    'total_price' => !empty($order['OrderTotal']) ? $order['OrderTotal']['Amount'] : 0,
                    'store_last_updated_at' => date('Y-m-d H:i:s',
                        strtotime(urldecode($order['LastUpdateDate']))),
                ];

                $extra['StorePurchasedDate'] = isset($order['PurchaseDate']) ? $order['PurchaseDate'] : '';
                $extra['StoreEstimatedDeliveryDate'] = isset($order['EarliestShipDate']) ? $order['EarliestShipDate'] : '';

                $orderTransform['extra'] = json_encode($extra);

                $result['orders'][] = $orderTransform;
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $result;
    }

    /**
     * @param array $orderItems
     * @param bool $nextToken
     * @return mixed
     * @throws \Exception
     */
    private function transformOrderItems(array $orderItems, $nextToken = false)
    {
        $result['nextToken'] = $nextToken;
        $result['items'] = [];

        try {

            if ($this->_hasItems($orderItems)) {

                $orderItems = $orderItems[0];
            }

            foreach ($orderItems as $orderItem) {

                $itemQuantity = $orderItem['QuantityOrdered'] == 0 ? $orderItem['ProductInfo']['NumberOfItems'] : $orderItem['QuantityOrdered'];

                for ($i = 1; $i <= $itemQuantity; $i++) {

                    $orderItemTransform = [
                        'store_item_id' => "{$orderItem['OrderItemId']}_{$i}",
                        'sku' => $orderItem['SellerSKU'],
                        'sku_name' => $orderItem['Title'],
                        'unit_price' => !empty($orderItem['ItemPrice'])
                            ? $orderItem['ItemPrice']['Amount'] / $itemQuantity
                            : 0,
                        'shipping_price' => !empty($orderItem['ShippingPrice'])
                            ? $orderItem['ShippingPrice']['Amount'] / $itemQuantity
                            : 0,
                        'quantity' => 1,
                        'total_commission' => 0,
                    ];

                    $result['items'][] = $orderItemTransform;
                }
            }

        } catch (\Exception $ex) {

            throw new \Exception($ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $result;
    }

    /**
     * @param FeedFactory $feedFactory
     * @param array $data
     * @return mixed
     */
    private function clientFeed(FeedFactory $feedFactory, array $data)
    {
        return $feedFactory->getXMLFeed($data, $this);
    }

    /**
     * @param $httpMethod
     * @param $feed
     * @param $data
     * @return array|mixed
     * @throws Exception
     */
    private function submitFeed($httpMethod, $feed, $data)
    {
        $url = $this->makeUrl($httpMethod, $feed, $data);
        $response = $this->send($httpMethod, $feed, $url, $data);
        return $response;
    }

    /**
     * @param $httpMethod
     * @param $feed
     * @param $data
     * @return mixed
     */
    private function makeUrl($httpMethod, $feed, $data)
    {
        $data['request']['params']['ContentMD5Value'] = Helper::getContentMd5($feed);
        $data['request']['params']['Signature'] = $this->auth->signParameters($data['request']['params'],
            $data['request']['SecretKey'], $httpMethod);
        $data['request']['url'] .= '?' . http_build_query($data['request']['params'], null,
                '&', PHP_QUERY_RFC3986);
        return $data['request']['url'];
    }

    /**
     * @param $httpMethod
     * @param $feed
     * @param $url
     * @param $data
     * @return array|mixed
     * @throws Exception
     */
    private function send($httpMethod, $feed, $url, $data)
    {
        $request = new Request($httpMethod, $url);
        $request->addOption(['headers' => ['Content-Type' => 'text/xml; charset=iso-8859-1']]);
        $request->addOption([
            'body' => $feed
        ]);

        $client = new Client();

        $response = $client->request($request);

        return $response;
    }

    /**
     * @param $order
     * @return array
     */
    private function _formatName($order)
    {
        if (!empty($order['BuyerName'])) {
            if (strpos($order['BuyerName'], '@') &&
                !empty($order['ShippingAddress']['Name'])) {
                $buyerName = explode(' ', $order['ShippingAddress']['Name'], 2);
                return $buyerName;
            };

            $buyerName = explode(' ', $order['BuyerName'], 2);
            return $buyerName;
        }

        return [];
    }

    /**
     * @param $orderItems
     * @return bool
     */
    private function _hasItems($orderItems)
    {
        return !empty($orderItems[0][0]);
    }
}