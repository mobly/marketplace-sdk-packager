<?php


namespace Mobly\MarketplaceSdk\Integrators;

use Illuminate\Support\Arr;
use Mobly\MarketplaceSdk\Exceptions\CnovaException;
use Mobly\MarketplaceSdk\Integrators\Cnova\ProductTransformer;
use Mobly\MarketplaceSdk\Integrators\Cnova\Order;
use Mobly\MarketplaceSdk\Integrators\Cnova\StatusTransformer;
use Mobly\MarketplaceSdk\Integrators\Cnova\StockTransformer;
use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use Mobly\MarketplaceSdk\Validators\Cnova\CnovaValidator;
use Mobly\MarketplaceSdk\Integrators\Cnova\PriceTransformer;

class CnovaIntegrator extends IntegratorAbstract
{

    // not used categories, worthless for importation
    const IGNORED_CATEGORIES = [
//        '2599',     // Artesanato
//        '448',      // Calçados
//        '29',       // Câmeras, Filmadoras e Drones
//        '2809',     // Ar e Ventilação
//        '983',      // Bebês
//        '336',      // Games
//        '56',       // Informática
//        '2031',     // Tablets
//        '2042',     // Natal
//        '1639',     // Papelaria
//        '2294',     // Pet Shop
//        '2285',     // Produtos de Limpeza
//        '2168',     // Joias
//        '418',      // Esporte & Lazer
//        '836',      // Automotivo
//        '2804',     // Artigos para Festas
//        '2596',     // Bebidas
//        '977',      // Brinquedos
//        '1',        // TV e Video
//        '3279',     // Áudio
//        '2471',     // Instrumentos Musicais
//        '102',      // Beleza e Saúde
//        '1886',     // Perfumaria
//        '38',       // Telefones e Celulares
//        '1017',     // Linha Industrial
//        '1733',     // Relogios
        '833',      // DVDs e Blue Ray
        '2673',     // Alameda de Serviços
        '2339',     // Alimentos
        '1141',     // eBooks
        '484',      // Livros
        '1734',     // Moda
    ];

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool|mixed
     * @throws \Exception
     */
    public function integrate(string $httpMethod, array $data)
    {
        $productsResults = [
            'errors' => [],
        ];

        $cNovaValidator = new CnovaValidator($data);

        if ($cNovaValidator->validate()) {
            $productTransformer = new ProductTransformer();
            list($formattedProducts, $skusWithErrors) = $productTransformer->transform($data);

            $productsResults['errors'] = $skusWithErrors;

            if (count($formattedProducts)) {
                $request = new Request($httpMethod, $data['request']['url']);

                $request->setOptions([
                    'headers' => $data['request']['headers'],
                ]);

                if ($data['api_ver'] == 'v2') {
                    $request->addOption([
                        'json' => $formattedProducts,
                    ]);
                } else {
                    $request->addOption([
                        'json' => ['itens' => $formattedProducts],
                    ]);
                }

                $client = new Client();
                $client->request($request);
            }

            return $productsResults;
        }

        return false;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function checkProduct(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => [],
        ];

        $client = new Client();
        try {
            $request = new Request($httpMethod,
                $this->_makeUrl($data['product']['sku'], $data['request']['url']));

            $request->addOption([
                'headers' => $data['request']['headers'],
            ]);

            $response = $client->request($request);
            $productsResults['success'][$data['product']['sku']] = (array)$response;
        } catch (\Exception $exception) {
            $productsResults['errors'][$data['product']['sku']] = ['message' => $exception->getMessage()];
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function checkPrice(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => [],
        ];

        $client = new Client();
        try {
            $request = new Request($httpMethod, $data['request']['url']);

            $request->addOption([
                'headers' => $data['request']['headers'],
            ]);

            $response = $client->request($request);


            $priceTransformer = new PriceTransformer();
            list($transformedPrices, $skusWithErrors) = $priceTransformer->transformCheckPrices((array)$response);

            $productsResults['success'] = $transformedPrices;
            $productsResults['errors'] = $skusWithErrors;
        } catch (\Exception $exception) {
            $productsResults['errors'] = ['message' => $exception->getMessage()];
        }

        return $productsResults;
    }


    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function updateStatus(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => []
        ];

        $productTransformer = new StatusTransformer();

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {

                try {
                    $formattedProducts = $productTransformer->transform($sku);

                    $request = new Request($httpMethod, $data['request']['url']);

                    $request->setOptions([
                        'headers' => $data['request']['headers'],
                        'json' => $formattedProducts,
                    ]);

                    $client = new Client();
                    $response = $client->request($request);
                    $productsResults['success'][$sku['sku']][] = (array)$response;

                } catch (\Exception $e) {
                    $productsResults['errors'][$sku['sku']][] = ['message' => $e->getMessage()];
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     *
     * (round(($sku['price']/2), 2) > $sku['special_price']) {
     * throw new \Exception('sku special_price cannot be set to a value inferior to 50% of price');
     * }
     *
     * @return array
     */
    public function integratePrice(string $httpMethod, array $data)
    {
        $priceTransformer = new PriceTransformer();
        $productsResults = [
            'success' => [],
            'errors' => []
        ];

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {
                try {

                    if (!empty($data['request']['configurations']['price_decrease_percentage'])
                        && $data['request']['configurations']['price_decrease_percentage'] > 0) {

                        $transformedPrices = $priceTransformer->transformDecrease($sku);
                        if (empty($transformedPrices)) {
                            throw new \Exception('Not found to formatted');
                        }

                        $targetDePrice = round($transformedPrices['default'], 2);

                        $targetPorPrice = $transformedPrices['offer'];

                        $currentCnovaDePrice = $this->checkProductToPrice($data);

                        if ($currentCnovaDePrice == $targetDePrice) {
                            $transformedPrices['default'] = $targetDePrice;
                            $transformedPrices['offer'] = $targetPorPrice;
                        } else {
                            $currentCnovaDePrice = round($currentCnovaDePrice / 2, 2) + 1;

                            $transformedPrices['default'] = $currentCnovaDePrice;
                            $transformedPrices['offer'] = $currentCnovaDePrice;

                            if ($currentCnovaDePrice < $targetDePrice) {
                                $transformedPrices['default'] = $targetDePrice;
                                $transformedPrices['offer'] = $targetDePrice;
                            }
                        }

                        $response = $this->sendPriceToStore($httpMethod, $data, $transformedPrices);
                        $productsResults['success'][$sku['sku']][] = (array)$response;

                    } else {

                        $transformedPrices = $priceTransformer->transform($data);
                        if (empty($transformedPrices)) {
                            throw new \Exception('Not found to formatted');
                        }

                        $response = $this->sendPriceToStore($httpMethod, $data, $transformedPrices);
                        $productsResults['success'][$sku['sku']][] = (array)$response;
                    }

                } catch (\Exception $e) {
                    $productsResults['errors'][$sku['sku']][] = ['message' => $e->getMessage()];
                }
            }
        }

        return $productsResults;
    }

    /**
     * NEW FUNCTION FOR UPDATE PRICE TO API VERSION V4 TO STORE VIA VAREJO
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function integratePriceV4(string $httpMethod, array $data)
    {
        $priceTransformer = new PriceTransformer();
        $productsResults = [
            'success' => [],
            'errors' => []
        ];

        foreach ($data['products'] as $product) {

            foreach ($product['skus'] as $sku) {

                try {

                    /* if (!empty($data['request']['configurations']['price_decrease_percentage']) && $data['request']['configurations']['price_decrease_percentage'] > 0) {

                        $transformedPrices = $priceTransformer->transformDecreaseV4($sku);

                        if (empty($transformedPrices)) {

                            throw new \Exception('Not found to formatted');
                        }

                        $targetDePrice = round($transformedPrices['preco']['padrao'], 2);
                        $targetPorPrice = $transformedPrices['preco']['oferta'];

                        $currentCnova = $this->checkProductToPrice($data);

                        if (empty($currentCnova['default']) && empty($currentCnova['offer'])) {

                            $transformedPrices['preco']['padrao'] = $targetDePrice;
                            $transformedPrices['preco']['oferta'] = $targetPorPrice;

                        } else {

                            if ($currentCnova['default'] == $targetDePrice) {

                                $transformedPrices['preco']['padrao'] = $targetDePrice;
                                $transformedPrices['preco']['oferta'] = $targetPorPrice;

                            } else {

                                $currentCnova['default'] = round($currentCnova['default'] / 2, 2) + 1;

                                $transformedPrices['preco']['padrao'] = $currentCnova['default'];
                                $transformedPrices['preco']['oferta'] = $currentCnova['offer'];

                                if ($currentCnova['default'] < $targetDePrice) {

                                    $transformedPrices['preco']['padrao'] = $targetDePrice;
                                    $transformedPrices['preco']['oferta'] = $targetPorPrice;
                                }
                            }
                        }

                        $response = $this->sendPriceToStore($httpMethod, $data, $transformedPrices);
                        $productsResults['success'][$sku['sku']][] = (array)$response;

                    } else {

                        
                    } */

                    $transformedPrices = $priceTransformer->transformV4($sku);

                    if (empty($transformedPrices)) {

                        throw new \Exception('Not found to formatted');
                    }

                    $response = $this->sendPriceToStore($httpMethod, $data, $transformedPrices);
                    $productsResults['success'][$sku['sku']][] = (array)$response;

                } catch (\Exception $e) {

                    $productsResults['errors'][$sku['sku']][] = ['message' => $e->getMessage()];
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function checkProductToPrice(array $data)
    {
        $priceGetRequest = new Request('GET', $data['checkProductUrl']);

        $priceGetRequest->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        $client = new Client();

        $detailResponse = json_decode(json_encode($client->request($priceGetRequest)), true);

        $currentCnovaPrices = collect($detailResponse['prices'])
            ->where('site', 'EX')
            ->first();

        return $currentCnovaPrices;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     */
    public function integrateStockv4(string $httpMethod, array $data)
    {
        $productsResults = [
            'errors' => [],
        ];

        foreach ($data['products'] as $product) {

            foreach ($product['skus'] as $sku) {

                try {

                    $stockTransformer = new StockTransformer();
                    $transformedStocks = $stockTransformer->transformStockV4($sku);

                    if (empty($transformedStocks)) {

                        throw new \Exception('Not found to formatted');
                    }

                    $request = new Request($httpMethod, $data['request']['url']);

                    $request->setOptions([
                        'headers' => $data['request']['headers'],
                        'json' => $transformedStocks
                    ]);

                    $client = new Client();

                    $response = $client->request($request);
                    $productsResults['success'][$sku['sku']][] = (array)$response;

                } catch (\Exception $e) {

                    $productsResults['errors'][$sku['sku']][] = ['message' => $e->getMessage()];
                }
            }
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @param array $transformedPrices
     * @return array|mixed|\SimpleXMLElement
     * @throws \Exception
     */
    public function sendPriceToStore(string $httpMethod, array $data, array $transformedPrices)
    {
        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $transformedPrices,
        ]);

        $client = new Client();
        return $client->request($request);

    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function integrateStocks(string $httpMethod, array $data)
    {
        $productsResults = [
            'errors' => [],
        ];

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {

                try {
                    $stockTransformer = new StockTransformer();
                    $transformedStocks = $stockTransformer->transform($data, $product, $sku);

                    if (empty($transformedStocks)) {
                        throw new \Exception('Not found to formatted');
                    }

                    $url = $this->_makeUrl($sku['sku'], $data['request']['url']);

                    $request = new Request($httpMethod, $url);
                    $request->setOptions([
                        'headers' => $data['request']['headers'],
                        'json' => $transformedStocks,
                    ]);

                    $client = new Client();
                    $response = $client->request($request);
                    $productsResults['success'][$sku['sku']][] = (array)$response;

                } catch (\Exception $e) {
                    $productsResults['errors'][$sku['sku']][] = ['message' => $e->getMessage()];
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \League\Csv\Exception
     */
    public function sendOpt(string $httpMethod, array $data)
    {
        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $data['request']['data'],
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getStatus(string $httpMethod, array $data)
    {
        $cnovaValidator = new CnovaValidator($data);
        $cnovaValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        if (!empty($data['request']['params'])) {
            $request->addOption([
                'query' => $data['request']['params'],
            ]);
        }

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function getStocks(string $httpMethod, array $data)
    {
        $cnovaValidator = new CnovaValidator($data);
        $cnovaValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        if (!empty($data['request']['params'])) {
            $request->addOption([
                'query' => $data['request']['params'],
            ]);
        }

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     * @throws \Exception
     */
    public function getOrders(string $httpMethod, array $data)
    {
        $cnovaValidator = new CnovaValidator($data);
        $cnovaValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
            'query' => $data['request']['params'],
        ]);

        $client = new Client();
        $rawResponse = $client->request($request);

        $orders = [json_decode(json_encode($rawResponse), true)];

        $orderTransformer = new Order();
        $response = $orderTransformer->transformOrders($orders[0]['orders']);

        $response['total'] = isset($rawResponse->metadata[0]) ? $rawResponse->metadata[0]->value : 0;
        $response['offset'] = isset($rawResponse->metadata[1]) ? $rawResponse->metadata[1]->value : 0;
        $response['raw'] = $rawResponse;

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getOrder(string $httpMethod, array $data)
    {
        $cnovaValidator = new CnovaValidator($data);
        $cnovaValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->addOption([
            'headers' => $data['request']['headers'],
        ]);

        $client = new Client();
        $rawResponse = $client->request($request);
        $order = [json_decode(json_encode($rawResponse), true)];

        $orderTransf = new Order();
        $response = $orderTransf->transformOrders($order);
        $response['raw'] = $rawResponse;

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deleteProducts(string $httpMethod, array $data)
    {
        $cnovaValidator = new CnovaValidator($data);

        if ($cnovaValidator->validate()) {

            $productsResults = [
                'success' => [], 'errors' => [],
            ];

            $request = new Request($httpMethod);
            $client = new Client();

            foreach ($data['request']['data']['skus'] as $sku) {
                try {
                    $request->setOptions([
                        'headers' => $data['request']['headers'],
                    ]);

                    $request->setUrl($this->_makeUrl($sku, $data['request']['url']));
                    $response = $client->request($request);

                    $productsResults['success'][$sku][] = (array)$response;
                } catch (\Exception $exception) {
                    $productsResults['errors'][$sku][] = ['message' => $exception->getMessage()];
                }
            }

            return $productsResults;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws CnovaException
     */
    public function sendInvoice(string $httpMethod, array $data)
    {
        $cnovaValidator = new CnovaValidator($data);
        if ($cnovaValidator->validate()) {

            $request = new Request($httpMethod, $data['request']['url']);

            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $request->addOption([
                'json' => $data['request']['data'],
            ]);

            $client = new Client();
            $response = $client->request($request);

            if ($client->getStatusCode() == 204) {
                throw new CnovaException('Order not updated to delivered.', $client->getStatusCode());
            }

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws CnovaException
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function setOrderToShipped(string $httpMethod, array $data)
    {
        $cnovaValidator = new CnovaValidator($data);
        if ($cnovaValidator->validate()) {
            $request = new Request($httpMethod, $data['request']['url']);

            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $request->addOption([
                'json' => $data['request']['data'],
            ]);

            $client = new Client();
            $response = $client->request($request);

            if ($client->getStatusCode() == 204) {
                throw new CnovaException('Order not updated to shipped.', $client->getStatusCode());
            }

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws CnovaException
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function setOrderToDelivered(string $httpMethod, array $data)
    {
        $cnovaValidator = new CnovaValidator($data);
        if ($cnovaValidator->validate()) {
            $request = new Request($httpMethod, $data['request']['url']);

            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $request->addOption([
                'json' => $data['request']['data'],
            ]);

            $client = new Client();
            $response = $client->request($request);

            if ($client->getStatusCode() == 204) {
                throw new CnovaException('Order not updated to delivered.', $client->getStatusCode());
            }

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function getCategories(string $httpMethod, array $data)
    {
        $cnovaValidator = new CnovaValidator($data);
        $cnovaValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        $params = [];
        $params['_offset'] = 0;

        if (!empty($data['request']['params']['_offset'])) {

            $params['_offset'] = $data['request']['params']['_offset'];
        }

        if (!empty($data['request']['params']['_limit'])) {

            $params['_limit'] = $data['request']['params']['_limit'];
        }

        if (count($params)) {

            $request->addOption(['query' => $params]);
        }

        $client = new Client();
        $response = $client->request($request, 0, 0);

        $result['categories'] = [];

        $parsedResponse = [json_decode(json_encode($response), true)];

        foreach ($parsedResponse[0]['categories'] as $mainCategory) {

            $result['categories'] = array_merge($this->formatCategories($mainCategory), $result['categories']);
        }

        return $result;
    }

    /**
     * @param array $mainCategory
     * @return array
     * @throws \Exception
     */
    private function formatCategories(array $mainCategory)
    {
        $categories = [];

        if (!empty($mainCategory['categories'])) {

            foreach ($mainCategory['categories'] as $category) {

                $this->traverseCategoryTree($categories, $category, $mainCategory['name']);
            }
        }

        if (!empty($mainCategory['groups']) && empty($mainCategory['categories'])) {

            $formattedCategory = [];

            $formattedCategory['id'] = $mainCategory['id'];
            $formattedCategory['name'] = $mainCategory['name'];
            $formattedCategory['attributes'] = $this->traverseAttributes($mainCategory['groups']);

            $categories[] = $formattedCategory;
        }

        return $categories;
    }

    /**
     * @param array $categoriesTree
     * @param array $category
     * @param string $categoriesName
     */
    private function traverseCategoryTree(array &$categoriesTree, array $category, string $categoriesName)
    {
        $categoriesName = trim($categoriesName);
        $fullCategoryName = "{$categoriesName}|{$category['name']}";

        if (!empty($category['categories'])) {

            foreach ($category['categories'] as $childrenCategory) {

                $this->traverseCategoryTree($categoriesTree, $childrenCategory, $fullCategoryName);
            }
        }

        $formattedCategory = [];

        if (!empty($category['groups']) && count($category['categories']) == 0) {

            $formattedCategory['id'] = $category['id'];
            $formattedCategory['name'] = $fullCategoryName;
            $formattedCategory['attributes'] = $this->traverseAttributes($category['groups']);

            $categoriesTree[] = $formattedCategory;
        }
    }

    /**
     * @param array $groups
     * @return array
     */
    private function traverseAttributes(array $groups)
    {
        $formattedAttributes = [];

        foreach ($groups as $group) {
            if (!empty($group['attributes'])) {
                foreach ($group['attributes'] as $attribute) {
                    $formattedAttributes[$attribute['id']] = [
                        'id' => $attribute['id'],
                        'name' => $attribute['name'],
                        'required' => $attribute['required'],
                        'is_variant' => $attribute['variant']
                    ];

                    $formattedAttributes[$attribute['id']]['values'] = [];
                    if (!empty($attribute['values'])) {
                        foreach ($attribute['values'] as $value) {
                            $formattedAttributes[$attribute['id']]['values'][] = $value;
                        }
                    }
                }
            }
        }

        return $formattedAttributes;
    }

    /**
     * @param $replaceParam
     * @param $url
     * @return mixed
     */
    private function _makeUrl($replaceParam, $url)
    {
        return str_replace('%s', $replaceParam, $url);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getProducts(string $httpMethod, array $data)
    {
        $cNovaValidator = new CnovaValidator($data);
        if ($cNovaValidator->validate()) {

            $request = new Request($httpMethod, $this->_makeUrl(null, $data['request']['url']));

            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $params = [];
            $params['_offset'] = 0;
            if (!empty($data['request']['params']['_offset'])) {
                $params['_offset'] = $data['request']['params']['_offset'];
            }

            if (!empty($data['request']['params']['_limit'])) {
                $params['_limit'] = $data['request']['params']['_limit'];
            }

            if (count($params)) {
                $request->addOption(['query' => $params]);
            }

            $client = new Client();

            $rawResponse = $client->request($request);
            $products = json_decode(json_encode($rawResponse), true);

            $metadata = Arr::get($products, 'metadata', false);
            $sellerItems = Arr::get($products, 'sellerItems', false);

            if ($metadata === false || $sellerItems === false) {
                throw new \Exception(
                    sprintf('CNOVA response was a invalid format. Response [%s]', json_encode($rawResponse))
                );
            }

            $productsTransfer = new ProductTransformer($this);

            $response['products'] = $productsTransfer->transfProductsStore($sellerItems);
            $response['total'] = 0;

            $metadataFiltered = array_filter(
                $metadata,
                function ($item) {
                    return Arr::get($item, 'key', '') === 'totalRows';
                }
            );

            if (count($metadataFiltered) === 1) {
                $response['total'] = Arr::get($metadataFiltered, '0.value', 0);
            }

            return $response;
        }
    }

    public function getTicket(string $httpMethod, array $data)
    {
        return $this->getDefaultRequest($httpMethod, $data);
    }

    public function getTicketMessages(string $httpMethod, array $data)
    {
        return $this->getDefaultRequest($httpMethod, $data);
    }

    public function sendNewMessage(string $httpMethod, array $data)
    {
        return $this->getDefaultRequest($httpMethod, $data);
    }

    protected function getDefaultRequest(string $method, array $data)
    {
        $request = new Request($method, $data['request']['url']);

        if (!empty($data['request']['messageBody'])) {
            $request->addOption(['json' => $data['request']['messageBody']]);
        }

        if (!empty($data['request']['params'])) {
            $request->addOption(['query' => $data['request']['params']]);
        }

        if (!empty($data['request']['headers'])) {
            $request->addOption(['headers' => $data['request']['headers']]);
        }

        $client = new Client();

        $response = $client->request($request);
        $responseAsAssocArray = json_decode(json_encode($response), true);

        if (!$responseAsAssocArray) {
            return [];
        }

        return $responseAsAssocArray;
    }

    public function fetchTicket(string $httpMethod, array $data)
    {
        $requestData = $data['request'];
        $requestParams = $requestData['params'];

        $request = new Request($httpMethod, $requestData['url']);

        if (count($requestParams)) {
            $request->setOptions(['headers' => $data['request']['headers']]);
            $request->addOption(['query' => $requestParams]);
        }

        $client = new Client();

        $response = $client->request($request);
        $ticketData = json_decode(json_encode($response), true);

        if (!$ticketData) {
            return [];
        }

        $ticketData['pagination'] = [
            'total' => $this->findMetadata($ticketData['metadata'], 'totalRows'),
            'offset' => $this->findMetadata($ticketData['metadata'], 'offset'),
            'limit' => $this->findMetadata($ticketData['metadata'], 'limit'),
            'next' => $this->findMetadata($ticketData['metadata'], 'next'),
            'previous' => $this->findMetadata($ticketData['metadata'], 'previous'),
            'last' => $this->findMetadata($ticketData['metadata'], 'last'),
            'first' => $this->findMetadata($ticketData['metadata'], 'first'),
        ];

        return $ticketData;
    }

    private function findMetadata(array $metadata, string $field)
    {
        $metaDataItem = array_filter($metadata, function ($meta) use ($field) {
            return $meta['key'] === $field;
        });

        if (empty($metaDataItem)) {
            return null;
        }

        return array_first($metaDataItem)['value'];
    }

}
