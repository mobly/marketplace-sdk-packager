<?php

namespace Mobly\MarketplaceSdk\Integrators\Kev;

use Illuminate\Support\Arr;

class ProductTransformer
{
    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function transform(array $data)
    {
        foreach ($data['products'] as $product) {

            try {

                $category = '';
                if (!empty($product['store_category_external_id'])) {
                    $category = $product['store_category_external_id'];
                }
                $attributes = $values = [];
                foreach ($product['attributes'] as $key => $value) {
                    if (in_array($key, ['Cor', 'Tamanho'])) {
                        $attributes[] = ['pt' => $key];
                        $values[] = ['pt' => $value];
                    }
                }

                $images = [];
                foreach ($product['images'] as $key => $image) {
                    $images[] = [
                        'src' => $image,
                        'position' => ($key + 1),
                        'alt' => []
                    ];
                }

                $formattedProduct = [
                    'name' => [
                        'pt' => $product['name'],
                    ],
                    'description' => [
                        'pt' => $product['description'] . $this->concatenateAttributesTechnicalDescription($product['attributes']),
                    ],
                    'handle' => [
                        'pt' => $product['name']
                    ],
                    'attributes' => $attributes,
                    'brand' => $product['brand'],
                    'variants' => [
                        [
                            'position' => 1,
                            'price' => $product['skus'][0]['price'],
                            'promotional_price' => $product['skus'][0]['special_price'] ? $product['skus'][0]['special_price'] : $product['skus'][0]['price'],
                            'stock_management' => true,
                            'stock' => $product['skus'][0]['quantity'],
                            'weight' => $product['dimensions']['weight'], //,
                            'width' => $product['dimensions']['width'],
                            'height' => $product['dimensions']['height'],
                            'depth' => $product['dimensions']['length'],
                            'sku' => $product['skus'][0]['sku'],
                            'barcode' => $product['skus'][0]['ean'],
                            'values' => $values,
                        ]
                    ],
                    'images' => $images,
                    'categories' => [$category],
                    'published' => $product['status'] == 'active' ? true : false
                ];

                return $formattedProduct;
            } catch (\Exception $exception) {
                throw $exception;
            }
        }

    }

    /**
     * @param array $product
     * @return array
     */
    public function transformUpdate(array $product)
    {
        $category = '';
        if (!empty($product['store_category_external_id'])) {
            $category = $product['store_category_external_id'];
        }

        $formattedProduct = [
            'name' => [
                'pt' => $product['name'],
            ],
            'description' => [
                'pt' => $product['description'] . $this->concatenateAttributesTechnicalDescription($product['attributes']),
            ],
            'handle' => [
                'pt' => $product['name']
            ],
            'brand' => $product['brand'],
            'categories' => [$category],
            'published' => $product['status'] == 'active' ? true : false
        ];

        return $formattedProduct;

    }

    /**
     * @param array $sku
     * @return array
     */
    public function transformPrice(array $sku)
    {
        $formattedProduct = [
            'price' => $sku['price'],
            'promotional_price' => $sku['special_price'] ? $sku['special_price'] : $sku['price'],
        ];

        return $formattedProduct;
    }

    /**
     * @param array $sku
     * @return array
     */
    public function transformStock(array $sku)
    {
        $formattedProduct['stock'] = $sku['quantity'];
        return $formattedProduct;
    }

    /**
     * @param array $attributes
     * @return string|null
     */
    private function concatenateAttributesTechnicalDescription(array $attributes)
    {
        if (empty($attributes)) {
            return null;
        }

        $data = '';

        foreach ($attributes as $key => $value) {
            $data .= "<tr><td style='width:150px'><b>{$key}</b></td><td>{$value}</td></tr>";
        }

        $html = "<table>{$data}</table>";
        return $html;
    }
}
