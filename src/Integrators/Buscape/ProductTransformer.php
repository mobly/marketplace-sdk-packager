<?php

namespace Mobly\MarketplaceSdk\Integrators\Buscape;

use Mobly\MarketplaceSdk\Helpers\Helper;

class ProductTransformer
{
    const MOBLY_URL = 'https://www.mobly.com.br/';

    /**
     * @param array $data
     * @return array
     */
    public static function transform(array $data)
    {
        $formattedProducts = [];
        $skusWithError = [];

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {
                try {
                    $formattedProduct = [
                        'sku' => $sku['sku'],
                        'title' => Helper::_concatTitle($product['name'], $sku['superAttribute']),
                        'category' => self::transformCategory($product['path_category']),
                        'description' => strip_tags($product['description']),
                        'images' => $product['images'],
                        'link' => self::MOBLY_URL . $product['external_id'] . '.html',
                        'prices' => self::getBuscapePrices($sku),
                        'productAttributes' => $product['attributes'],
                        'technicalSpecification' => [
                            'brand' => $product['brand'],
                        ],
                        'quantity' => $sku['quantity'],
                        'weightValue' => Helper::convertKilogramToGram($product['dimensions']['weight']),
                        'sizeLength' => (int)$product['dimensions']['length'],
                        'sizeWidth' => (int)$product['dimensions']['width'],
                        'sizeHeight' => (int)$product['dimensions']['height'],
                        'handlingTimeDays' => $product['chain_time'] ?? 1,
                        'marketplace' => true,
                        'marketplaceName' => 'Mobly Connectopus',
                    ];

                    $formattedProducts[] = $formattedProduct;
                } catch (\Exception $exception) {
                    $skusWithError[$sku['sku']][] = ['message' => $exception->getMessage()];
                }
            }
        }

        return [
            'formattedProducts' => $formattedProducts,
            'skusWithError' => $skusWithError,
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    public static function transformForUpdate(array $data)
    {
        $formattedProducts = [];
        $skusWithError = [];

        $product = array_pop($data['products']);
        foreach ($product['skus'] as $sku) {
            try {
                $formattedProduct = [
                    'sku' => $sku['sku'],
                    'prices' => self::getBuscapePrices($sku),
                    'quantity' => $sku['quantity'],
                ];

                $formattedProducts[] = $formattedProduct;
            } catch (\Exception $exception) {
                $skusWithError[$sku['sku']][] = ['message' => $exception->getMessage()];
            }
        }

        return [
            'formattedProducts' => $formattedProducts,
            'skusWithError' => $skusWithError,
        ];
    }

    public static function transformForBatchUpdate(array $data)
    {
        $formattedProducts = [];
        $skusWithError = [];

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {
                try {
                    $formattedProduct = [
                        'sku' => $sku['sku'],
                        'prices' => self::getBuscapePrices($sku),
                        'quantity' => $sku['quantity'],
                    ];

                    $formattedProducts[] = $formattedProduct;
                } catch (\Exception $exception) {
                    $skusWithError[$sku['sku']][] = ['message' => $exception->getMessage()];
                }
            }
        }

        return [
            'formattedProducts' => $formattedProducts,
            'skusWithError' => $skusWithError,
        ];
    }


    /**
     * @param array $sku
     * @return array
     */
    private static function getBuscapePrices(array $sku)
    {
        $price = !empty($sku['special_price']) ? $sku['special_price'] : $sku['price'];

        $buscapePrices[] = [
            'type' => 'cartao_avista',
            'price' => $price,
            'installment' => '1',
            'installmentValue' => $price,
        ];

        $buscapePrices[] = [
            'type' => 'cartao_parcelado_sem_juros',
            'price' => $price,
            'installment' => '1',
            'installmentValue' => $price,
        ];

        return $buscapePrices;
    }

    /**
     * @param string $categoryPath
     * @return mixed
     */
    private static function transformCategory(string $categoryPath)
    {
        $categoryPath = ltrim($categoryPath, '##');
        $categoryPath = rtrim($categoryPath, '##');

        return str_replace('##', '>', $categoryPath);
    }
}