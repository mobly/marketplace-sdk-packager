<?php

namespace Mobly\MarketplaceSdk\Integrators\Buscape;

use Symfony\Component\HttpFoundation\Response;

class OrderTransformer
{
    /**
     * @param array $orders
     * @return mixed
     * @throws \Exception
     */
    public static function transform(array $orders)
    {
        $result['orders'] = [];

        foreach ($orders as $order) {
            try {
                $totalAmount = 0;

                foreach ($order->paymentMethods as $paymentMethod) {
                    $totalAmount += $paymentMethod->amount;
                }

                $orderTransform = [
                    'store_order_number' => $order->orderID,
                    'store_last_updated_at' => $order->lastUpdatedAt,
                    'store_order_status' => $order->orderStatus,

                    'customer_first_name' => $order->clientProfileData->firstName,
                    'customer_last_name' => $order->clientProfileData->lastName,
                    'customer_document' => preg_replace('/[^0-9x]/i', '',
                        $order->clientProfileData->document),
                    'customer_email' => $order->clientProfileData->email,
                    'customer_phone' => preg_replace('/[^0-9]/', '', $order->clientProfileData->phone),
                    'customer_gender' => null,

                    'billing_postcode' => preg_replace('/[^0-9]/', '',
                        $order->billingInfo[0]->address->postalCode),
                    'billing_street' => $order->billingInfo[0]->address->street,
                    'billing_street_number' => $order->billingInfo[0]->address->number,
                    'billing_neighborhood' => $order->billingInfo[0]->address->neighborhood,
                    'billing_complement' => $order->billingInfo[0]->address->complement,
                    'billing_city' => $order->billingInfo[0]->address->city,
                    'billing_state' => $order->billingInfo[0]->address->state,
                    'billing_country' => $order->billingInfo[0]->address->country,

                    'shipping_postcode' => preg_replace('/[^0-9]/', '',
                        $order->shippingInfo[0]->address->postalCode),
                    'shipping_street' => $order->shippingInfo[0]->address->street,
                    'shipping_street_number' => $order->shippingInfo[0]->address->number,
                    'shipping_neighborhood' => $order->shippingInfo[0]->address->neighborhood,
                    'shipping_complement' => $order->shippingInfo[0]->address->complement,
                    'shipping_city' => $order->shippingInfo[0]->address->city,
                    'shipping_state' => $order->shippingInfo[0]->address->state,
                    'shipping_country' => $order->shippingInfo[0]->address->country,
                    'shipping_price' => $order->totalFreight,

                    'total_price' => $totalAmount,
                    'items' => [],
                ];

                $itemFreightPrice = $order->totalFreight / count($order->orderedItems);

                foreach ($order->orderedItems as $item) {
                    $orderTransform['items'][] = [
                        'store_item_id' => $item->sku,
                        'sku' => $item->skuSellerId,
                        'sku_name' => '',
                        'unit_price' => $item->price,
                        'shipping_price' => $itemFreightPrice,
                        'quantity' => $item->quantity,
                        'total_commission' => 0,
                    ];
                }

                $result['orders'][] = $orderTransform;
            } catch (\Exception $ex) {
                //todo treat
//                throw new \Exception($ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return $result;
    }
}