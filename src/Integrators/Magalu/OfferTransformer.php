<?php

namespace Mobly\MarketplaceSdk\Integrators\Magalu;

use Illuminate\Support\Arr;
use Mobly\MarketplaceSdk\Helpers\Helper;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class OfferTransformer
{
    /**
     * @var IntegratorAbstract
     */
    private $integratorAbstract;

    public function __construct(IntegratorAbstract $integratorAbstract)
    {
        $this->integratorAbstract = $integratorAbstract;
    }

    /**
     * @param array $data
     * @return array
     */
    public function transform(array $data, $create = false)
    {
        foreach ($data['offers'] as $offer) {
            foreach ($offer['skus'] as $offerSku) {
                try {
                    $productName = Helper::_concatTitle($offer['name'], $offerSku['superAttribute']);

                    $primaryImage = $offer['images'][0];
                    array_shift($offer['images']);
                    $secondaryImages = $offer['images'];

//                    $primaryImage = str_replace('http://', 'https://', $primaryImage);
//                    foreach($secondaryImages as $index=>$image) {
//                        $secondaryImages[$index] = str_replace('http://', 'https://', $image);
//                    }

                    $formattedOffer = [
                        'IdSku' => $offerSku['sku'], //$offerSku['sku'],
                        'IdProduct' => !empty($offerSku['store_sku'][0]) ? $offerSku['store_sku'][0] : $offerSku['sku'],
                        'IdSkuErp' => $offerSku['sku'], //
                        'Name' => $productName,
                        'Description' => $offer['description'],
                        'Height' => (string) Helper::convertCentimeterToMeter($offer['dimensions']['height']),
                        'Width' => (string) Helper::convertCentimeterToMeter($offer['dimensions']['width']),
                        'Length' => (string) Helper::convertCentimeterToMeter($offer['dimensions']['length']),
                        'Weight' => (string) Helper::convertDotToComma($offer['dimensions']['weight']),
                        'Weight' => $offer['dimensions']['weight'],
                        'CodeEan' => $offerSku['ean'],
                        'CodeNcm' => '',
                        'CodeIsbn' => '',
                        'CodeNbm' => '',
                        'Variation' => '',
                        'Status' => $offerSku['status'] == "active" ? true : false,
                        'Price' => [
                            'ListPrice' => $offerSku['price'],
                            'SalePrice' => $offerSku['special_price'] ?? $offerSku['price'],
                        ],
                        'StockQuantity' => $offerSku['quantity'],
                        'MainImageUrl' => $primaryImage,
                        'UrlImages' => $secondaryImages,
                        'Attributes' => [],
                    ];

                    if ($create != false) {
                        $formattedOffer['IdSkuErp'] = $offerSku['sku'];
                    }

                    return (object) $formattedOffer;
                } catch (\Exception $exception) {
                    $this->integratorAbstract->addErrorsWithKey($exception->getMessage(), $offerSku['sku']);
                }
            }
        }
    }
}