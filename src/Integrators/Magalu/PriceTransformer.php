<?php

namespace Mobly\MarketplaceSdk\Integrators\Magalu;

use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class PriceTransformer
{

    /**
     * @var IntegratorAbstract
     */
    private $integratorAbstract;

    public function __construct(IntegratorAbstract $integratorAbstract)
    {
        $this->integratorAbstract = $integratorAbstract;
    }

    /**
     * @param array $data
     * @return array
     */
    public function transform(array $data)
    {
        $formattedProducts = [];

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {
                try {
                    $formattedProduct = [
                        'IdSku' => $sku['sku'],
                        'ListPrice' => $sku['price'],
                        'SalePrice' => $sku['special_price'] ?? $sku['price'],
                    ];

                    $formattedProducts[] = $formattedProduct;
                } catch (\Exception $exception) {
                    $this->integratorAbstract->addErrorsWithKey($exception->getMessage(), $sku['sku']);
                }
            }
        }

        return $formattedProducts;
    }
}