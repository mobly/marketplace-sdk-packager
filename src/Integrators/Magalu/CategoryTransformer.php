<?php

namespace Mobly\MarketplaceSdk\Integrators\Magalu;

class CategoryTransformer
{
    /**
     * @param array $data
     * @return array
     */
    public function transform(array $data)
    {
        $formattedCategories = [];

        foreach ($data['categories'] as $category) {
            $nameParts = explode('##', $category['path']);
            array_shift($nameParts);
            array_pop($nameParts);
            $formattedCategory = [
                'Id' => (string)$category['external_id'],
                'Name' => end($nameParts),
                'ParentId' => $category['parent_id'] ?? ''
            ];
            $formattedCategories[] = (object)$formattedCategory;
        }
        return $formattedCategories;
    }
}