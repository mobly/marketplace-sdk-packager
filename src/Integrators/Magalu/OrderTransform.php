<?php

namespace Mobly\MarketplaceSdk\Integrators\Magalu;


use Illuminate\Http\Response;

class OrderTransform
{
    /**
     * @param array $orders
     * @return mixed
     * @throws \Exception
     */
    public function transformOrders(array $orders)
    {
        $result['orders'] = [];

        try {
            foreach ($orders as $order) {
                list($firstName, $lastName) =
                    !empty($order['CustomerPfName']) ?
                        $this->formatName($order['CustomerPfName']) :
                        $this->formatName($order['CustomerPjCorporatename']);

                $orderTransform = [
                    'store_order_number' => $order['IdOrder'],
                    'store_last_updated_at' => $order['UpdatedDate'] ?? date('Y-m-d H:i:s'),
                    'store_order_status' => $order['OrderStatus'],
                    'customer_first_name' => $firstName,
                    'customer_last_name' => $lastName,
                    'customer_document' => !empty($order['CustomerPfCpf']) ?
                        $order['CustomerPfCpf'] :
                        $order['CustomerPjCnpj'],
                    'customer_email' => $order['CustomerMail'],
                    'customer_phone' => $order['TelephoneMainNumber'],
                    'customer_phone2' => $order['TelephoneSecundaryNumber'],
                    'customer_gender'  => null,
                    'billing_postcode' => $order['DeliveryAddressZipcode'],
                    'billing_street' => $order['DeliveryAddressStreet'],
                    'billing_street_number' => $order['DeliveryAddressNumber'],
                    'billing_neighborhood' => $order['DeliveryAddressNeighborhood'],
                    'billing_complement' => !empty($order['DeliveryAddressAdditionalInfo']) ? $order['DeliveryAddressAdditionalInfo'] : '',
                    'billing_city' => $order['DeliveryAddressCity'],
                    'billing_state' => $order['DeliveryAddressState'],
                    'billing_country' => 'BR',
                    'shipping_postcode' => $order['DeliveryAddressZipcode'],
                    'shipping_street' => $order['DeliveryAddressStreet'],
                    'shipping_street_number' => $order['DeliveryAddressNumber'],
                    'shipping_neighborhood' => $order['DeliveryAddressNeighborhood'],
                    'shipping_complement' => !empty($order['DeliveryAddressAdditionalInfo']) ? $order['DeliveryAddressAdditionalInfo'] : '',
                    'shipping_city' => $order['DeliveryAddressCity'],
                    'shipping_state' => $order['DeliveryAddressState'],
                    'shipping_country' => 'BR',
                    'shipping_price' => str_replace(',', '.', $order['TotalFreight']),
                    'total_price' => str_replace(',', '.', $order['TotalAmount']),
                    'items' => [],
                ];

                $items_qty = 0;

                foreach ($order['Products'] as $item) {
                    $item = (array)$item;
                    for($i=1; $i<=$item['Quantity']; $i++) {
                        $price = $this->formatPrice($item);

                        $orderTransform['items'][] = [
                            'store_item_id' => "{$item['IdSku']}_{$i}",
                            'sku' => $item['IdSku'],
                            'sku_name' => '',
                            'unit_price' => ( $price - ($item['Discount'] / $item['Quantity'])),
                            'shipping_price' => 0,
                            'quantity' => 1,
                            'total_commission' => 0,
                        ];

                        //$orderTransform['total_price'] += ($this->formatPrice($item) * $item['Quantity']) - $item['Discount'];
                    }
                    $items_qty += $item['Quantity'];
                }

                /**
                 * CREATE JSON AND SAVE ON COLUMN "EXTRA"
                 * IF IS NOT NULL
                 */
                $extra = array();

                if (!empty($order['Payments'][0])) {
                    foreach ($order['Payments'][0] as $key => $value) {
                        $tmp = [
                            'payment_' . strtolower($key) => $value
                        ];

                        if ('amount' === strtolower($key)) {
                            $tmp = [
                                'payment_' . strtolower($key) => (string) round((float) $value, 2)
                            ];
                        }

                        $extra = array_merge($extra, $tmp);
                    }
                }

                if(isset($order['TotalTax'])) {
                    $totalTax = str_replace(',', '.', $order['TotalTax']);
                    $extra['StoreTotalTax'] = $totalTax;
//                    $orderTransform['total_price'] += $totalTax;
                }

                if (isset($order['PurchasedDate'])) {
                    $extra['StorePurchasedDate'] = $order['PurchasedDate'];
                }

                if (isset($order['ApprovedDate']) && !empty($order['ApprovedDate'])) {
                    $extra['StoreApprovedDate'] = isset($order['ApprovedDate']) ? $order['ApprovedDate'] : '';
                }

                if (isset($order['EstimatedDeliveryDate']) ) {
                    $extra['StoreEstimatedDeliveryDate'] = $order['EstimatedDeliveryDate'];
                }

                $extra['storeDiscount'] = $this->formatTotalDiscount($order);

                $orderTransform['extra'] = json_encode($extra);
                /**
                 * END
                 */

                $result['orders'][] = $orderTransform;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);

        }

        return $result;
    }
    /**
     * @param string $name
     * @return array
     */
    private function formatName(string $name)
    {
        $nameExploded = explode(' ', ucwords(strtolower(trim($name))));

        return array($nameExploded[0], end($nameExploded));
    }

    /**
     * @param $item
     * @return mixed
     * @throws \Exception
     */
    private function formatTotalDiscount($item)
    {
        if (empty($item['TotalDiscount'])) {
            return 0;
        }

        return str_replace(',', '.', $item['TotalDiscount']);
    }

    /**
     * @param $item
     * @return mixed
     * @throws \Exception
     */
    private function formatPrice($item)
    {
        if (empty($item['Price'])) {
            return 0;
            //throw new \Exception('Price not found.');
        }

        return str_replace(',', '.', $item['Price']);
    }
}
