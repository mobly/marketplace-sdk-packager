<?php

namespace Mobly\MarketplaceSdk\Integrators\Magalu;

use Illuminate\Support\Arr;
use Mobly\MarketplaceSdk\Helpers\Helper;

class ProductTransformer
{
    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function transform(array $data)
    {
        foreach ($data['products'] as $product) {

            try {
                $categories = [(object)['Id' => (string)$product['external_id']]];

                $warrantyTime = '0';
                $attributes = [];
                foreach ($product['attributes'] as $key => $value) {
                    if ($key == 'Garantia') $warrantyTime = explode(' ', $value)[0];
                    $attributes[] = (object)['Name' => $key, 'Value' => $value];
                }


                if (Arr::get($product, 'skus.0.store_sku.0', false)) {
                    $sku = Arr::get($product, 'skus.0.store_sku.0');
                    $idSimples = explode('-', $sku)[1] ?? $sku;
                } else {
                    $sku = Arr::get($product, 'skus.0.sku');
                    $idSimples = explode('-', $sku)[1] ?? $sku;
                }


                $formattedProduct = [
                    'IdProduct' => $product['skus'][0]['sku'], //
                    'Code' => $idSimples,
                    'Name'=> $product['name'], //
                    'Brand' => $product['brand'],
                    'NbmOrigin' => 0,
                    'WarrantyTime' => $warrantyTime,
                    'Categories' => $categories,
                    'Attributes' => $attributes,
                    'Active' => $product['status'] == 'active' ? 1 : 0
                ];

                return $formattedProduct;
            } catch (\Exception $exception) {
                throw $exception;
            }
        }

    }
}
