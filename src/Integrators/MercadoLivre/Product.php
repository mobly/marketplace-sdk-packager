<?php
/**
 * Created by PhpStorm.
 * User: hermes
 * Date: 18/10/18
 * Time: 15:13
 */

namespace Mobly\MarketplaceSdk\Integrators\MercadoLivre;


use Mobly\MarketplaceSdk\Helpers\Helper;


class Product
{
    public $measures = ['WIDTH', 'HEIGHT', 'LENGTH', 'DEPTH', 'TOTAL_HEIGHT'];
    /**
     * @param array $data
     * @return array
     */
    public function formatProducts(array $data)
    {
        $formattedProducts = array();
        $skusWithErrors = array();

        $products = $data['products'];

        foreach ($products as $product) {
            foreach ($product['skus'] as $sku) {
                try {
                    foreach ($sku['listing_types'] as $listing_type) {
                        $formattedProduct = [
                            'status' => 'active',
                            'title' => $this->_cleanString(Helper::_concatTitle($product['name'], $sku['superAttribute']), 60, 57),
                            'official_store_id' => $data['request']['params']['official_store_id'],
                            'category_id' => $product['store_category_external_id'],
                            'condition' => 'new',
                            'currency_id' => 'BRL',
                            'listing_type_id' => $listing_type,
                            'description' => [
                                'plain_text' => Helper::getPlainText($product['description']),
                            ],
                            'pictures' => $this->_makeImages($product['images']),
                            'shipping' => [
                                'mode' => $sku['shipping_mode'],
                                'local_pick_up' => false,
                                'dimensions' => $this->_makeDimensions($product['dimensions'])
                            ],
                            'seller_custom_field' => $sku['sku']
                        ];

                        if(!empty($product['attributes']['Cor'])) {
                            $formattedProduct['variations'] = [[
                                'attribute_combinations' => [
                                    [
                                        'id' => 'COLOR',
                                        'name' => 'Cor',
                                        'value_name' => $product['attributes']['Cor'],
                                    ]
                                ],
                                'available_quantity' => $sku['quantity'],
                                'price' => isset($sku['special_price']) && $sku['special_price'] > 0 ? $sku['special_price'] : $sku['price'],
                                'picture_ids' => array_slice($product['images'], 0, 10),
                                'attributes' => [
                                    [
                                        'id' => 'SELLER_SKU',
                                        'value_name' => $sku['sku']
                                    ],
                                    [
                                        'id' => 'EAN',
                                        'value_name' => $sku['ean']
                                    ]
                                ],
                            ]];
                        } else {
                            $formattedProduct['available_quantity'] = $sku['quantity'];
                            $formattedProduct['price'] = isset($sku['special_price']) && $sku['special_price'] > 0 ? $sku['special_price'] : $sku['price'];
                            if(!empty($product['ean'])) {
                                $formattedProduct['attributes'][] = [
                                    'id' => 'EAN',
                                    'value_name' => $sku['ean']
                                ];
                            }
                        }

                        foreach ($product['store_attributes'] as $store_attribute) {
                            if ($store_attribute['external_value_id'] != null
                                && $store_attribute['external_id'] != null
                            ) {
                                if ($store_attribute['external_id'] === 'COLOR') {
                                    continue;
                                }

                                if(in_array($store_attribute['external_id'], $this->measures)) {
                                    $formattedProduct['attributes'][] = [
                                        'id' => $store_attribute['external_id'] ? $store_attribute['external_id'] : -1,
                                        'value_name' => $store_attribute['external_value_id'] ? "{$store_attribute['external_value_id']} cm" : 'Null',
                                    ];
                                } else {

                                    $att = [
                                        'id' => $store_attribute['external_id']  ? $store_attribute['external_id'] : -1,
                                        'value_id' => $store_attribute['external_value_id'] ? $store_attribute['external_value_id'] : 'Null',
                                    ];

                                    if($store_attribute['external_value_id'] == $store_attribute['value']) {
                                        $att = [
                                            'id' => $store_attribute['external_id']  ? $store_attribute['external_id'] : -1,
                                            'value_name' => $store_attribute['value'] ? $store_attribute['value'] : 'Null',
                                        ];

                                    }
                                    $formattedProduct['attributes'][] = $att;
                                }
                            }
                        }

                        if(!empty($product['brand'])) {
                            $formattedProduct['attributes'][] = [
                                'id' => 'BRAND',
                                'value_id' => '1108895',
                                'value_name' => $product['brand'],
                            ];
                        }

//                        if(!empty($product['supplier_delivery_time'])) {
//                            $formattedProduct['sale_terms'][] = [
//                                'id' => 'MANUFACTURING_TIME',
//                                'value_name' => null
//                            ];
//                        }

                        $formattedProducts[$sku['sku']][] = $formattedProduct;
                    }

                } catch (\Exception $exception) {
                    $skusWithErrors[$sku['sku']] = ['message' => $exception->getMessage()];
                }
            }
        }

        return array($formattedProducts, $skusWithErrors);
    }

    /**
     * @param $product
     * @param int $soldQuantity
     * @return array
     */
    public function formatProductUpdateSimple($product, $soldQuantity = false)
    {
        $data = [
            'pictures' => $this->_makeImages($product['images']),
            'seller_custom_field' => null,
        ];

        if(!$soldQuantity) {
            $data['title'] = $this->_cleanString($product['name'], 60, 57);
        }

        return $data;
    }

    /**
     * @param array $products
     * @return array
     */
    public function formatProductsToUpdateDescription(array $products)
    {
        $formattedProducts = array();
        $skusWithErrors = array();

        foreach ($products as $product) {
            foreach ($product['skus'] as $sku) {
                if (!empty($sku['store_sku']) && !empty($sku['sku'])) {
                    try {
                        $formattedProduct = [
                            'plain_text' => Helper::getPlainText($product['description']),
                            'store_sku' => $sku['store_sku']
                        ];

                        $formattedProducts[$sku['sku']][] = $formattedProduct;
                    } catch (\Exception $exception) {
                        $skusWithErrors[$sku['sku']] = ['message' => $exception->getMessage()];
                    }
                }
            }
        }

        return array($formattedProducts, $skusWithErrors);
    }

    /**
     * @param array $images
     * @return array
     */
    private function _makeImages(array $images)
    {
        $images = array_map(function ($image) {
            return array('source' => $image);
        }, $images);

        $images = array_slice($images, 0, 11);

        return $images;
    }

    /**
     * @param $dimensions
     * @return string
     */
    private function _makeDimensions($dimensions)
    {
        return "{$dimensions['length']}x{$dimensions['width']}x{$dimensions['height']},{$dimensions['weight']}";
    }

    /**
     * @param string $string
     * @param $max
     * @param $limit
     * @return bool|string
     */
    private function _cleanString(string $string, $max, $limit)
    {
        // remove html tags
        $string = strip_tags($string);

        // 2000 characters max
        if (strlen($string) > $max) {
            $string = mb_substr($string, 0, $limit, 'UTF-8');
            $string .= '...';
        }

        return $string;
    }

    /**
     * @param $data
     * @return array
     */
    public function transfProductsStore($data)
    {
        $formattedProducts = [];
        foreach ($data[0]['results'] as $product) {
            $formattedProducts[] = [
                'sku' => $product
            ];
        }

        return $formattedProducts;
    }

    /**
     * @param $data
     * @return array
     */
    public function transfProductStore($product)
    {
        $formattedProduct = [
            'sku' => $product['id'],
            'attributes' => $product['attributes'],
            'variations' => $product['variations'],
            'listing_type' => $product['listing_type_id'],
            'sold_quantity' => $product['sold_quantity'],
            'price' => $product['original_price'],
            'promotional_price' => $product['price'],
            'stock' => $product['available_quantity'],
            'status' => $product['status'],
            'seller_custom_field' => $product['seller_custom_field'],
        ];
        return $formattedProduct;
    }

    /**
     * @param $product
     * @return array
     */
    public function formatProductsToUpdateExpedition($product)
    {
        $data = array();

        if(!empty($product['supplier_delivery_time'])) {
            $data['sale_terms'][] = [
                'id' => 'MANUFACTURING_TIME',
                'value_name' => null
            ];
        }

        return $data;
    }

    /**
     * @param $product
     * @param $sku
     * @param bool $hasVariation
     * @return array
     */
    public function formatProductsToUpdateAttributes($product, $sku, $hasVariation = false)
    {
        $data = array();

        if(!empty($product['brand'])) {
            $data['attributes'][] = [
                'id' => 'BRAND',
                'value_name' => $product['brand'],
            ];
        }

        if(!empty($product['ean'])) {
            $data['attributes'][] = [
                'id' => 'EAN',
                'value_name' => $sku['ean']
            ];
        }

        if($hasVariation) {
            $data['variations'] = [[
                'attribute_combinations' => [
                    [
                        'id' => 'COLOR',
                        'name' => 'Cor',
                        'value_name' => $product['attributes']['Cor'],
                    ]
                ],
                'available_quantity' => $sku['quantity'],
                'price' => isset($sku['special_price']) && $sku['special_price'] > 0 ? $sku['special_price'] : $sku['price'],
                'picture_ids' => array_slice($product['images'], 0, 10),
                'attributes' => [
                    [
                        'id' => 'SELLER_SKU',
                        'value_name' => $sku['sku']
                    ],
                    [
                        'id' => 'EAN',
                        'value_name' => $sku['ean']
                    ]
                ],
            ]];
        }

        if (!empty($product['store_attributes'])) {
            foreach ($product['store_attributes'] as $storeAttribute) {
                try {
                    if ($storeAttribute['external_id'] === 'COLOR') {
                        continue;
                    }

                    if(in_array($storeAttribute['external_id'], $this->measures)) {
                        $data['attributes'][] = [
                            'id' => $storeAttribute['external_id'] ? $storeAttribute['external_id'] : -1,
                            'value_name' => $storeAttribute['external_value_id'] ? "{$storeAttribute['external_value_id']} cm" : 'Null',
                        ];
                    } else {
                        $data['attributes'][] = [
                            'id' => $storeAttribute['external_id'] ? $storeAttribute['external_id'] : -1,
                            'value_id' => $storeAttribute['external_value_id'] ? $storeAttribute['external_value_id'] : 'Null',
                        ];
                    }
                } catch (\Throwable $e) {
                    continue;
                }
            }
        }

        return $data;
    }
}
