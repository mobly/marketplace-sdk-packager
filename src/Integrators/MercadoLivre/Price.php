<?php
/**
 * Created by PhpStorm.
 * User: hermes
 * Date: 17/06/19
 * Time: 17:20
 */

namespace Mobly\MarketplaceSdk\Integrators\MercadoLivre;


class Price
{
    /**
     * @param $sku
     * @param $idVariation
     * @return array
     */
    public function format($sku, $idVariation)
    {
        if($idVariation) {
            $data['variations'] = [[
                'id' => $idVariation,
                'price' => isset($sku['special_price']) && $sku['special_price'] > 0
                    ? $sku['special_price']
                    : $sku['price'],
            ]];
        } else {
            $data['price'] = isset($sku['special_price']) && $sku['special_price'] > 0
                    ? $sku['special_price']
                    : $sku['price'];
        }

        return $data;
    }
}