<?php
/**
 * Created by PhpStorm.
 * User: hermes
 * Date: 15/10/18
 * Time: 18:50
 */

namespace Mobly\MarketplaceSdk\Integrators\MercadoLivre;


use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;

class Auth
{
    const URLS = [
        'auth' => 'https://auth.mercadolivre.com.br',
        'api' => 'https://api.mercadolibre.com'
    ];

    const ENDPOINTS = [
        'authorization' => 'authorization',
        'token' => 'oauth/token'
    ];

    private $clientId;
    private $clientSecret;
    private $refreshToken;

    /**
     * Auth constructor.
     * @param $clientId
     * @param $clientSecret
     * @param $refreshToken
     */
    public function __construct($clientId, $clientSecret, $refreshToken = null)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return mixed
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @return mixed
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param null $refreshToken
     */
    public function setRefreshToken($refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return array|mixed
     */
    public function getAccessToken()
    {
        try {
            $url = self::URLS['api'] . '/' . self::ENDPOINTS['token'];

            $request = new Request('POST', $url);

            $request->addOption(array(
                'query' => array(
                    'grant_type' => 'client_credentials',
                    'client_id' => $this->getClientId(),
                    'client_secret' => $this->getClientSecret()
                )
            ));

            $client = new Client();

            $response = $client->request($request);

        } catch (\Exception $exception) {
            return ['message' => $exception->getMessage()];
        }

        return $response;
    }

    /**
     * @return array|mixed
     * @throws \Exception
     */
    public function refreshAccessToken()
    {
        if (is_null($this->getRefreshToken())) {
            throw new \Exception('Refresh token has not found.');
        }

        try{
            $url = self::URLS['api'] . '/' . self::ENDPOINTS['token'];

            $request = new Request('POST', $url);

            $request->addOption([
                'query' => [
                    'grant_type' => 'refresh_token',
                    'client_id' => $this->getClientId(),
                    'client_secret' => $this->getClientSecret(),
                    'refresh_token' => $this->getRefreshToken()
                ]
            ]);

            $client = new Client();

            $response = $client->request($request);

        } catch (\Exception $exception) {
            return ['message' => $exception->getMessage()];
        }

        return $response;
    }
}