<?php
/**
 * Created by PhpStorm.
 * User: hermes
 * Date: 17/06/19
 * Time: 17:20
 */

namespace Mobly\MarketplaceSdk\Integrators\MercadoLivre;


class Stock
{
    /**
     * @param $sku
     * @param $idVariation
     * @return array
     */
    public function format($sku, $idVariation)
    {
        if($idVariation) {
            $data['variations'] = [[
                'id' => $idVariation,
                'available_quantity' => $sku['quantity'],
            ]];
        } else {
            $data['available_quantity'] = $sku['quantity'];
        }

        return $data;
    }
}