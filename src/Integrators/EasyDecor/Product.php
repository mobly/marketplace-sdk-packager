<?php
/**
 * Created by PhpStorm.
 * User: hermes
 * Date: 07/05/19
 * Time: 17:51
 */

namespace Mobly\MarketplaceSdk\Integrators\EasyDecor;

use Mobly\MarketplaceSdk\Helpers\Helper;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;
use Mobly\MarketplaceSdk\Validators\EasyDecor\ProductValidator;
use Illuminate\Validation\ValidationException;


class Product
{
    const MOBLY_URL = 'https://www.mobly.com.br/';

    private $integratorAbstract;

    /**
     * Product constructor.
     * @param IntegratorAbstract $integratorAbstract
     * @param ProductValidator $validator
     */
    public function __construct(IntegratorAbstract $integratorAbstract, ProductValidator $validator)
    {
        $this->integratorAbstract = $integratorAbstract;
        $this->validator = $validator;
    }

    /**
     * Map - Mobly Manager statuses to SDK
     */
    const STATUSES_MAP = [
        'active'   => 'enabled',
        'inactive' => 'disabled'
    ];

    /**
     * @param array $data
     * @return array
     */
    public function formatProducts(array $data)
    {
        $formattedProducts = array();

        $products = $data['products'];
        foreach ($products as $product) {
            if ($this->isValidProduct($product)) {
                foreach ($product['skus'] as $sku) {

                    $productName = Helper::_concatTitle($product['name'], $sku['superAttribute']);

                    $formattedProduct = [
                        'sku' => $sku['sku'],
                        'name' => $productName,
                        'description' => strip_tags($product['description']),
                        'status' => key_exists($sku['status'], self::STATUSES_MAP) ?
                            self::STATUSES_MAP[$sku['status']] :
                            'enabled',
                        'qty' => $sku['quantity'],
                        'price' => $sku['price'],
                        'promotional_price' => empty($sku['special_price']) ? null : $sku['special_price'],
                        'cost' => 0,
                        'weight' => !empty($product['dimensions']['weight']) ? $product['dimensions']['weight'] : 1,
                        'height' => !empty($product['dimensions']['height']) ? $product['dimensions']['height'] : 1,
                        'width'  => !empty($product['dimensions']['width'])  ? $product['dimensions']['width']  : 1,
                        'length' => !empty($product['dimensions']['length']) ? $product['dimensions']['length'] : 1,
                        'brand' => $product['brand'],
                        'ean' => $sku['ean'],
                        'url' => self::MOBLY_URL . $product['product_external_id'] . '.html?utm_medium=partnership&utm_source=easydeco',
                    ];

                    $partsPathCategory = explode('##', $product['path_category']);
                    array_pop($partsPathCategory);
                    array_shift($partsPathCategory);
                    $pathCategory = implode(' > ', $partsPathCategory);
                    $formattedProduct['category'][] = [
                        'code' => $product['external_id'],
                        'name' => $pathCategory
                    ];

                    $formattedProduct['category_product'] = end($partsPathCategory);

                    foreach ($product['images'] as $image) {
                        $formattedProduct['images'][] = $image;
                    }

                    foreach ($product['attributes'] as $key => $attribute) {
                        $formattedProduct['specifications'][] = [
                            'key' => $key,
                            'value' => $attribute
                        ];
                    }

                    $formattedProducts[$sku['sku']][] = $formattedProduct;
                }

            }
        }

        return $formattedProducts;
    }

    /**
     * @param $product
     * @return bool
     */
    public function isValidProduct($product)
    {
        $this->validator->setData($product);

        try {
            $result = $this->validator->validate();
        } catch (ValidationException $exception) {
            foreach ($product['skus'] as $sku) {
                foreach ($exception->errors() as $error) {
                    foreach ($error as $msg) {
                        $aux[] = $msg;
                    }
                    $this->integratorAbstract->addErrorsWithKey($aux, $sku['sku']);
                }
            }
            $result = false;
        }

        return $result;
    }
}
