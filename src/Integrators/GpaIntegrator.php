<?php

namespace Mobly\MarketplaceSdk\Integrators;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Mobly\MarketplaceSdk\Helpers\Helper;
use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use Mobly\MarketplaceSdk\Validators\Gpa\Product\Validator as ProductValidator;
use Mobly\MarketplaceSdk\Validators\Gpa\Validator as GpaValidator;
use Mobly\MarketplaceSdk\Validators\Gpa\Offer\Validator as OfferValidator;
use Mobly\MarketplaceSdk\Validators\Gpa\Price\Validator as PriceValidator;
use League\Csv\CannotInsertRecord;
use League\Csv\Exception as CsvException;

class GpaIntegrator extends IntegratorAbstract
{
    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws CannotInsertRecord
     * @throws CsvException
     */
    public function integrate(string $method, array $data)
    {
        $gpaValidator = new GpaValidator($data);
        if (!$gpaValidator->validate()) {
            return [];
        }

        if (empty($data['products']) && empty($data['offers'])) {
            return [
                'errors' => $this->getErrors()
            ];
        }
        $content = !empty($data['products'])
            ? $this->transform($data['products'])
            : $this->transformOffer($data['offers']);

        if (!$content) {
            return [
                'errors' => $this->getErrors()
            ];
        }
        $request = new Request($method, $data['request']['url']);
        $request->addOption([
            'headers' => [
                'Authorization' => $data['request']['shop_key'],
                'Accept' => 'application/json',
            ]
        ]);

        $client = new Client();

        if (!empty($data['offers'])) {
            $response = (array) $client->jsonRequest($request, $content);
        } else {
            $response = (array) $client->fileRequest($request, 'file', 'products.csv', $content);
        }

        $response['errors'] = $this->getErrors();
        return $response;
    }
    /**
     * @param array $products
     * @return string
     * @throws CannotInsertRecord
     */
    protected function transform(array $products)
    {
        $result = [];

        foreach ($products as $sku => $product) {
            $result = array_merge($result, $this->parseProductData($product));
        }

        return Helper::getCsvFromArray($result);
    }

    protected function parseProductData(array $product)
    {
        $results = [];

        if (!$this->isValidProduct($product)) {
            return $results;
        }

        foreach ($product['skus'] as $productSku) {
            $productName = empty($productSku['superAttribute'])
                ? $product['name']
                : sprintf('%s - %s', $product['name'], $productSku['superAttribute']);

            $newProduct = [
                'name' => $productName,
                'marca' => $product['brand'],
                'categoria' => $product['store_category_external_id'],
                'sku' => $productSku['sku'],
                'description' => strip_tags($product['description']),
                'ean1' => $productSku['ean'],
                'width' => $product['dimensions']['width'] ?? 1,
                'height' => $product['dimensions']['height'] ?? 1,
                'length' => $product['dimensions']['length'] ?? 1,
                'weight' => number_format($product['dimensions']['weight'], 2) * 1000 ?? 1,
                'imagem1' => $product['images'][0],
                'imagem2' => !empty($product['images'][1]) ? $product['images'][1] : null,
                'imagem3' => !empty($product['images'][2]) ? $product['images'][2] : null,
                'imagem4' => !empty($product['images'][3]) ? $product['images'][3] : null,
                'imagem5' => !empty($product['images'][4]) ? $product['images'][4] : null,
                'seller-atributte' => $this->attributeParser($product['attributes']),
            ];

            if (isset($product['store_attributes'])) {
                foreach ($product['store_attributes'] as $storeAttribute) {
                    $newProduct[$storeAttribute['external_id']] = $storeAttribute['external_value_id'];
                }
            }

            if (isset($product['attributes'])) {
                foreach ($product['attributes'] as $key=>$value) {
                    $newProduct[$key] = $value;
                }
            }

            $results[] = $newProduct;
        }

        return $results;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function transformOffer(array $data)
    {
        $results = [];
        foreach ($data as $offer) {
            if ($this->isValidOffer($offer)) {

                $arrOffer = [
                    'price' => $offer['price'], //PREÇO DE
                    'product_id' => $offer['sku'],
                    'product_id_type' => 'SHOP_SKU',
                    'quantity' => $offer['quantity'],
                    'shop_sku' => $offer['sku'],
                    'min_quantity_alert' => 1,
                    'state_code' => 11, //hardcoded, for more info consult Gpa documentation
                    'update_delete' => 'update',
                    'all_prices' => [[
                        'channel_code' => 'GPA',
                        'unit_origin_price' => $offer['price'],
                        'discount_end_date' => !empty($offer['special_price']) ? Carbon::parse($offer['special_price_end'])->toAtomString() : null,
                        'discount_start_date' => !empty($offer['special_price']) ? Carbon::parse($offer['special_price_start'])->toAtomString() : null,
                        'unit_discount_price' => !empty($offer['special_price']) ? $offer['special_price'] : null
                    ], [
                        'channel_code' => 'clubeextra',
                        'unit_origin_price' => $offer['price'],
                        'discount_end_date' => !empty($offer['special_price']) ? Carbon::parse($offer['special_price_end'])->toAtomString() : null,
                        'discount_start_date' => !empty($offer['special_price']) ? Carbon::parse($offer['special_price_start'])->toAtomString() : null,
                        'unit_discount_price' => !empty($offer['special_price']) ? $offer['special_price'] : null
                    ]]
                ];

                if(!empty($offer['special_price'])) {
                    $arrDiscount =
                        ['discount' =>
                            [
                                'price' => $offer['special_price'], //PREÇO POR
                                'end_date' => Carbon::parse($offer['special_price_end'])->toAtomString(),
                                'start_date' => Carbon::parse($offer['special_price_start'])->toAtomString()
                            ]
                        ];

                    $arrOffer = array_merge($arrOffer, $arrDiscount);
                }

                $results['offers'][] = $arrOffer;

            }
        }

        return $results;
    }

    /**
     * @param array $attributes
     * @return string
     */
    protected function attributeParser(array $attributes)
    {
        $attrString = '';

        foreach ($attributes as $key => $value) {
            $attrString .= "{$key}:{$value}|";
        }

        $attrString = substr($attrString, 0, -1);

        return $attrString;
    }

    /**
     * @param array $offer
     * @return bool
     */
    public function isValidOffer(array $offer)
    {
        $result = true;
        try {
            $offerValidator = new OfferValidator($offer);
            $offerValidator->validate();
        } catch (ValidationException $exception) {
            $this->addErrorsWithKey($exception->getMessage(), $offer['sku']);
            $result = false;
        }
        return $result;
    }

    /**
     * @param array $product
     * @return bool
     */
    public function isValidProduct(array $product)
    {
        $result = true;
        try {
            $productValidator = new ProductValidator($product);
            $productValidator->validate();
        } catch (ValidationException $exception) {
            foreach ($product['skus'] as $sku) {
                $this->addErrorsWithKey($exception->getMessage(), $sku['sku']);
            }
            $result = false;
        }
        return $result;
    }

    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws CsvException
     * @throws \Exception
     */
    public function getStatus(string $method, array $data)
    {
        if (empty($data['request']['url'])) {
            throw new \Exception('the field \'endpoint_url\' is required and cannot be empty',
                Response::HTTP_BAD_REQUEST);
        }

        if (empty($data['request']['shop_key'])) {
            throw new \Exception('the field \'shop_key\' is required and cannot be empty',
                Response::HTTP_BAD_REQUEST);
        }

        $client = new Client();

        $request = new Request($method, $data['request']['url']);
        $request->addOption([
            'headers' => [
                'Authorization' => $data['request']['shop_key'],
                'Accept' => 'application/json',
            ]
        ]);

        return $client->request($request);
    }

    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws CsvException
     * @throws \Exception
     */
    public function getOrders(string $method, array $data)
    {
        $gpaValidator = new GpaValidator($data);
        if (!$gpaValidator->validate()) {
            return null;
        }

        $request = new Request($method, $data['request']['url']);
        $request->addOption([
            'headers' => [
                'Authorization' => $data['request']['shop_key'],
                'Accept' => 'application/json',
            ]
        ]);

        if (!empty($data['params']) && is_array($data['params'])) {
            $request->addOption(['query' => $data['params']]);
        }

        if (!empty($data['order_aditional_fields']) && is_string($data['order_aditional_fields'])) {
            $request->addOption(['query' => ['order_aditional_fields' => $data['order_aditional_fields']]]);
        }

        $client = new Client();
        $rawResponse = $client->request($request);

        $response = $this->transformOrders($rawResponse);
        $response['raw'] = $rawResponse;

        return $response;
    }

    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getOrder(string $method, array $data)
    {
        return $this->getOrders($method, $data);
    }

    public function formatAddress($address) {
        $return = [];

        foreach($address as $key => $value) {
            if($key === 'street_1') {
                $addressStreet2 = explode(',', $value);
                $return['street_1'] = trim($addressStreet2[0]);
                $return['street_number'] = trim($addressStreet2[1]);
            }

            $return[$key] = $value;
        }

        return $return;
    }

    /**
     * @param array $adicional
     * @return mixed
     */
    public function getAddressFromAdditional(array $adicional) {

        $address = [];
        foreach($adicional as $field) {
            switch ($field->code) {
                case 'shipping-address-city':
                    $address['city'] = $field->value;
                    break;
                case 'shipping-address-country':
                    $address['country'] = 'BR'; //$field->value;
                    break;
                case 'shipping-address-phone':
                    $address['phone'] = $field->value;
                    break;
                case 'shipping-address-state':
                    $address['state'] = $field->value;
                    break;
                case 'shipping-address-street-1':
                    $address['street_1'] = $field->value;
                    break;
                case 'shipping-address-street-2':
                    $addressStreet2 = explode('-', $field->value);
                    if(count($addressStreet2) > 2) {
                        $address['street_number'] = trim($addressStreet2[0]);
                        $address['street_complement'] = trim($addressStreet2[1]);
                        $address['street_neighborhood'] = trim($addressStreet2[2]);
                    } else {
                        $address['street_number'] = trim($addressStreet2[0]);
                        $address['street_complement'] = '';
                        $address['street_neighborhood'] = trim($addressStreet2[1]);
                    }
                    break;
                case 'shipping-address-zip-code':
                    $address['zip_code'] = $field->value;
                    break;
            }
        }
        return $address;
    }

    /**
     * @param array $additional
     * @return array
     */
    public function getDocumentFromAdditional(array $additional)
    {
        foreach ($additional as $field) {
            if ($field->code == 'cpf' || $field->code == 'cnpj') {
                return $field->value;
            }
        }

        return [];
    }

    /**
     * @param \stdClass $response
     * @return array
     * @throws \Exception
     */
    protected function transformOrders(\stdClass $response)
    {
        try {
            $results['orders'] = [];

            foreach ($response->orders as $order) {
                $resolvedName = Helper::resolveFirstLastName([$order->customer->firstname, $order->customer->lastname]);

                $billingAddress = !empty($order->customer->billing_address) ? $this->formatAddress($order->customer->billing_address) : '';

                if(empty($billingAddress)) {
                    $billingAddress = $this->getAddressFromAdditional($order->order_additional_fields);
                }

                if (!empty($billingAddress)) {
                    $billingArray = [
                        'customer_phone' => !empty($billingAddress['phone']) ? preg_replace('/[^0-9]/', '', $billingAddress['phone']) : '',
                        'customer_phone2' => !empty($billingAddress['phone_secondary']) ? preg_replace('/[^0-9]/', '', $billingAddress['phone_secondary']) : '',
                        'billing_postcode' => preg_replace('/[^0-9]/', '', $billingAddress['zip_code']),
                        'billing_street' => $billingAddress['street_1'] ?? 'NI',
                        'billing_street_number' => $billingAddress['street_number'],
                        'billing_neighborhood' => $billingAddress['street_2'],
                        'billing_complement' => $billingAddress['street_complement'] ?? 'NI',
                        'billing_city' => $billingAddress['city'],
                        'billing_state' => $billingAddress['state'],
                        'billing_country' => 'BR',
                    ];
                }


                $shippingAddress = !empty($order->customer->shipping_address) ? $this->formatAddress($order->customer->shipping_address) : '';
                if(empty($shippingAddress)) {
                    $shippingAddress = $this->getAddressFromAdditional($order->order_additional_fields);
                }

                if (!empty($shippingAddress)) {
                    $shippingArray = [
                        'shipping_postcode' => preg_replace('/[^0-9]/', '', $shippingAddress['zip_code']),
                        'shipping_street' => $shippingAddress['street_1'],
                        'shipping_street_number' => $shippingAddress['street_number'],
                        'shipping_neighborhood' => $billingAddress['street_2'],
                        'shipping_complement' => $shippingAddress['street_complement'] ?? 'NI',
                        'shipping_city' => $shippingAddress['city'],
                        'shipping_state' => $shippingAddress['state'],
                        'shipping_country' => 'BR',
                    ];
                }
                $orderTransform = [
                    'store_order_number' => $order->order_id,
                    'store_order_status' => $order->order_state,
                    'store_last_updated_at' => $order->last_updated_date ?? date('Y-m-d H:i:s'),
                    'customer_first_name' => $resolvedName[0],
                    'customer_last_name' => $resolvedName[1],
                    'customer_document' => !empty($this->getDocumentFromAdditional($order->order_additional_fields)) ? $this->getDocumentFromAdditional($order->order_additional_fields) : preg_replace('/[^0-9x]/i', '', $order->customer->customer_id),
                    'customer_email' => "crf{$order->customer->customer_id}@mkt-gpa-merlin.com.br",
                    'customer_gender' => '',
                    'shipping_price' => $order->shipping_price,
                    'total_price' => $order->total_price,
                    'items' => [],
                ];

                foreach ($order->order_lines as $line) {
                    for($i=1; $i<= $line->quantity; $i++) {
                        $orderTransform['items'][] = [
                            'store_item_id' => "{$line->order_line_id}_{$i}",
                            'sku' => $line->offer_sku,
                            'sku_name' => $line->product_title,
                            'unit_price' => $line->price_unit,
                            'shipping_price' => $line->shipping_price/$line->quantity,
                            'quantity' => 1,
                            'total_commission' => $line->total_commission/$line->quantity,
                        ];
                    }
                }

                if(!empty($billingArray)) {
                    $orderTransform = array_merge($orderTransform, $billingArray);
                }

                if(!empty($shippingArray)) {
                    $orderTransform = array_merge($orderTransform, $shippingArray);
                }

                $extra['StorePurchasedDate'] = isset($order->acceptance_decision_date) ? $order->acceptance_decision_date : '';
                if (isset($order->customer_debited_date) && !empty($order->customer_debited_date)) {
                    $extra['StoreApprovedDate'] = isset($order->customer_debited_date) ? $order->customer_debited_date : '';
                }
                $extra['StoreEstimatedDeliveryDate'] = isset($order->leadtime_to_ship) ? $order->leadtime_to_ship : '';
                $extra['StoreScheduledDeliveryDate'] = $order->shipping_deadline;

                $orderTransform['extra'] = json_encode($extra);

                $results['orders'][] = $orderTransform;
            }

            return $results;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param string $method
     * @param array $requestData
     * @return array|mixed
     * @throws \Exception
     */
    public function getCategories(string $method, array $requestData)
    {
        $this->validate($requestData);

        $url = $requestData['request']['url'];
        $Authorization = $requestData['request']['shop_key'];

        $request = $this->getDefaultRequest($method, $url, $Authorization);

        return (new Client())->request($request);
    }

    /**
     * @param string $method
     * @param array $requestData
     * @return array|mixed
     * @throws \Exception
     */
    public function fetchAttributes(string $method, array $requestData)
    {
        $this->validate($requestData);

        $url = $requestData['request']['url'];
        $Authorization = $requestData['request']['shop_key'];

        $request = $this->getDefaultRequest($method, $url, $Authorization);
        $request->addOption([
            'query' => [
                'hierarchy' => $requestData['params']['category_code']
            ]
        ]);

        return (new Client())->request($request);
    }

    /**
     * @param string $method
     * @param array $requestData
     * @return array|mixed
     * @throws \Exception
     */
    public function fetchAttributesValues(string $method, array $requestData)
    {
        $this->validate($requestData);

        $url = $requestData['request']['url'];
        $Authorization = $requestData['request']['shop_key'];

        $request = $this->getDefaultRequest($method, $url, $Authorization);
        $request->addOption([
            'query' => [
                'code' => $requestData['params']['list_code']
            ]
        ]);

        return (new Client())->request($request);
    }

    /**
     * @param array $requestData
     * @return bool
     * @throws ValidationException
     */
    protected function validate(array $requestData)
    {
        return (new GpaValidator($requestData))->validate();
    }

    /**
     * @param string $method
     * @param string $url
     * @param string $Authorization
     * @return Request
     */
    protected function getDefaultRequest(string $method, string $url, string $Authorization)
    {
        $request = new Request($method, $url);
        $request->addOption([
            'headers' => [
                'Authorization' => $Authorization,
                'Accept' => 'application/json',
            ]
        ]);

        return $request;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \League\Csv\Exception
     */
    public function setOrderToDelivered(string $httpMethod, array $data)
    {
        $url = $data['request']['url'];
        $Authorization = $data['request']['shop_key'];

        $request = $this->getDefaultRequest($httpMethod, $url, $Authorization);

        $data = array_filter($data, function ($key) {
            return 'order_additional_fields' === $key;
        }, ARRAY_FILTER_USE_KEY);

        $client = new Client();

        $data['order_additional_fields'] = [$data['order_additional_fields']];
        return $client->jsonRequest($request, $data);
    }

    public function sendInvoice(string $httpMethod, array $data)
    {
//        $request = new Request($method, $data['request']['url']);
//
//        $request->addOption([
//            'headers' => [
//                'Authorization' => $data['request']['shop_key'],
//                'Content-Type' => 'multipart/form-data',
//                'Accept' => 'application/json',
//            ],
//            'query' => [
//                'order_documents' => [[
//                    'file_name' => $data['file']['name'],
//                    'type_code' => 'CUSTOMER_INVOICE'
//                ]]
//            ]
//        ]);
//
//        $client = new Client();
//        return $client->fileRequest($request, 'files', $data['file']['name'], $data['file']['raw']);

        $url = $data['request']['url'];
        $Authorization = $data['request']['shop_key'];

        $request = $this->getDefaultRequest($httpMethod, $url, $Authorization);

        $data = array_filter($data, function ($key) {
            return 'order_additional_fields' === $key;
        }, ARRAY_FILTER_USE_KEY);

        $client = new Client();

        $data['order_additional_fields'] = $data['order_additional_fields'];
        return $client->jsonRequest($request, $data);

    }

    /**
     * @param string $method
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws \League\Csv\Exception
     */
    public function sendTrackingCode(string $method, array $data)
    {
        $gpaValidator = new GpaValidator($data);

        if ($gpaValidator->validate()) {
            $request = new Request($method, $data['request']['url']);

            $request->addOption([
                'headers' => [
                    'Authorization' => $data['request']['shop_key']
                ]
            ]);

            $client = new Client();

            unset($data['request']);

            return $client->jsonRequest($request, $data);
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws \Exception
     */
    public function getProducts(string $httpMethod, array $data)
    {
        $gpaValidator = new GpaValidator($data);

        $gpaValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->addOption([
            'headers' => [
                'Authorization' => $data['request']['shop_key'],
                'Accept' => 'application/json',
            ]
        ]);
        $request->addOption([
            'query' => $data['request']['params'],
        ]);

        return (new Client())->request($request);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws \Exception
     */
    public function findProducts(string $httpMethod, array $data)
    {
        $magaluValidator = new GpaValidator($data);
        $magaluValidator->validate();

        $requestData = $data['request'];

        $request = new Request($httpMethod, $requestData['url']);

        $request->addOption([
            'query' => [
                'product_references' => $requestData['product_references']
            ],
            'headers' => [
                'Authorization' => $requestData['shop_key']
            ]
        ]);

        return (new Client())->request($request);
    }

    /**
     * @param string $method
     * @param array $data
     * @return array
     */
    public function integratePrice(string $method, array $data)
    {
        $leroyValidator = new GpaValidator($data);
        if (!$leroyValidator->validate()) {
            return [];
        }

        $content = $this->transformPrices($data['prices']);

        if (!$content) {
            return [
                'errors' => $this->getErrors()
            ];
        }
        $request = new Request($method, $data['request']['url']);
        $request->addOption([
            'headers' => [
                'Authorization' => $data['request']['shop_key'],
                'Accept' => 'application/json',
            ]
        ]);

        $client = new Client();
        $response = (array) $client->fileRequest($request, 'file', 'prices.csv', $content);
        $response['errors'] = $this->getErrors();
        return $response;
    }

    /**
     * @param array $products
     * @return string
     */
    protected function transformPrices(array $products)
    {
        $result = [];

        foreach ($products as $sku => $product) {
            $result = array_merge($result, $this->parseProductDataPrice($product['skus']));
        }

        return Helper::getCsvFromArray($result);
    }

    /**
     * @param array $data
     * @return array
     */
    protected function parseProductDataPrice(array $data)
    {
        $results = [];
        foreach ($data as $price) {
            if ($this->isValidPrice($price)) {

                $arrPrice = [
                    'offer-sku' => $price['sku'],
                    'price' => $price['price'],
                ];

                if(!empty($price['special_price'])) {
                    $arrDiscount = [
                        'discount-price' => $price['special_price'], //PREÇO POR
                        'discount-end-date' => Carbon::parse($price['special_price_end'])->toAtomString(),
                        'discount-start-date' => Carbon::parse($price['special_price_start'])->toAtomString()
                    ];

                    $arrPrice = array_merge($arrPrice, $arrDiscount);
                }

                $results[] = $arrPrice;

            }
        }

        return $results;
    }

    /**
     * @param array $price
     * @return bool
     */
    public function isValidPrice(array $price)
    {
        $result = true;
        try {
            $priceValidator = new PriceValidator($price);
            $priceValidator->validate();
        } catch (ValidationException $exception) {
            $this->addErrorsWithKey($exception->getMessage(), $price['sku']);
            $result = false;
        }
        return $result;
    }
}