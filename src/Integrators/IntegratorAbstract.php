<?php

namespace Mobly\MarketplaceSdk\Integrators;

use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;

abstract class IntegratorAbstract
{
    protected $errors = array();

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     */
    abstract public function integrate(string $httpMethod, array $data);

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     */
    abstract public function getStatus(string $httpMethod, array $data);

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     */
    abstract public function getOrders(string $httpMethod, array $data);

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     */
    abstract public function getOrder(string $httpMethod, array $data);

    /**
     * IntegratorAbstract constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param $error
     */
    public function addErrors($error)
    {
        $this->errors[] = $error;
    }

    /**
     * @param $error
     * @param $key
     */
    public function addErrorsWithKey($error, $key)
    {
        $this->errors[$key] = $error;
    }

    protected function defaultRequest(string $method, array $data)
    {
        $request = new Request($method, $data['request']['url']);

        if(!empty($data['request']['messageBody'])) {
            $request->addOption(['json' => $data['request']['messageBody']]);
        }

        if (!empty($data['request']['params'])) {
            $request->addOption(['query' => $data['request']['params']]);
        }

        if (!empty($data['request']['headers'])) {
            $request->addOption(['headers' => $data['request']['headers']]);
        }

        $client = new Client();

        $response = $client->request($request);
        $responseAsAssocArray = json_decode(json_encode($response), true);

        if (!$responseAsAssocArray) {
            return [];
        }

        return $responseAsAssocArray;
    }
}