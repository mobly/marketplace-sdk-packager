<?php
/**
 * Created by PhpStorm.
 * User: hermes
 * Date: 15/10/18
 * Time: 17:37
 */

namespace Mobly\MarketplaceSdk\Integrators;

use Illuminate\Support\Arr;
use Mobly\MarketplaceSdk\Helpers\Helper;
use Mobly\MarketplaceSdk\Integrators\MercadoLivre\Auth;
use Mobly\MarketplaceSdk\Integrators\MercadoLivre\Product;
use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use mysql_xdevapi\Exception;

class MercadoLivreIntegrator extends IntegratorAbstract
{
    /**
     * @var MercadoLivre\Price
     */
    protected $price;

    /**
     * @var MercadoLivre\Product
     */
    protected $product;

    /**
     * @var MercadoLivre\Stock
     */
    protected $stock;

    /**
     * MercadoLivreIntegrator constructor.
     */
    public function __construct()
    {
        $this->price = new MercadoLivre\Price();
        $this->product = new MercadoLivre\Product();
        $this->stock = new MercadoLivre\Stock();
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     */
    public function integrate(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => [],
        ];

        list($formattedProducts, $skusWithErrors) = $this->product->formatProducts($data);

        $productsResults['errors'] = $skusWithErrors;

        $client = new Client();

        $i = 0;

        foreach ($formattedProducts as $sku => $products) {
            foreach ($products as $formattedProduct) {
                try {
                    $request = new Request($httpMethod, $data['request']['url']);

                    $request->addOption([
                        'query' => $data['request']['params'],
                    ]);
                    $request->addOption([
                        'json' => $formattedProduct,
                    ]);
                    $request->addOption([
                        'headers' => $data['request']['headers']
                    ]);

                    $response = $client->request($request);

                    $productsResults['success'][$sku][$i] = (array)$response;
                    $productsResults['success'][$sku][$i]['status_code'] = $client->getStatusCode() ?? 200;

                    $i++;
                } catch (\Exception $exception) {
                    $statusCode = 500;

                    // prevent add invalid status code 0 to return
                    if ($exception->getCode() != 0)
                        $statusCode = $exception->getCode();

                    $productsResults['errors'][$sku] = [
                        'message' => $exception->getMessage(),
                        'status_code' => $statusCode
                    ];
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function update(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => array(),
            'errors'  => array(),
        ];

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {

                if(empty($sku['store_sku'])) {
                    $productsResults['errors'][$sku['sku']][] = ['message' => 'Is empty store skus'];
                }

                foreach ($sku['store_sku'] as $storeSku) {
                    try {

                        $response = $this->getProductData($data, $storeSku);
                        $soldQuantity = (isset($response['sold_quantity']) && $response['sold_quantity'] > 0) ? true : false;
                        $formattedProduct = $this->product->formatProductUpdateSimple($product, $soldQuantity);

                        if (empty($formattedProduct)) {
                            throw new \Exception('Not found to formatted');
                        }

                        $response = $this->sendProductData($httpMethod, $data, $storeSku, $formattedProduct);
                        $productsResults['success'][$sku['sku']][] = (array)$response;
                    } catch (\Throwable $e) {
                        $productsResults['errors'][$sku['sku']][] = ['message' => $e->getMessage()];
                    }
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function updateDescription(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => [],
        ];

        list($formattedProducts, $skusWithErrors) = $this->product->formatProductsToUpdateDescription($data['products']);

        $productsResults['errors'] = $skusWithErrors;

        $client = new Client();

        foreach ($formattedProducts as $sku => $products) {
            foreach ($products as $formattedProduct) {
                try {
                    $request = new Request($httpMethod,
                        $this->_makeUrl($formattedProduct['store_sku'], $data['request']['url']));

                    unset($formattedProduct['store_sku']);

                    $request->addOption([
                        'query' => $data['request']['params'],
                    ]);
                    $request->addOption([
                        'json' => $formattedProduct,
                    ]);
                    $request->addOption([
                        'headers' => $data['request']['headers']
                    ]);

                    $response = $client->request($request);

                    $productsResults['success'][$sku][] = (array)$response;
                } catch (\Exception $exception) {
                    $productsResults['errors'][$sku][] = ['message' => $exception->getMessage()];
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function checkProduct(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => [],
        ];

        $client = new Client();
        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {

                if(empty($sku['store_sku'])) {
                    $productsResults['errors'][$sku['sku']][] = ['message' => 'Is empty store skus'];
                }

                foreach ($sku['store_sku'] as $storeSku) {
                    try {
                        $request = new Request($httpMethod,
                            $this->_makeUrl($storeSku, $data['request']['url']));

                        $request->addOption([
                            'query' => $data['request']['params'],
                        ]);
                        $request->addOption([
                            'headers' => $data['request']['headers']
                        ]);

                        $response = $client->request($request);
                        if ($response->status == 'active') {
                            throw new \Exception('Product active');
                        }

                        $productsResults['success'][$sku['sku']][] = (array)$response;
                    } catch (\Exception $exception) {
                        $productsResults['errors'][$sku['sku']][] = ['message' => $exception->getMessage()];
                    }
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed|void
     */
    public function getStatus(string $httpMethod, array $data)
    {
        // TODO: Implement getStatus() method.
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getOrders(string $httpMethod, array $data)
    {
        $request = new Request($httpMethod, $data['request']['url']);

        $result['orders'] = [];

        $request->addOption([
            'query' => $data['request']['params'],
        ]);
        $request->addOption([
            'headers' => $data['request']['headers']
        ]);

        $client = new Client();

        $response = $client->request($request);

        $orders = [json_decode(json_encode($response->results), true)];

        $result = $this->_transformOrders($orders[0]);
        $result['total'] = $response->paging->total;
        $result['offset'] = $response->paging->offset;
        $result['raw'] = $response;

        return (array)$result;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getOrder(string $httpMethod, array $data)
    {
        $request = new Request($httpMethod, $this->_makeUrl($data['order'], $data['request']['url']));

        $request->addOption([
            'query' => $data['request']['params'],
        ]);
        $request->addOption([
            'headers' => $data['request']['headers']
        ]);

        $client = new Client();

        $rawResponse = $client->request($request);

        $order = [json_decode(json_encode($rawResponse), true)];

        $response = $this->_transformOrders($order);
        $response['raw'] = $rawResponse;

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function setOrderToShipped(string $httpMethod, array $data)
    {
        $request = new Request($httpMethod, $data['request']['url']);

        $request->addOption([
            'query' => $data['request']['params'],
        ]);
        $request->addOption([
            'headers' => $data['request']['headers']
        ]);
        $request->addOption([
            'headers' => $data['request']['headers']
        ]);

        $client = new Client();

        $result = $client->request($request);

        if (!empty($result->shipping)) {
            $shippingId = $result->shipping->id;
            $shipmentUrl = str_replace('%s', $shippingId, $data['request']['shipment_url']);
            $shipmentRequest = new Request('PUT', $shipmentUrl);
            $shipmentRequest->addOption([
                'query' => $data['request']['params'],
            ]);
            $shipmentRequest->addOption([
                'json' => $data['request']['body'],
            ]);
            $request->addOption([
                'headers' => $data['request']['headers']
            ]);
            $shipmentRequest->addOption([
                'decode_content' => 'gzip'
            ]);

            return $client->request($shipmentRequest);
        }

        throw new \Exception('could not read shipping field', 500);
    }

    public function setOrderToDelivered(string $httpMethod, array $data)
    {
        $request = new Request($httpMethod, $data['request']['url']);

        $request->addOption([
            'query' => $data['request']['params'],
        ]);
        $request->addOption([
            'json' => $data['request']['body'],
        ]);
        $request->addOption([
            'headers' => $data['request']['headers']
        ]);

        return (new Client)->request($request);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function deleteProducts(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => [],
        ];

        $client = new Client();

        $request = new Request($httpMethod);

        foreach ($data['request']['data']['skus'] as $sku) {
            try {
                if ($data['request']['step'] == 1)
                    $request->addOption([
                        'headers' => $data['request']['headers'],
                        'query' => $data['request']['params'],
                        'json' => ['status' => 'closed']
                    ]);
                else
                    $request->addOption([
                        'headers' => $data['request']['headers'],
                        'query' => $data['request']['params'],
                        'json' => ['deleted' => 'true']
                    ]);

                $request->setUrl($this->_makeUrl($sku->store_sku, $data['request']['url']));
                $response = $client->request($request);
                $productsResults['success'][$sku->sku] = $response->status;
            } catch (\Exception $exception) {
                $productsResults['errors'][$sku->sku] = ['message' => $exception->getMessage()];
            }
        }
        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function getAccessToken(string $httpMethod, array $data)
    {
        if (!array_key_exists('client_id', $data['request']['keys'])) {
            throw new \Exception('client_id not found.');
        }

        if (!array_key_exists('client_secret', $data['request']['keys'])) {
            throw new \Exception('client_secret not found.');
        }

        $auth = new Auth($data['request']['keys']['client_id'], $data['request']['keys']['client_secret']);

        $response = $auth->getAccessToken();
        return (array)$response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function refreshAccessToken(string $httpMethod, array $data)
    {
        if (!array_key_exists('client_id', $data['request']['keys'])) {
            throw new \Exception('client_id not found.');
        }

        if (!array_key_exists('client_secret', $data['request']['keys'])) {
            throw new \Exception('client_secret not found.');
        }

        if (!array_key_exists('refresh_token', $data['request']['params'])) {
            throw new \Exception('refresh_token not found.');
        }

        $auth = new Auth($data['request']['keys']['client_id'], $data['request']['keys']['client_secret']);
        $auth->setRefreshToken($data['request']['params']['refresh_token']);

        $response = $auth->refreshAccessToken();
        return (array)$response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function integrateStocks(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => array(),
            'errors'  => array(),
        ];

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {

                if(empty($sku['store_sku'])) {
                    $productsResults['errors'][$sku['sku']][] = ['message' => 'Is empty store skus'];
                }

                foreach ($sku['store_sku'] as $storeSku) {
                    try {
                        $response = $this->getProductData($data, $storeSku);

                        if(in_array($response['status'], ['under_review', 'closed', 'inactive']) ) {
                            throw new \Exception('Status ' . $response['status']);
                        }

                        $idVariation =  (!empty($response['variations'])) ? $response['variations'][0]['id'] : false;
                        $formattedProduct = $this->stock->format($sku, $idVariation);

                        if (empty($formattedProduct)) {
                            throw new \Exception('Not found to formatted');
                        }

                        $response = $this->sendProductData($httpMethod, $data, $storeSku, $formattedProduct);

                        $productsResults['success'][$sku['sku']][] = (array)$response;
                    } catch (\Throwable $e) {
                        $productsResults['errors'][$sku['sku']][] = ['message' => $e->getMessage()];
                    }
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function integratePrice(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => array(),
            'errors'  => array(),
        ];

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {

                if(empty($sku['store_sku'])) {
                    $productsResults['errors'][$sku['sku']][] = ['message' => 'Is empty store skus'];
                }

                foreach ($sku['store_sku'] as $storeSku) {
                    try {

                        $response = $this->getProductData($data, $storeSku);
                        if(!in_array($response['status'], ['active', 'paused']) ) {
                            throw new \Exception('Status ' . $response['status']);
                        }

                        $idVariation =  (!empty($response['variations'])) ? $response['variations'][0]['id'] : false;
                        $formattedProduct = $this->price->format($sku, $idVariation);

                        if (empty($formattedProduct)) {
                            throw new \Exception('Not found to formatted');
                        }

                        $response = $this->sendProductData($httpMethod, $data, $storeSku, $formattedProduct);

                        $productsResults['success'][$sku['sku']][] = (array)$response;
                    } catch (\Throwable $e) {
                        $productsResults['errors'][$sku['sku']][] = ['message' => $e->getMessage()];
                    }
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param $data
     * @param $sku
     * @param $storeSku
     * @return array
     * @throws \Exception
     */
    public function getProductData($data, $storeSku)
    {
        $client   = new Client();
        $request = new Request('GET',
            $this->_makeUrl($storeSku, $data['request']['url']));

        $response = $client->request($request);
        return json_decode(json_encode($response), true);
    }

    /**
     * @param $data
     * @param $storeSku
     * @return bool
     * @throws \Exception
     */
    public function getPromotions($data, $storeSku)
    {
        $client   = new Client();
        $request = new Request('GET',
            $this->_makeUrl($storeSku, $data['request']['url-promotion']));

        $request->addOption([
            'headers' => $data['request']['headers']
        ]);

        $response = $client->request($request);
        $response = json_decode(json_encode($response), true);
        $promotions = array_filter($response, function($row){
            if($row['status'] == 'started')  {
                return $row;
            }
        });

        return (count($promotions) ? true : false);
    }

    /**
     * @param $httpMethod
     * @param $data
     * @param $storeSku
     * @param $formattedProduct
     * @return array|mixed
     * @throws \Exception
     */
    public function sendProductData($httpMethod, $data, $storeSku, $formattedProduct)
    {
        $client   = new Client();
        $request = new Request($httpMethod,
            $this->_makeUrl($storeSku, $data['request']['url']));

        $request->addOption([
            'query' => $data['request']['params'],
        ]);
        $request->addOption([
            'json' => $formattedProduct,
        ]);
        $request->addOption([
            'headers' => $data['request']['headers']
        ]);

        return $client->request($request);
    }

    /**
     * @param $replaceParam
     * @param $url
     * @return mixed
     */
    private function _makeUrl($replaceParam, $url)
    {
        return str_replace('%s', $replaceParam, $url);
    }

    /**
     * Detect and validate meli phone number
     * @param $buyer
     * @return string
     */
    public function formatCustomerPhone($buyer)
    {
        if (!empty($buyer['phone']['number'])) {
            return $buyer['phone']['area_code'] . $buyer['phone']['number'];
        } else if (empty($buyer['phone']['number']) && !empty($buyer['alternative_phone']['number'])) {
            return $buyer['alternative_phone']['area_code'] . $buyer['alternative_phone']['number'];
        }

        return 'NI';
    }

    /**
     * @param array $orders
     * @return mixed
     * @throws \Exception
     */
    private function _transformOrders(array $orders)
    {
        $result['orders'] = [];

        foreach ($orders as $order) {
            try {
                $buyer = Arr::get($order, 'buyer', []);

                $resolvedName = Helper::resolveFirstLastName([
                    Arr::get($buyer, 'first_name', ''),
                    Arr::get($buyer, 'last_name', '')
                ]);

                $cpfFake = !empty($buyer['billing_info']['doc_number']) ?
                    $buyer['billing_info']['doc_number'] : 'NI';

                $orderTransform = [
                    'store_order_number' => $order['id'],
                    'store_order_status' => $order['status'],
                    'customer_first_name' => $resolvedName[0],
                    'customer_last_name' => $resolvedName[1],
                    'customer_document' => $cpfFake,
                    'customer_email' => !empty($buyer['email']) ? $buyer['email'] : 'NI',
                    'customer_phone' => $this->formatCustomerPhone($buyer),
                    'customer_gender' => '',
                    'billing_postcode' => !empty($order['shipping']['receiver_address']['zip_code']) ?
                        $order['shipping']['receiver_address']['zip_code'] : 'NI',
                    'billing_street' => !empty($order['shipping']['receiver_address']['street_name']) ?
                        $order['shipping']['receiver_address']['street_name'] : 'NI',
                    'billing_street_number' => !empty($order['shipping']['receiver_address']['street_number']) ?
                        $order['shipping']['receiver_address']['street_number'] : 'NI',
                    'billing_neighborhood' => !empty($order['shipping']['receiver_address']['neighborhood']['name']) ?
                        $order['shipping']['receiver_address']['neighborhood']['name'] : 'NI',
                    'billing_complement' => !empty($order['shipping']['receiver_address']['comment']) ?
                        $order['shipping']['receiver_address']['comment'] : 'NI',
                    'billing_city' => !empty($order['shipping']['receiver_address']['city']['name']) ?
                        $order['shipping']['receiver_address']['city']['name'] : 'NI',
                    'billing_state' => !empty($order['shipping']['receiver_address']['state']['name']) ?
                        $order['shipping']['receiver_address']['state']['name'] : 'NI',
                    'billing_country' => !empty($order['shipping']['receiver_address']['country']['id']) ?
                        $order['shipping']['receiver_address']['country']['id'] : 'NI',
                    'shipping_id' => Arr::get($order, 'shipping.id', null),
                    'pack_id' => Arr::get($order, 'pack_id', null),
                    'shipping_status' => !empty($order['shipping']['status']) ? $order['shipping']['status'] :
                        'NI',
                    'shipping_postcode' => !empty($order['shipping']['receiver_address']['zip_code']) ?
                        $order['shipping']['receiver_address']['zip_code'] : 'NI',
                    'shipping_street' => !empty($order['shipping']['receiver_address']['street_name']) ?
                        $order['shipping']['receiver_address']['street_name'] : 'NI',
                    'shipping_street_number' => !empty($order['shipping']['receiver_address']['street_number']) ?
                        $order['shipping']['receiver_address']['street_number'] : 'NI',
                    'shipping_neighborhood' => !empty($order['shipping']['receiver_address']['neighborhood']['name']) ?
                        $order['shipping']['receiver_address']['neighborhood']['name'] : 'NI',
                    'shipping_complement' => !empty($order['shipping']['receiver_address']['comment']) ?
                        $order['shipping']['receiver_address']['comment'] : 'NI',
                    'shipping_city' => !empty($order['shipping']['receiver_address']['city']['name']) ?
                        $order['shipping']['receiver_address']['city']['name'] : 'NI',
                    'shipping_state' => !empty($order['shipping']['receiver_address']['state']['name']) ?
                        $order['shipping']['receiver_address']['state']['name'] : 'NI',
                    'shipping_country' => !empty($order['shipping']['receiver_address']['country']['id']) ?
                        $order['shipping']['receiver_address']['country']['id'] : 'NI',
                    'shipping_price' => !empty($order['shipping']['shipping_option']['cost']) ?
                        $order['shipping']['shipping_option']['cost'] : 0,
                    'total_price' => Arr::get($order, 'total_amount', 0),
                    'store_last_updated_at' => date('Y-m-d H:i:s',
                        strtotime(urldecode(!empty($order['last_updated']) ? $order['last_updated'] :
                            $order['date_last_updated']))),
                    'items' => [],
                ];

                foreach ($order['order_items'] as $item) {
                    for($i=1; $i<=$item['quantity']; $i++) {
                        $orderTransform['items'][] = [
                            'store_item_id' => $i,
                            'mlb' => $item['item']['id'],
                            'sku' => '',
                            'sku_name' => $item['item']['title'],
                            'unit_price' => $item['unit_price'],
                            'shipping_price' => 0, // MLB não traz esta informação por item
                            'quantity' => 1,
                            'total_commission' => 0,
                        ];
                    }
                }

                $extra['StorePurchasedDate'] = isset($order['date_created']) ? $order['date_created'] : '';
                if (isset($order['date_closed']) && !empty($order['date_closed'])) {
                    $extra['StoreApprovedDate'] = isset($order['date_closed']) ? $order['date_closed'] : '';
                }
                $extra['StoreEstimatedDeliveryDate'] = isset($order['expiration_date']) ? $order['expiration_date'] : '';

                $orderTransform['extra'] = json_encode($extra);

                $result['orders'][] = $orderTransform;

            } catch (\Exception $ex) {
                continue;
                //TODO tratar em caso de erro
//                throw new \Exception($ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return $result;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getProducts(string $httpMethod, array $data)
    {
        $request = new Request($httpMethod, $this->_makeUrl($data['request']['params']['seller'], $data['request']['url']));

        $data['request']['params']['offset'] = !empty($data['request']['params']['offset']) ? $data['request']['params']['offset'] : 0;

        if (count($data['request']['params'])) {
            $request->addOption(['query' => $data['request']['params']]);
        }
        if (count($data['request']['headers'])) {
            $request->addOption(['headers' => $data['request']['headers']]);
        }

        $client = new Client();

        $rawResponse = $client->request($request);
        $products = [json_decode(json_encode($rawResponse), true)];

        if(count($products)) {
            $productsTransfer = new Product($this);
            $response['products'] = $productsTransfer->transfProductsStore($products);
            $response['scroll_id'] = !empty($products[0]['scroll_id']) ? $products[0]['scroll_id'] : false;
            $response['total'] = !empty($products[0]['paging']['total']) ? $products[0]['paging']['total'] : 0;
        }

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getProduct(string $httpMethod, array $data)
    {
        $request = new Request($httpMethod, $this->_makeUrl($data['request']['params']['sku'], $data['request']['url']));

        if (count($data['request']['params'])) {
            $request->addOption(['query' => $data['request']['params']]);
        }
        if (count($data['request']['headers'])) {
            $request->addOption(['headers' => $data['request']['headers']]);
        }

        $client = new Client();

        $rawResponse = $client->request($request);
        $product = json_decode(json_encode($rawResponse), true);

        if(count($product)) {
            $productsTransfer = new Product($this);
            $response['product_mlb'] = $productsTransfer->transfProductStore($product);
        }

        return $response;
    }

    public function getShipmentData(string $httpMethod, array $data)
    {
        $requestData = $data['request'];
        $requestParams = $requestData['params'];
        $requestHeaders = $requestData['headers'];

        $request = new Request($httpMethod, $requestData['url']);

        if (count($requestParams)) {
            $request->addOption(['query' => $requestParams]);
        }
        if (count($requestHeaders)) {
            $request->addOption(['headers' => $requestHeaders]);
        }

        $client = new Client();

        $response = $client->request($request);
        $arrayAssocResponse = json_decode(json_encode($response), true);;

        if (!$response) {
            return [];
        }

        $receiverAddress = Arr::get($arrayAssocResponse, 'receiver_address', null);

        $mode = Arr::get($arrayAssocResponse,'mode','');
        if(empty($mode)) {
            $mode = Arr::get($arrayAssocResponse,'logistic.mode','');
        }

        return [
            'order_id' => Arr::get($arrayAssocResponse, 'order_id', null),
            'shipping_id' => Arr::get($arrayAssocResponse, 'id', null),
            'pack_id' => Arr::get($arrayAssocResponse, 'external_reference', null),
            'logistic_mode' => $mode ?? 'me1',
            'billing_postcode' => Arr::get($receiverAddress, 'zip_code', 'NI') ?: 'NI',
            'billing_street' => Arr::get($receiverAddress, 'street_name', 'NI') ?: 'NI',
            'billing_street_number' => Arr::get($receiverAddress, 'street_number', 'NI') ?: 'NI',
            'billing_complement' => Arr::get($receiverAddress, 'comment', 'NI') ?: 'NI',
            'billing_city' => Arr::get($receiverAddress, 'city.name', 'NI') ?: 'NI',
            'billing_country' => Arr::get($receiverAddress, 'country.id', 'NI') ?: 'NI',
            'billing_state' => Arr::get($receiverAddress, 'state.name', 'NI') ?: 'NI',
            'billing_neighborhood' => Arr::get($receiverAddress, 'neighborhood.name', 'NI') ?: 'NI',
            'shipping_status' => Arr::get($arrayAssocResponse, 'status', 'NI') ?: 'NI',
            'shipping_postcode' =>  Arr::get($receiverAddress, 'zip_code', 'NI') ?: 'NI',
            'shipping_street' => Arr::get($receiverAddress, 'street_name', 'NI') ?: 'NI',
            'shipping_street_number' =>  Arr::get($receiverAddress, 'street_number', 'NI') ?: 'NI',
            'shipping_complement' => Arr::get($receiverAddress, 'comment', 'NI') ?: 'NI',
            'shipping_city' => Arr::get($receiverAddress, 'city.name', 'NI') ?: 'NI',
            'shipping_country' => Arr::get($receiverAddress, 'country.id', 'NI') ?: 'NI',
            'shipping_state' => Arr::get($receiverAddress, 'state.name', 'NI') ?: 'NI',
            'shipping_neighborhood' => Arr::get($receiverAddress, 'neighborhood.name', 'NI') ?: 'NI',
            'shipping_price' => Arr::get($arrayAssocResponse, 'shipping_option.cost', 0) ?: 0,
        ];
    }

    public function getCustomerData(string $httpMethod, array $data)
    {
        $requestData = $data['request'];
        $requestParams = $requestData['params'];
        $requestHeaders = $requestData['headers'];

        $request = new Request($httpMethod, $requestData['url']);

        if (count($requestParams)) {
            $request->addOption(['query' => $requestParams]);
        }
        if (count($requestHeaders)) {
            $request->addOption(['headers' => $requestHeaders]);
        }

        $client = new Client();

        $response = $client->request($request);
        $arrayAssocResponse = json_decode(json_encode($response), true);;

        if (!$response) {
            return [];
        }

        $customer = Arr::get($arrayAssocResponse, 'billing_info', null);

        $result = [];

        foreach ($customer as $i => $data) {

            if (is_array($data)) {

                foreach($data as $index => $value) {
                    if (is_string($value)) {
                        $result[$index] = $value;
                    } elseif (is_array($value) && isset($value['type']) && isset($value['value']) ) {
                        $result[$value['type']] = $value['value'];
                    }
                }

            } elseif (is_string($data)) {
                $result[$i] = $data;
            }

        }

        return $result;
    }

    public function getPaymentData(string $httpMethod, array $data)
    {
        return $this->defaultRequest($httpMethod, $data);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \League\Csv\Exception
     */
    public function sendInvoice(string $httpMethod, array $data)
    {
        $request = new Request($httpMethod, $data['request']['url']);

        $request->addOption([
            'headers' => [
                'Content-Type' => 'multipart/form-data',
            ]
        ]);

        $request->addOption([
            'query' => $data['request']['params'],
        ]);

        return (new Client())->fileRequest(
            $request,
            'fiscal_document',
            $data['request']['body']['filename'],
            $data['request']['body']['content']
        );
    }

    public function getTicket(string $httpMethod, array $data)
    {
        $requestData = $data['request'];
        $requestParams = $requestData['params'];

        $request = new Request($httpMethod, $requestData['url']);

        if (count($requestParams)) {
            $request->addOption(['query' => $requestParams]);
        }

        $client = new Client();

        $response = $client->request($request);
        $ticketData = json_decode(json_encode($response), true);

        if (!$ticketData) {
            return [];
        }

        return [
            'store_ticket_id' => Arr::get($ticketData, 'id', null),
            'type' => Arr::get($ticketData, 'type', null),
            'resource' => Arr::get($ticketData, 'resource', null),
            'resource_id' => Arr::get($ticketData, 'resource_id', null),
            'store_status' => Arr::get($ticketData, 'status', null),
            'palyers' => Arr::get($ticketData, 'players', null),
            'reason_id' => Arr::get($ticketData, 'reason_id', null),
            'stage' => Arr::get($ticketData, 'stage', null),
            'created_at' => Arr::get($ticketData, 'date_created', null),
            'updated_at' => Arr::get($ticketData, 'last_updated', null),
        ];
    }

    public function getTicketMessages(string $httpMethod, array $data)
    {
        $requestData = $data['request'];
        $requestParams = $requestData['params'];

        $request = new Request($httpMethod, $requestData['url']);

        if (count($requestParams)) {
            $request->addOption(['query' => $requestParams]);
        }

        $client = new Client();

        $response = $client->request($request);
        $messages = json_decode(json_encode($response), true);

        if (!$response) {
            return [];
        }

        return array_map(function ($message) {
            return [
                'sender_role' => $message['sender_role'],
                'receiver_role' => $message['receiver_role'],
                'status' => $message['stage'],
                'message' => $message['message'],
                'date_created' => $message['date_created']
            ];
        }, $messages);
    }

    public function getReason(string $httpMethod, array $data)
    {
        $requestData = $data['request'];
        $requestParams = $requestData['params'];

        $request = new Request($httpMethod, $requestData['url']);

        if (count($requestParams)) {
            $request->addOption(['query' => $requestParams]);
        }

        $client = new Client();

        $response = $client->request($request);
        $messages = json_decode(json_encode($response), true);

        if (!$messages) {
            return [];
        }

        return isset($messages['path_from_root']) ? $messages['path_from_root'] : $messages;
    }

    public function sendNewMessage(string $httpMethod, array $data)
    {
        $requestData = $data['request'];
        $requestParams = $requestData['params'];

        $request = new Request($httpMethod, $requestData['url']);

        $request->addOption(['json' => $data['messageBody']]);

        if (count($requestParams)) {
            $request->addOption(['query' => $requestParams]);
        }

        $client = new Client();

        $response = $client->request($request);
        $responseAsAssocArray = json_decode(json_encode($response), true);

        if (!$responseAsAssocArray) {
            return [];
        }

        return $responseAsAssocArray;
    }

    /**
     * @param string $httpMethod'
     * @param array $data
     * @return array
     */
    public function updateExpedition(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => array(),
            'errors'  => array(),
        ];

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {

                if(empty($sku['store_sku'])) {
                    $productsResults['errors'][$sku['sku']][] = ['message' => 'Is empty store skus'];
                }

                foreach ($sku['store_sku'] as $storeSku) {
                    try {

                        $formattedProduct = $this->product->formatProductsToUpdateExpedition($product);

                        if (empty($formattedProduct)) {
                            throw new \Exception('Not found to formatted');
                        }

                        $response = $this->sendProductData($httpMethod, $data, $storeSku, $formattedProduct);
                        $productsResults['success'][$sku['sku']][] = (array)$response;
                    } catch (\Throwable $e) {
                        $productsResults['errors'][$sku['sku']][] = ['message' => $e->getMessage()];
                    }
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod'
     * @param array $data
     * @return array
     */
    public function updateAttributes(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => array(),
            'errors'  => array(),
        ];

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {

                if(empty($sku['store_sku'])) {
                    $productsResults['errors'][$sku['sku']][] = ['message' => 'Is empty store skus'];
                }

                foreach ($sku['store_sku'] as $storeSku) {
                    try {

                        $response = $this->getProductData($data, $storeSku);

                        if($response['status'] != 'active') {
                            throw new \Exception('Product is not active');
                        }
                        
                        $hasVariation =  (empty($response['variations'])) ? false : true;
                        $formattedProduct = $this->product->formatProductsToUpdateAttributes($product, $sku, $hasVariation);

                        if (empty($formattedProduct)) {
                            throw new \Exception('Not found to formatted');
                        }

                        $response = $this->sendProductData($httpMethod, $data, $storeSku, $formattedProduct);
                        $productsResults['success'][$sku['sku']][] = (array)$response;
                    } catch (\Throwable $e) {
                        $productsResults['errors'][$sku['sku']][] = ['message' => $e->getMessage()];
                    }
                }
            }
        }

        return $productsResults;
    }
}
