<?php

namespace Mobly\MarketplaceSdk\Integrators\B2w;

use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;
use Mobly\MarketplaceSdk\Validators\B2w\PriceValidator;
use Illuminate\Validation\ValidationException;

class Price
{

    private $integratorAbstract;

    public function __construct(IntegratorAbstract $integratorAbstract, PriceValidator $validator)
    {
        $this->integratorAbstract = $integratorAbstract;
        $this->validator = $validator;
    }

    /**
     * @param array $data
     * @return array
     */
    public function formatPrices(array $data)
    {
        $formattedProducts = array();

        $products = $data['products'];
        foreach ($products as $product) {
            foreach ($product['skus'] as $sku) {
                if ($this->isValidProduct($sku)) {

                    $formattedProduct = [
                        'sku' => $sku['sku'],
                        'price' => $sku['price'],
                        'promotional_price' => empty($sku['special_price']) ? null : $sku['special_price'],
                    ];

                    $formattedProducts[$sku['sku']][] = $formattedProduct;
                }

            }
        }

        return $formattedProducts;
    }

    public function isValidProduct($product)
    {
        $this->validator->setData($product);
        try {
            $result = $this->validator->validate();
        } catch (ValidationException $exception) {
            foreach ($exception->errors() as $error) {
                foreach ($error as $msg) {
                    $aux[] = $msg;
                }
                $this->integratorAbstract->addErrorsWithKey($aux, $sku['sku']);
            }
            $result = false;
        }
        return $result;
    }
}