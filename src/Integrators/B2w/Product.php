<?php

namespace Mobly\MarketplaceSdk\Integrators\B2w;

use Mobly\MarketplaceSdk\Helpers\Helper;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;
use Mobly\MarketplaceSdk\Validators\B2w\ProductValidator;
use Illuminate\Validation\ValidationException;

class Product
{

    private $integratorAbstract;

    public function __construct(IntegratorAbstract $integratorAbstract, ProductValidator $validator)
    {
        $this->integratorAbstract = $integratorAbstract;
        $this->validator = $validator;
    }

    /**
     * Map - Mobly Manager statuses to SDK
     */
    const STATUSES_MAP = [
        'active' => 'enabled',
        'inactive' => 'disabled'
    ];

    /**
     * @param array $data
     * @return array
     */
    public function formatProducts(array $data)
    {
        $formattedProducts = array();

        $products = $data['products'];
        foreach ($products as $product) {
            if ($this->isValidProduct($product)) {
                foreach ($product['skus'] as $sku) {

                    $productName = Helper::_concatTitle($product['name'], $sku['superAttribute']);

                    $formattedProduct = [
                        'sku' => $sku['sku'],
                        'name' => $productName,
                        'description' => strip_tags($product['description']),
                        'status' => key_exists($sku['status'], self::STATUSES_MAP) ?
                            self::STATUSES_MAP[$sku['status']] :
                            'enabled',
                        'qty' => $sku['quantity'],
                        'price' => $sku['price'],
                        'promotional_price' => empty($sku['special_price']) ? null : $sku['special_price'],
                        'cost' => 0,
                        'weight' => !empty($product['weight']) ? $product['weight'] : 1,
                        'height' => !empty($product['height']) ? $product['height'] : 1,
                        'width' => !empty($product['width']) ? $product['width'] : 1,
                        'length' => !empty($product['length']) ? $product['length'] : 1,
                        'brand' => $product['brand'],
                        'ean' => $sku['ean'],
                        'nbm' => 'foo'
                    ];

                    $partsPathCategory = explode('##', $product['path_category']);
                    array_pop($partsPathCategory);
                    array_shift($partsPathCategory);
                    $pathCategory = implode(' > ', $partsPathCategory);
                    $formattedProduct['categories'][] = [
                        'code' => $product['external_id'],
                        'name' => $pathCategory
                    ];

                    foreach ($product['images'] as $image) {
                        $formattedProduct['images'][] = $image;
                    }

                    foreach ($product['attributes'] as $key => $attribute) {
                        $formattedProduct['specifications'][] = [
                            'key' => $key,
                            'value' => $attribute
                        ];
                    }


                    $formattedProducts[$sku['sku']][] = $formattedProduct;
                }

            }
        }

        return $formattedProducts;
    }

    /**
     * @param $data
     * @return array
     */
    public function transfProductsStore($data)
    {
        $formattedProducts = [];

        if (isset($data[0]['products'])) {
            foreach ($data[0]['products'] as $product) {
                $formattedProducts[] = [
                    'sku' => $product['sku'],
                    'stock' => $product['qty'],
                    'price' => $product['price'],
                    'promotional_price' => $product['promotional_price']
                ];
            }
        } else if (isset($data[0]['sku'])) {
            $formattedProducts[] = [
                'sku' => $data[0]['sku'],
                'stock' => $data[0]['qty'],
                'price' => $data[0]['price'],
                'promotional_price' => $data[0]['promotional_price']
            ];
        }


        return $formattedProducts;
    }

    /**
     * @param $data
     * @return array
     */
    public function transfProductStore($data)
    {
        $formattedProducts = [];

        if (isset($data[0]['products'])) {
            foreach ($data[0]['products'] as $product) {
                $formattedProducts[] = [
                    'sku' => $product['sku'],
                    'price' => $product['price'],
                    'stock' => $product['qty'],
                    'promotional_price' => $product['promotional_price'],
                    'specifications' => $product['specifications'],
                ];
            }
        } else if (isset($data[0]['sku'])) {
            $formattedProducts[] = [
                'sku' => $data[0]['sku'],
                'specifications' => $data[0]['specifications'],
            ];
        }


        return $formattedProducts;
    }

    public function isValidProduct($product)
    {
        $result = false;
        $this->validator->setData($product);
        try {
            $result = $this->validator->validate();
        } catch (ValidationException $exception) {
            foreach ($product['skus'] as $sku) {
                foreach ($exception->errors() as $error) {
                    foreach ($error as $msg) {
                        $aux[] = $msg;
                    }
                    $this->integratorAbstract->addErrorsWithKey($aux, $sku['sku']);
                }
            }
            $result = false;
        }
        return $result;
    }
}