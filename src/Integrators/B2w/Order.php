<?php

namespace Mobly\MarketplaceSdk\Integrators\B2w;

use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Mobly\MarketplaceSdk\Helpers\Helper;

class Order
{
    /**
     * @param array $orders
     * @return mixed
     * @throws \Exception
     */
    public function transformOrders(array $orders)
    {
        /**
         * for e-mail blacklist
         */
        $prefix = 'b2w-';

        $result['orders'] = [];

        try {
            foreach ($orders as $order) {

                $phone = null;
                if(count($order['customer']['phones'])) {
                    $phone = $order['customer']['phones'][0];
                }

                $store_order_number = explode('-', $order['code']);

                list($firstName, $lastName) = Helper::formatName($order['customer']['name']);

                $orderTransform = [
                    'store_order_number'    => $store_order_number[1],
                    'full_store_order_number' => $order['code'],
                    'store_last_updated_at' => $order['updated_at'],
                    'store_order_status'    => $order['status']['code'],
                    'customer_first_name'   => $firstName,
                    'customer_last_name'    => $lastName,
                    'customer_document'     => $order['customer']['vat_number'],
                    'customer_email'        => $prefix . $order['customer']['email'],
                    'customer_phone'        => $phone,
                    'customer_gender'       => $order['customer']['gender'],
                    'billing_postcode'      => $order['billing_address']['postcode'],
                    'billing_street'        => $order['billing_address']['street'],
                    'billing_street_number' => $order['billing_address']['number'],
                    'billing_neighborhood'  => $order['billing_address']['neighborhood'],
                    'billing_complement'    => !empty($order['billing_address']['complement']) ?
                        $order['billing_address']['complement'] : $order['billing_address']['detail'],
                    'billing_city'          => $order['billing_address']['city'],
                    'billing_state'         => $order['billing_address']['region'],
                    'billing_country'       => $order['billing_address']['country'],
                    'shipping_postcode'     => $order['shipping_address']['postcode'],
                    'shipping_street'       => $order['shipping_address']['street'],
                    'shipping_street_number'=> $order['shipping_address']['number'],
                    'shipping_neighborhood' => $order['shipping_address']['neighborhood'],
                    'shipping_complement'   => !empty($order['shipping_address']['complement']) ?
                        $order['shipping_address']['complement'] : $order['shipping_address']['detail'],
                    'shipping_city'         => $order['shipping_address']['city'],
                    'shipping_state'        => $order['shipping_address']['region'],
                    'shipping_country'      => $order['shipping_address']['country'],
                    'shipping_price'        => !empty($order['shipping_cost']) ? $order['shipping_cost'] : 0,
                    'total_price'           => !empty($order['total_ordered']) ? $order['total_ordered'] : 0,
                    'items' => [],
                    'site' => strtoupper(str_replace(' ', '', $order['channel'])),
                ];

                if ( !$orderTransform['billing_complement'] ) $orderTransform['billing_complement']  = '';
                if ( !$orderTransform['shipping_complement'] ) $orderTransform['shipping_complement']  = '';

                foreach ($order['items'] as $item) {
                    for($i=1; $i<=$item['qty']; $i++) {
                        $special_price = 0;
                        if ($item['special_price']) {
                            $special_price = $item['special_price'];
                        }
                        if ($item['original_price']) {
                            $special_price = $item['original_price'];
                        }

                        $orderTransform['items'][] = [
                            'store_item_id' => "{$item['id']}_{$i}",
                            'sku' => $item['product_id'],
                            'sku_name' => $item['name'],
                            'paid_price' => $item['special_price'],
                            'unit_price' => $special_price,
                            'shipping_price' => !empty($item['shipping_cost']) ? ($item['shipping_cost'] / $item['qty']) : 0,
                            'quantity' => 1,
                            'total_commission' => 0,
                        ];
                    }

                }

                $extra = array();
                $extra['storeDiscount'] = $order['discount'];

                $extra['StorePurchasedDate'] = isset($order['placed_at']) ? $order['placed_at'] : '';
                if (isset($order['approved_date']) && !empty($order['approved_date'])) {
                    $extra['StoreApprovedDate'] = $order['approved_date'];
                }
                $extra['StoreEstimatedDeliveryDate'] = isset($order['estimated_delivery']) ? $order['estimated_delivery'] : '';

                $orderTransform['extra'] = json_encode($extra);

                $result['orders'][] = $orderTransform;
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $result;
    }
}
