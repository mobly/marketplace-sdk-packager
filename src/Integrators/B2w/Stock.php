<?php

namespace Mobly\MarketplaceSdk\Integrators\B2w;

use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;
use Mobly\MarketplaceSdk\Validators\B2w\StockValidator;
use Illuminate\Validation\ValidationException;

class Stock
{

    private $integratorAbstract;

    public function __construct(IntegratorAbstract $integratorAbstract, StockValidator $validator)
    {
        $this->integratorAbstract = $integratorAbstract;
        $this->validator = $validator;
    }

    /**
     * @param array $data
     * @return array
     */
    public function formatStocks(array $data)
    {
        $formattedProducts = array();

        $products = $data['stocks'];
        foreach ($products as $product) {

            foreach ($product['skus'] as $sku) {
                if ($this->isValidProduct($sku)) {

                    $formattedProduct = [
                        'sku' => $sku['sku'],
                        'qty' => $sku['quantity'],
                    ];

                    $formattedProducts[$sku['sku']][] = $formattedProduct;
                }

            }
        }

        return $formattedProducts;
    }

    public function isValidProduct($product)
    {
        $this->validator->setData($product);
        try {
            $result = $this->validator->validate();
        } catch (ValidationException $exception) {
            foreach ($exception->errors() as $error) {
                foreach ($error as $msg) {
                    $aux[] = $msg;
                }
                $this->integratorAbstract->addErrorsWithKey($aux, $sku['sku']);
            }
            $result = false;
        }
        return $result;
    }
}