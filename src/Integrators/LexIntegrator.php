<?php

namespace Mobly\MarketplaceSdk\Integrators;

use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Mobly\MarketplaceSdk\Integrators\Lex\OrderTransform;
use Mobly\MarketplaceSdk\Integrators\Lex\ProductTransformer;
use Mobly\MarketplaceSdk\Validators\Lex\LexValidator;
use Mobly\MarketplaceSdk\Integrators\Lex\Order;
use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;

class LexIntegrator extends IntegratorAbstract
{
    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function integrate(string $httpMethod, array $data)
    {
        $lexValidator = new LexValidator($data);
        $lexValidator->validate();

        $productTransformer = new ProductTransformer();
        $formattedProduct = $productTransformer->transform($data);

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $formattedProduct,
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function update(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => [],
        ];

        $lexValidator = new LexValidator($data);
        $lexValidator->validate();

        foreach ($data['products'] as $skuProd => $product) {
            foreach ($product['skus'] as $sku) {
                try {

                    $productTransformer = new ProductTransformer();
                    $formattedProduct = $productTransformer->transformUpdate($product);

                    $response = $this->getProductData($data['get-products'], $sku['sku']);

                    $request = new Request(
                        $httpMethod,
                        $this->_makeUrl(
                            $data['request']['url'],
                            [
                                $response['id']
                            ]
                        )
                    );
                    $request->setOptions([
                        'headers' => $data['request']['headers'],
                        'json' => $formattedProduct,
                    ]);

                    $client = new Client();
                    $response = $client->request($request);

                    $productsResults['success'][$sku['sku']][] = (array)$response;
                } catch (\Exception $exception) {
                    $productsResults['errors'][$sku['sku']][] = ['message' => $exception->getMessage()];
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function integratePrice(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => [],
        ];

        $lexValidator = new LexValidator($data);
        $lexValidator->validate();

        foreach ($data['products'] as $skuProd => $product) {
            foreach ($product['skus'] as $sku) {
                try {

                    $productTransformer = new ProductTransformer();
                    $formattedProduct = $productTransformer->transformPrice($sku);

                    $response = $this->getProductData($data['get-products'], $sku['sku']);

                    $request = new Request(
                        $httpMethod,
                        $this->_makeUrl(
                            $data['request']['url'],
                            [
                                $response['id'],
                                $response['variants'][0]['id'],
                            ]
                        )
                    );
                    $request->setOptions([
                        'headers' => $data['request']['headers'],
                        'json' => $formattedProduct,
                    ]);

                    $client = new Client();
                    $response = $client->request($request);

                    $productsResults['success'][$sku['sku']][] = (array)$response;
                } catch (\Exception $exception) {
                    $productsResults['errors'][$sku['sku']][] = ['message' => $exception->getMessage()];
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function integrateStocks(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => [],
        ];

        $lexValidator = new LexValidator($data);
        $lexValidator->validate();

        foreach ($data['products'] as $skuProd => $product) {
            foreach ($product['skus'] as $sku) {
                try {

                    $productTransformer = new ProductTransformer();
                    $formattedProduct = $productTransformer->transformStock($sku);

                    $response = $this->getProductData($data['get-products'], $sku['sku']);

                    $request = new Request(
                        $httpMethod,
                        $this->_makeUrl(
                            $data['request']['url'],
                            [
                                $response['id'],
                                $response['variants'][0]['id'],
                            ]
                        )
                    );

                    $request->setOptions([
                        'headers' => $data['request']['headers'],
                        'json' => $formattedProduct,
                    ]);

                    $client = new Client();
                    $response = $client->request($request);

                    $productsResults['success'][$sku['sku']][] = (array)$response;
                } catch (\Exception $exception) {
                    $productsResults['errors'][$sku['sku']][] = ['message' => $exception->getMessage()];
                }
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getCategories(string $httpMethod, array $data)
    {
        $lexValidator = new LexValidator($data);
        $lexValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers']
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     * @throws \Exception
     */
    public function getStatus(string $httpMethod, array $data)
    {
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getOrders(string $httpMethod, array $data)
    {
        $request = new Request($httpMethod, $data['request']['url']);

        $result['orders'] = [];

        $request->addOption([
            'query' => $data['request']['params'],
            'headers' => $data['request']['headers']
        ]);

        $client = new Client();

        $response = $client->request($request);
        $productTransformer = new OrderTransform();
        $result = $productTransformer->transformOrders($response);
        $result['raw'] = $response;

        return (array)$result;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function getOrder(string $httpMethod, array $data)
    {
        $urlFormated = $data['request']['url'] . "/" . $data['order'];

        $request = new Request($httpMethod, $urlFormated);

        $request->addOption([
            'headers' => $data['request']['headers']
        ]);

        $client = new Client();

        $response = $client->request($request);
        $productTransformer = new OrderTransform();
        $result = $productTransformer->transformOrders([$response]);

        $result['raw'] = $response;

        return (array)$result;
    }

    public function sendInvoice(string $httpMethod, array $data)
    {
        $lexValidator = new LexValidator($data);
        $lexValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $data['request']['data']
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    public function setOrderToShipped(string $httpMethod, array $data)
    {
        $lexValidator = new LexValidator($data);
        $lexValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $data['request']['data']
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed|string
     * @throws \Exception
     */
    public function setOrderToDelivered(string $httpMethod, array $data)
    {
        return $this->simpleRequest($httpMethod, $data);
    }

    /**
     * @param string $url
     * @param array $params
     * @return string
     */
    private function _makeUrl(string $url, array $params)
    {
        return vsprintf($url, $params);
    }

    /**
     * @param $data
     * @param $storeSku
     * @return mixed
     * @throws \Exception
     */
    public function getProductData($data, $storeSku)
    {
        $client = new Client();
        $request = new Request('GET',
            $this->_makeUrl($data['request']['url'], [$storeSku]));

        $request->setOptions([
            'headers' => $data['request']['headers']
        ]);
        $response = $client->request($request);

        return json_decode(json_encode($response), true);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed|string
     * @throws \Exception
     */
    private function simpleRequest(string $httpMethod, array $data)
    {
        $lexValidator = new LexValidator($data);
        $lexValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => !empty($data['request']['data']) ? $data['request']['data'] : NULL,
        ]);

        $client = new Client();

        try {

            $response = $client->request($request);

        } catch (\Throwable $e) {

            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed|\SimpleXMLElement
     * @throws \Exception
     */
    public function getProducts(string $httpMethod, array $data)
    {
        $kevaValidator = new LexValidator($data);
        $kevaValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'query' => $data['request']['params'],
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }
}