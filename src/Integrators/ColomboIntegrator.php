<?php


namespace Mobly\MarketplaceSdk\Integrators;

use League\Csv\Exception;
use Mobly\MarketplaceSdk\Integrators\Colombo\Auth;
use Mobly\MarketplaceSdk\Integrators\Colombo\OrderTransform;
use Mobly\MarketplaceSdk\Integrators\Colombo\InvoiceTransformer;
use Mobly\MarketplaceSdk\Integrators\Colombo\PriceTransformer;
use Mobly\MarketplaceSdk\Integrators\Colombo\DimensionsTransformer;
use Mobly\MarketplaceSdk\Integrators\Colombo\ProductTransformer;
use Mobly\MarketplaceSdk\Integrators\Colombo\ShippedTransformer;
use Mobly\MarketplaceSdk\Integrators\Colombo\DeliveredTransformer;
use Mobly\MarketplaceSdk\Integrators\Colombo\StatusTransformer;
use Mobly\MarketplaceSdk\Integrators\Colombo\StockTransformer;
use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use Mobly\MarketplaceSdk\Validators\Colombo\ColomboValidator;

class ColomboIntegrator extends IntegratorAbstract
{
    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool|mixed
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function integrate(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);
        $colomboValidator->validate();

        $productTransformer = new ProductTransformer();
        $formattedProduct = $productTransformer->transform($data);

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $formattedProduct,
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function updateDimensions(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);
        $colomboValidator->validate();

        $dimensionsTransformer = new DimensionsTransformer();
        $formattedDimension = $dimensionsTransformer->transform($data);

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $formattedDimension,
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed|void
     */
    public function getStatus(string $httpMethod, array $data)
    {

    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function sendInvoice(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);
        $colomboValidator->validate();

        $invoiceTransformer = new InvoiceTransformer();
        $formatedInvoice = $invoiceTransformer->transform($data['request']['data']);

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $formatedInvoice,
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function integrateStocks(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);

        if ($colomboValidator->validate()) {

            $priceTransformer = new StockTransformer($this);
            $formattedStocks = $priceTransformer->transform($data);

            $request = new Request(
                $httpMethod,
                $this->_makeUrl(key($formattedStocks), $data['request']['url'])
            );

            $request->setOptions([
                'headers' => $data['request']['headers'],
                'json' => $formattedStocks[key($formattedStocks)],
            ]);

            $client = new Client();

            $response = (array)$client->request($request);
            return $response;
        }

        return false;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function integratePrice(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);

        if ($colomboValidator->validate()) {

            $priceTransformer = new PriceTransformer();
            $formattedPrices = $priceTransformer->transform($data);

            $request = new Request(
                $httpMethod,
                $this->_makeUrl(key($formattedPrices), $data['request']['url'])
            );

            $request->setOptions([
                'headers' => $data['request']['headers'],
                'json' => $formattedPrices[key($formattedPrices)],
            ]);

            $client = new Client();

            $response = (array)$client->request($request);
            return $response;
        }

        return false;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateStatus(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);

        if ($colomboValidator->validate()) {

            $statusTransformer = new StatusTransformer($this);
            $formattedStatus = $statusTransformer->transform($data);

            $request = new Request(
                $httpMethod,
                $this->_makeUrl(key($formattedStatus), $data['request']['url'])
            );

            $request->setOptions([
                'headers' => $data['request']['headers'],
                'json' => $formattedStatus[key($formattedStatus)],
            ]);

            $client = new Client();

            $response = (array)$client->request($request);
            return $response;
        }

        return false;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);
        $colomboValidator->validate();

        $productTransformer = new ProductTransformer();
        $formattedProduct = $productTransformer->transform($data);

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $formattedProduct,
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function checkProduct(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);
        $colomboValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function getOrder(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);
        if ($colomboValidator->validate()) {
            $request = new Request($httpMethod, $this->_makeUrl($data['order'], $data['request']['url']));

            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $client = new Client();

            $rawResponse = $client->request($request);
            $orders = !empty($rawResponse) ? [json_decode(json_encode($rawResponse), true)] : [];

            $orderTransf = new OrderTransform();
            $response = $orderTransf->transformOrders($orders);
            $response['raw'] = $rawResponse;

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function getOrders(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);
        if ($colomboValidator->validate()) {
            $request = new Request($httpMethod, $this->_makeUrl(null, $data['request']['url']));

            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $request->addOption(['query' => $data['request']['params']]);

            $client = new Client();
            $rawResponse = $client->request($request);
            $orders = !empty($rawResponse) ? json_decode(json_encode($rawResponse), true) : [];

            $response = [];
            if(!empty($orders['content'])) {
                $orderTransf = new OrderTransform();
                $response = $orderTransf->transformOrders($orders['content']);
            }

            $response['raw'] = $rawResponse;

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getOrdersQueue(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);
        if ($colomboValidator->validate()) {
            $request = new Request($httpMethod, $this->_makeUrl(null, $data['request']['url']));

            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $client = new Client();

            $rawResponse = $client->request($request);
            $orders = !empty($rawResponse) ? [json_decode(json_encode($rawResponse), true)] : [];

            $orderTransf = new OrderTransform();
            $response = $orderTransf->transformOrders($orders);
            $response['raw'] = $rawResponse;

            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deleteOrderQueue(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);
        if ($colomboValidator->validate()) {

            $request = new Request($httpMethod, $this->_makeUrl($data['order'], $data['request']['url']));
            $request->setOptions([
                'headers' => $data['request']['headers'],
            ]);

            $client = new Client();

            $response = $client->request($request);

            return $response;
        }

    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function setOrderToShipped(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);
        $colomboValidator->validate();

        $shippedTransformer = new ShippedTransformer();
        $formatedShipped = $shippedTransformer->transform($data['request']['data']);

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $formatedShipped,
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function setOrderToDelivered(string $httpMethod, array $data)
    {
        $colomboValidator = new ColomboValidator($data);
        $colomboValidator->validate();

        $deliveredTransformer = new DeliveredTransformer();
        $formatedDelivered = $deliveredTransformer->transform($data['request']['data']);

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $formatedDelivered,
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed|\SimpleXMLElement
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getCategories(string $httpMethod, array $data)
    {
        $cnovaValidator = new ColomboValidator($data);
        $cnovaValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        $client = new Client();
        $response = $client->request($request);

        $result['categories'] = json_decode(json_encode($response), true);
        return $result;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed|\SimpleXMLElement
     * @throws Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getCategoriesAttributes(string $httpMethod, array $data)
    {
        $cnovaValidator = new ColomboValidator($data);
        $cnovaValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        $client = new Client();
        $response = $client->request($request);

        $result['attributes'] = json_decode(json_encode($response), true);
        return $result;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function getAccessToken(string $httpMethod, array $data)
    {
        if (!array_key_exists('apiTokenId', $data['request']['keys'])) {
            throw new \Exception('apiTokenId not found.');
        }

        if (!array_key_exists('apiKey', $data['request']['keys'])) {
            throw new \Exception('apiKey not found.');
        }

        $auth = new Auth(
            $data['request']['url'],
            $data['request']['keys']['apiTokenId'],
            $data['request']['keys']['apiKey']
        );

        $response = $auth->getAccessToken($httpMethod);
        return (array)$response;
    }

    /**
     * @param $replaceParam
     * @param $url
     * @return mixed
     */
    private function _makeUrl($replaceParam, $url)
    {
        return str_replace('%s', $replaceParam, $url);
    }
}
