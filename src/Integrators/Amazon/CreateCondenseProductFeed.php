<?php
/**
 * Created by PhpStorm.
 * User: hermes
 * Date: 04/10/18
 * Time: 16:38
 */

namespace Mobly\MarketplaceSdk\Integrators\Amazon;


use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\CondenseProductFeed;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\FeedType;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class CreateCondenseProductFeed extends FeedFactory
{
    /**
     * @param IntegratorAbstract $integratorAbstract
     * @return FeedType
     */
    protected function getFeedInstance(IntegratorAbstract $integratorAbstract): FeedType
    {
        return new CondenseProductFeed($integratorAbstract);
    }
}