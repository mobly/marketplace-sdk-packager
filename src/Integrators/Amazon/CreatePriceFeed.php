<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon;


use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\FeedType;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\PriceFeed;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class CreatePriceFeed extends FeedFactory
{
    /**
     * @return FeedType
     */
    protected function getFeedInstance(IntegratorAbstract $integratorAbstract): FeedType
    {
        return new PriceFeed($integratorAbstract);
    }
}