<?php
/**
 * Created by PhpStorm.
 * User: hermes
 * Date: 04/10/18
 * Time: 16:24
 */

namespace Mobly\MarketplaceSdk\Integrators\Amazon\Feeds;


use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;
use Mobly\MarketplaceSdk\Helpers\Helper;

class CondenseProductFeed extends FeedType
{
    /**
     * @var string
     */
    const MESSAGE_TYPE = 'Product';

    /**
     * @var int
     */
    protected $messageIdIncrementer = 1;

    /**
     * ProductFeed constructor.
     * @param IntegratorAbstract $integratorAbstract
     */
    public function __construct(IntegratorAbstract $integratorAbstract)
    {
        parent::__construct($integratorAbstract);
    }

    /**
     * @param array $data
     * @return mixed|string
     * @throws \DOMException
     */
    public function makeFeed(array $data)
    {
        $formattedProducts = $this->_transform($data['products']);

        return trim($this->formatToXMLNoPurge($data, $formattedProducts, self::MESSAGE_TYPE));
    }

    /**
     * @param array $products
     * @return array
     */
    private function _transform(array $products)
    {
        $formattedProducts = array();
        foreach ($products as $product) {
            $response = $this->productFormatter($product);

            if (empty($response)) {
                continue;
            }

            $formattedProducts[] = $response;
        }

        return $formattedProducts;
    }

    /**
     * @param $product
     * @return array|null
     */
    public function productFormatter($product)
    {
        $formattedProducts = array();
        $formattedProduct = array();
        foreach ($product['skus'] as $sku) {
            try {
                $formattedProduct = [
                    'MessageID' => $this->messageIdIncrementer,
                    'OperationType' => 'Update',
                    'Product' => [
                        'SKU' => $sku['sku'],
                        'StandardProductID' => [
                            'Type' => 'EAN',
                            'Value' => $sku['ean'],
                        ],
                        'Condition' => [
                            'ConditionType' => 'New',
                        ],
                        'DescriptionData' => [
                            'Title' => $this->_concatTitle($product['name'], $sku['superAttribute']),
                            'ShippingWeight' => [
                                ['_attributes' => ['unitOfMeasure' => 'GR']],
                                Helper::convertKilogramToGram($product['dimensions']['weight'])
                            ],
                            'MerchantShippingGroupName' => 'Modelo Padrão da Amazon',
                        ],
                    ]
                ];

                $formattedProducts[] = $formattedProduct;

                $this->messageIdIncrementer++;
            } catch (\Exception $exception) {
                $this->integratorAbstract->addErrorsWithKey($exception->getMessage(), $sku['sku']);
            }
        }

        if (empty($formattedProduct)) {
            return null;
        }

        return $formattedProducts;
    }

    /**
     * @param $title
     * @param $attribute
     * @return string
     */
    private function _concatTitle($title, $attribute)
    {
        if (empty($attribute)) {
            return $title;
        }

        return "{$title} - {$attribute}";
    }
}