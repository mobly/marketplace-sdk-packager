<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon\Feeds;


use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class InventoryFeed extends FeedType
{
    /**
     *
     */
    const MESSAGE_TYPE = 'Inventory';

    /**
     * @var int
     */
    private $messageIdIncrementer = 1;

    /**
     * InventoryFeed constructor.
     * @param IntegratorAbstract $integratorAbstract
     */
    public function __construct(IntegratorAbstract $integratorAbstract)
    {
        parent::__construct($integratorAbstract);
    }

    /**
     * @param array $data
     * @return mixed|string
     * @throws \DOMException
     */
    public function makeFeed(array $data)
    {
        $formattedInventory = [];
        foreach ($data['products'] as $product) {
            $formattedInventory[] = $this->inventoryFormatter($product);
        }

        return $this->formatToXML($data, $formattedInventory, self::MESSAGE_TYPE);
    }

    /**
     * @param $product
     * @return array
     */
    public function inventoryFormatter($product)
    {
        $formattedInventory = [];
        foreach ($product['skus'] as $sku) {
            $formatted = [
                'MessageID' => $this->messageIdIncrementer,
                'OperationType' => 'Update',
                'Inventory' => [
                    'SKU' => $sku['sku'],
                    'Quantity' => $sku['quantity'],
                    'FulfillmentLatency' =>
                        isset($sku['chain_time']) ?
                            $this->limitInteger(intval($sku['chain_time'])) : null
                ]
            ];

            if (!isset($sku['chain_time'])) {
                unset($formatted['Inventory']['FulfillmentLatency']);
            }

            $formattedInventory[] = $formatted;

            $this->messageIdIncrementer++;
        }

        return $formattedInventory;
    }

    /**
     * FulfillmentLatency - it can accept any integer value between 1 and 30
     * @param $value
     * @param int $limit
     * @return int
     */
    protected function limitInteger($value, $limit = 30)
    {
        if ($value > $limit) {
            return $limit;
        }

        return $value;
    }
}