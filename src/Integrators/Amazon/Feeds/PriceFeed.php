<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon\Feeds;


use Carbon\Carbon;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class PriceFeed extends FeedType
{
    /**
     *
     */
    const MESSAGE_TYPE = 'Price';

    /**
     * @var int
     */
    private $messageIdIncrementer = 1;

    /**
     * PriceFeed constructor.
     * @param IntegratorAbstract $integratorAbstract
     */
    public function __construct(IntegratorAbstract $integratorAbstract)
    {
        parent::__construct($integratorAbstract);
    }

    /**
     * @param array $data
     * @return mixed|string
     * @throws \DOMException
     */
    public function makeFeed(array $data)
    {
        $formattedPrice = [];
        foreach ($data['products'] as $product) {
            $formattedPrice[] = $this->priceFormatter($product);
        }

        return $this->formatToXML($data, $formattedPrice, self::MESSAGE_TYPE);
    }

    /**
     * @param $product
     * @return array
     */
    public function priceFormatter($product)
    {
        $formattedPrice = [];
        foreach ($product['skus'] as $sku) {
            if (is_null($sku['special_price'])) {
                $formattedPrice[] = $this->priceWithoutSpecialPriceFormatter($sku);
            } else {
                $formattedPrice[] = $this->priceWithSpecialPriceFormatter($sku);
            }

            $this->messageIdIncrementer++;
        }

        return $formattedPrice;
    }

    /**
     * @param $sku
     * @return array
     */
    private function priceWithSpecialPriceFormatter($sku): array
    {
        $formatted = [
            'MessageID' => $this->messageIdIncrementer,
            'Price' => [
                '_attributes' => [
                    'xmlns:xsi' => $this->config['schema_instance'],
                    'xmlns:xsd' => $this->config['schema']
                ],
                'SKU' => $sku['sku'],
                'StandardPrice' => [
                    ['_attributes' => [
                        'currency' => 'BRL'
                    ]],
                    $sku['price']
                ],
                'Sale' => [
                    'StartDate' => Carbon::parse($sku['special_price_start'])->toAtomString(),
                    'EndDate' => Carbon::parse($sku['special_price_end'])->toAtomString(),
                    'SalePrice' => [
                        ['_attributes' => [
                            'currency' => 'BRL'
                        ]],
                        $sku['special_price']
                    ]
                ],
            ]
        ];

        return $formatted;
    }

    /**
     * @param $sku
     * @return array
     */
    private function priceWithoutSpecialPriceFormatter($sku): array
    {
        $formatted = [
            'MessageID' => $this->messageIdIncrementer,
            'Price' => [
                '_attributes' => [
                    'xmlns:xsi' => $this->config['schema_instance'],
                    'xmlns:xsd' => $this->config['schema']
                ],
                'SKU' => $sku['sku'],
                'StandardPrice' => [
                    ['_attributes' => [
                        'currency' => 'BRL'
                    ]],
                    $sku['price']
                ]
            ]
        ];

        return $formatted;
    }
}