<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon\Feeds;


use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class ImagesFeed extends FeedType
{
    /**
     *
     */
    const MESSAGE_TYPE = 'ProductImage';

    /**
     * ImagesFeed constructor.
     * @param IntegratorAbstract $integratorAbstract
     */
    public function __construct(IntegratorAbstract $integratorAbstract)
    {
        parent::__construct($integratorAbstract);
    }

    /**
     * @param array $data
     * @return mixed|string
     * @throws \DOMException
     */
    public function makeFeed(array $data)
    {
        $count = 0;
        $messageId = 1;
        $formattedImages = [];

        foreach ($data['products'] as $product) {
            foreach ($product['skus'] as $sku) {
                try {
                    $limitImages = 0;

                    if(!count($product['images'])) {
                        throw new \Exception('No Images');
                    }

                    foreach ($product['images'] as $image) {
                        if($limitImages >= 7) {
                            break;
                        }

                        $imageType = 0 == $count ? 'Main' : "PT{$count}";

                        $formattedImages[] = [
                            'MessageID' => $messageId,
                            'OperationType' => 'Update',
                            'ProductImage' => [
                                'SKU' => $sku['sku'],
                                'ImageType' => $imageType,
                                'ImageLocation' => $image
                            ]
                        ];

                        $count++;
                        $messageId++;
                        $limitImages++;
                    }
                    $count = 0;
                } catch (\Exception $exception) {
                    $this->integratorAbstract->addErrorsWithKey($exception->getMessage(), $sku['sku']);
                }
            }
        }

        return $this->formatToXML($data, $formattedImages, self::MESSAGE_TYPE);
    }
}