<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon\Feeds;


use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;
use Spatie\ArrayToXml\ArrayToXml;

abstract class FeedType
{
    /**
     * @var array
     */
    protected $config = array(
        'document_version' => '1.01',
        'schema' => 'http://www.w3.org/2001/XMLSchema',
        'schema_location' => 'amznenvelope.xsd',
        'schema_instance' => 'http://www.w3.org/2001/XMLSchema-instance'
    );

    /**
     * @var IntegratorAbstract|null
     */
    protected $integratorAbstract = null;

    /**
     * @param array $products
     * @return mixed
     */
    public abstract function makeFeed(array $products);

    /**
     * FeedType constructor.
     * @param IntegratorAbstract $integratorAbstract
     */
    public function __construct(IntegratorAbstract $integratorAbstract)
    {
        $this->integratorAbstract = $integratorAbstract;
    }

    /**
     * @param array $data
     * @param array $message
     * @param string $messageType
     * @return string
     * @throws \DOMException
     */
    protected function formatToXML(array $data, array $message, string $messageType)
    {
        $header = $this->getHeader($data);

        $formatted = [
            'Header' => $header,
            'MessageType' => $messageType,
            'Message' => $message
        ];

        $dom = (new ArrayToXml($formatted, [
            'rootElementName' => 'AmazonEnvelope',
            '_attributes' => [
                'xmlns:xsi' => $this->config['schema_instance'],
                'xsi:noNamespaceSchemaLocation' => $this->config['schema_location']
            ]
        ]))->toDom();
        $dom->encoding = 'UTF-8';
        $xml = $dom->saveXML();

        return $xml;
    }

    /**
     * @param array $data
     * @param array $message
     * @param string $messageType
     * @return string
     * @throws \DOMException
     */
    protected function formatToXMLNoPurge(array $data, array $message, string $messageType)
    {
        $header = $this->getHeader($data);

        $formatted = [
            'Header' => $header,
            'MessageType' => $messageType,
            'PurgeAndReplace' => 'false',
            'Message' => $message
        ];

        $dom = (new ArrayToXml($formatted, [
            'rootElementName' => 'AmazonEnvelope',
            '_attributes' => [
                'xmlns:xsi' => $this->config['schema_instance'],
                'xsi:noNamespaceSchemaLocation' => $this->config['schema_location']
            ]
        ]))->toDom();
        $dom->encoding = 'UTF-8';
        $xml = $dom->saveXML();

        return $xml;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function getHeader(array $data): array
    {
        return [
            'DocumentVersion' => $this->config['document_version'],
            'MerchantIdentifier' => $data['request']['MerchantIdentifier']
        ];
    }
}