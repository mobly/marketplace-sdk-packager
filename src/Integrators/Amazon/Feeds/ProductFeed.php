<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon\Feeds;


use Carbon\Carbon;
use Illuminate\Http\Response;
use Mobly\MarketplaceSdk\Helpers\Helper;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class ProductFeed extends FeedType
{
    /**
     * @var string
     */
    const MESSAGE_TYPE = 'Product';

    /**
     * @var array
     */
    const CATEGORIES = array(
        'Bed',
        'Bench',
        'Cabinet',
        'Chair',
        'Desk',
        'Dresser',
        'Furniture',
        'FurnitureAndDecor',
        'Headboard',
        'Home',
        'Mattress',
        'Sofa',
        'Table',
    );

    /**
     * @var array
     */
    const COLOR_MAP = array(
        'Amarelo' => 'yellow',
        'Azul' => 'blue',
        'Bege' => 'beige',
        'Branco' => 'white',
        'Bronze' => 'bronze',
        'Cinza' => 'gray',
        'Colorido' => 'burst',
        'Croma' => 'chrome',
        'Latão' => 'brass',
        'Laranja' => 'orange',
        'Marrom' => 'brown',
        'Metalico' => 'metallic',
        'Prata' => 'silver',
        'Preto' => 'black',
        'Rosa' => 'pink',
        'Roxo' => 'purple',
        'Transparente' => 'clear',
        'Verde' => 'green',
        'Vermelho' => 'red',
    );

    /**
     * @var array
     */
    protected $attributesMap = array(
        'Bed' => array(
            'Material' => null
        ),
        'Bench' => array(
            'ColorMap' => null,
            'Color' => null,
            'BaseMaterialType' => null
        ),
        'Cabinet' => array(
            'ColorMap' => null,
            'Color' => null,
            'BaseMaterialType' => null
        ),
        'Chair' => array(
            'ColorMap' => null,
            'Color' => null,
            'BaseMaterialType' => null
        ),
        'Desk' => array(
            'ColorMap' => null,
            'Material' => null,
            'Color' => null
        ),
        'Dresser' => array(
            'ColorMap' => null,
            'Color' => null,
            'BaseMaterialType' => null
        ),
        'Furniture' => array(),
        'FurnitureAndDecor' => array(
            'Material' => null
        ),
        'Headboard' => array(
            'ColorMap' => null,
            'Color' => null,
            'BaseMaterialType' => null
        ),
        'Home' => array(),
        'Mattress' => array(
            'ColorMap' => null,
            'Color' => null
        ),
        'Sofa' => array(
            'ColorMap' => null,
            'Material' => null,
            'Color' => null
        ),
        'Table' => array(
            'ColorMap' => null,
            'Color' => null,
            'BaseMaterialType' => null
        ),
    );

    /**
     * @var int
     */
    protected $messageIdIncrementer = 1;

    /**
     * ProductFeed constructor.
     * @param IntegratorAbstract $integratorAbstract
     */
    public function __construct(IntegratorAbstract $integratorAbstract)
    {
        parent::__construct($integratorAbstract);
    }

    /**
     * @param array $data
     * @return mixed|string
     * @throws \DOMException
     */
    public function makeFeed(array $data)
    {
        $formattedProducts = $this->_transform($data['products']);

        return trim($this->formatToXML($data, $formattedProducts, self::MESSAGE_TYPE));
    }

    /**
     * @param array $products
     * @return array
     */
    private function _transform(array $products)
    {
        $formattedProducts = array();
        foreach ($products as $product) {
            $response = $this->productFormatter($product);

            if (empty($response)) {
                continue;
            }

            $formattedProducts[] = $response;
        }

        return $formattedProducts;
    }

    /**
     * @param $product
     * @return array|null
     */
    public function productFormatter($product)
    {
        $launchDate = Carbon::now()->toAtomString();
        $discontinueDate = Carbon::parse('+99 years')->toAtomString();

        $formattedProducts = array();
        $formattedProduct = array();

        foreach ($product['skus'] as $sku) {
            try {

                $inferredVolume = $product['box_dimensions']['box_weight']; //$this->getVolume($product, $sku['inferred_volume_wms']);

                if (!$inferredVolume) {
                    $this->integratorAbstract->addErrorsWithKey('No box dimensions.', $sku['sku']);
                    continue;
                }

                $formattedProduct = [
                    'MessageID' => $this->messageIdIncrementer,
                    'OperationType' => $sku['hasApproved'] ? 'PartialUpdate' : 'Update',
                    'Product' => [
                        'SKU' => $sku['sku'],
                        'StandardProductID' => [
                            'Type' => 'EAN',
                            'Value' => $sku['ean'],
                        ],
                        'LaunchDate' => $launchDate,
                        'DiscontinueDate' => $discontinueDate,
                        'Condition' => [
                            'ConditionType' => 'New',
                        ],
                        'ItemPackageQuantity' => 1, // valor fixo
                        'NumberOfItems' => 1, // valor fixo
                        'DepthWidthSideToSide' => [
                            ['_attributes' => ['unitOfMeasure' => 'CM']],
                            $product['dimensions']['length']
                        ],
                        'DescriptionData' => [
                            'Title' => Helper::_concatTitle($product['name'], $sku['superAttribute']),
                            'Brand' => $product['brand'],
                            'Description' => $this->_cleanString($product['description'], 2000, 1997),
                            'BulletPoint' => $this->_makeBulletPoint($product, $sku),
                            'ItemDimensions' => [
                                'Length' => [
                                    ['_attributes' => ['unitOfMeasure' => 'CM']],
                                    $product['dimensions']['length']
                                ],
                                'Width' => [
                                    ['_attributes' => ['unitOfMeasure' => 'CM']],
                                    $product['dimensions']['width']
                                ],
                                'Height' => [
                                    ['_attributes' => ['unitOfMeasure' => 'CM']],
                                    $product['dimensions']['height']
                                ],
                                'Weight' => [
                                    ['_attributes' => ['unitOfMeasure' => 'GR']],
                                    Helper::convertKilogramToGram($product['dimensions']['weight'])
                                ],
                            ],
                            'PackageDimensions' => [
                                'Length' => [
                                    ['_attributes' => ['unitOfMeasure' => 'CM']],
                                    $product['box_dimensions']['box_length'] ?? $product['dimensions']['length']
                                ],
                                'Width' => [
                                    ['_attributes' => ['unitOfMeasure' => 'CM']],
                                    $product['box_dimensions']['box_width'] ?? $product['dimensions']['width']
                                ],
                                'Height' => [
                                    ['_attributes' => ['unitOfMeasure' => 'CM']],
                                    $product['box_dimensions']['box_height'] ?? $product['dimensions']['height']
                                ],
                                'Weight' => [
                                    ['_attributes' => ['unitOfMeasure' => 'GR']],
                                    Helper::convertKilogramToGram(
                                        $product['box_dimensions']['box_weight'] ?? $product['dimensions']['weight']
                                    )
                                ],
                            ],
                            'ShippingWeight' => [
                                ['_attributes' => ['unitOfMeasure' => 'GR']],
                                Helper::convertKilogramToGram(
                                    $product['box_dimensions']['box_weight'] ?? $product['dimensions']['weight'])
                            ],
                            //                        'MSRP' => '', // ??
                            'Manufacturer' => 'Mobly',
                            //                        'MfrPartNumber' => '',
                            //                        'SearchTerms' => '',
                            'RecommendedBrowseNode' => $product['store_category_external_id'],
                            'MerchantShippingGroupName' => 'Modelo Padrão da Amazon',
                        ],
                        'ProductData' => $this->_makeProductData($product, $sku)
                    ]
                ];
                
                if (empty($sku['ean']) || (!empty($sku['ean']) && $sku['ean'] == '.')) {
                    unset($formattedProduct['Product']['StandardProductID']);
                }

                $formattedProducts[] = $formattedProduct;

                $this->messageIdIncrementer++;
            } catch (\Exception $exception) {
                $this->integratorAbstract->addErrorsWithKey($exception->getMessage(), $sku['sku']);
            }
        }

        if (empty($formattedProduct)) {
            return null;
        }

        return $formattedProducts;
    }

    /**
     * @param $inferredVolume
     * @param $product
     * @return float|int
     * @throws \Exception
     */
    private function getVolume($product, $inferredVolume)
    {
        if(empty($inferredVolume)){
            foreach ($product['box_dimensions'] as $dimension){
                if(empty($dimension)){
                    return false;
                }
            }
            $inferredVolume = $product['box_dimensions']['box_weight'] *
                $product['box_dimensions']['box_height'] *
                $product['box_dimensions']['box_width'] *
                $product['box_dimensions']['box_length'];
        }

        return $inferredVolume;
    }

    /**
     * @param $product
     * @param $sku
     * @return array
     */
    private function _makeBulletPoint($product, $sku)
    {
        if (!is_null($sku['superAttribute'])) {
            return $sku['superAttribute'];
        }

        $limit = 0;
        $attributes = array();
        foreach ($product['attributes'] as $key => $attribute) {
            if ($limit > 4) {
                break;
            }

            $attributes[] = "{$key}: {$attribute}";
            $limit++;
        }
        return array(array_unique($attributes));
    }

    /**
     * @param $product
     * @param $sku
     * @return array
     * @throws \Exception
     */
    private function _makeProductData($product, $sku)
    {
        $category = $product['category'];

        if (!in_array($category, self::CATEGORIES)) {
            throw new \Exception("SKU: {$sku['sku']} - Category not found.", Response::HTTP_BAD_REQUEST);
        }

        if ($category == 'Furniture') {
            return $this->_makeFurnitureProductData($product);
        }

        return $this->_makeHomeProductData($product);
    }

    /**
     * @param $product
     * @return array
     */
    private function _makeFurnitureProductData($product)
    {
        $category = $product['category'];

        return [
            'Furniture' => [
                'ProductType' => $category
            ]
        ];
    }

    /**
     * @param $product
     * @return array
     * @throws \Exception
     */
    private function _makeHomeProductData($product)
    {
        $category = $product['category'];

        if (array_key_exists('Color', $this->attributesMap[$category]) && !empty($product['attributes']['Cor'])) {
            $color = trim(ucwords(strtolower($product['attributes']['Cor'])));
            $this->attributesMap[$category]['Color'] = $color;
            $this->attributesMap[$category]['ColorMap'] = !empty(self::COLOR_MAP[$color]) ? self::COLOR_MAP[$color] : 'Multi-Colored';
        }

        if (array_key_exists('BaseMaterialType', $this->attributesMap[$category]) && !empty($product['attributes']['Material'])) {
            $this->attributesMap[$category]['BaseMaterialType'] = $this->_cleanString(trim($product['attributes']['Material']), 50, 47);
        }

        if (array_key_exists('Material', $this->attributesMap[$category]) && !empty($product['attributes']['Material'])) {
            $this->attributesMap[$category]['Material'] = $this->_cleanString(trim($product['attributes']['Material']), 50, 47);
        }

        $notFoundAttributes = '';
        foreach ($this->attributesMap[$category] as $key => $attribute) {
            if (is_null($attribute)) {
                $notFoundAttributes .= $key . "|";
            }
        }

        if (!empty($notFoundAttributes)) {
            throw new \Exception("[{$notFoundAttributes}] not found.");
        }

        $aux = array();
        if (array_key_exists('ColorMap', $this->attributesMap[$category])) {
            $aux['ColorMap'] = $this->attributesMap[$category]['ColorMap'];
        }

        if (array_key_exists('Material', $this->attributesMap[$category])) {
            $aux['Material'] = $this->attributesMap[$category]['Material'];
        }

        if (array_key_exists('Color', $this->attributesMap[$category])) {
            $aux['VariationData']['Color'] = $this->attributesMap[$category]['Color'];
        }

        if (array_key_exists('BaseMaterialType', $this->attributesMap[$category])) {
            $aux['BaseMaterialType'] = $this->attributesMap[$category]['BaseMaterialType'];
        }

        if (empty($aux)) {
            $aux = $this->attributesMap[$category];
        }

        $productData = array(
            'Home' => array(
                'ProductType' => array(
                    $category => $aux
                )
            )
        );

        return $productData;
    }

    /**
     * @param string $string
     * @param $max
     * @param $limit
     * @return bool|string
     */
    private function _cleanString(string $string, $max, $limit)
    {
        // remove html tags
        $string = strip_tags($string, '<p><br><ul><li>');

        // 2000 characters max
        if (strlen($string) > $max) {
            $string = substr($string, 0, $limit);
            $string .= '...';
        }

        return $string;
    }

    /**
     * @param $title
     * @param $attribute
     * @return string
     */
    private function _concatTitle($title, $attribute)
    {
        if (empty($attribute)) {
            return $title;
        }

        return "{$title} - {$attribute}";
    }
}