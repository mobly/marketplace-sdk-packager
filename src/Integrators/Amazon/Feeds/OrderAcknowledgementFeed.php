<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon\Feeds;

use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class OrderAcknowledgementFeed extends FeedType
{
    /**
     *
     */
    const MESSAGE_TYPE = 'OrderAcknowledgement';

    /**
     * @var int
     */
    protected $messageIdIncrementer = 1;

    /**
     * OrderAcknowledgementFeed constructor.
     * @param IntegratorAbstract $integratorAbstract
     */
    public function __construct(IntegratorAbstract $integratorAbstract)
    {
        parent::__construct($integratorAbstract);
    }

    /**
     * @param array $data
     * @return mixed|string
     * @throws \DOMException
     */
    public function makeFeed(array $data)
    {
        $formattedOrders = $this->transform($data['request']['orders']);

        return trim($this->formatToXML($data, $formattedOrders, self::MESSAGE_TYPE));
    }

    /**
     * @param array $orders
     * @return mixed
     */
    public function transform(array $orders)
    {
        $formattedOrders = [];

        foreach ($orders as $order) {
            $formattedOrder = [
                'MessageID' => $this->messageIdIncrementer,
                'OperationType' => 'Update',
                'OrderAcknowledgement' => [
                    'AmazonOrderID' => $order['store_order_number'],
                    'MerchantOrderID' => $order['id'],
                    'StatusCode' => 'Success',
                ],
            ];

            $formattedItems = $this->orderItemsFormatter($orders[0]);
            $formattedOrder['OrderAcknowledgement']['Item'] = $formattedItems[0];
            $formattedOrders[] = $formattedOrder;
        }

        return $formattedOrders[0];
    }

    /**
     * @param array $order
     * @return array
     */
    protected function orderItemsFormatter(array $order)
    {
        $formattedItems = [];

        foreach ($order['items'] as $item) {
            $formattedItem = [
                'AmazonOrderItemCode' => $item['store_item_id'],
                'MerchantOrderItemID' => $item['id'],
            ];

            $formattedItems[] = $formattedItem;
        }

        return $formattedItems;
    }
}