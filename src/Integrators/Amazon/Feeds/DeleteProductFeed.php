<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon\Feeds;


use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class DeleteProductFeed extends FeedType
{
    /**
     * @var string
     */
    const MESSAGE_TYPE = 'Product';

    /**
     * @var int
     */
    private $messageIdIncrementer = 1;

    /**
     * InventoryFeed constructor.
     * @param IntegratorAbstract $integratorAbstract
     */
    public function __construct(IntegratorAbstract $integratorAbstract)
    {
        parent::__construct($integratorAbstract);
    }

    /**
     * @param array $data
     * @return mixed|string
     * @throws \DOMException
     */
    public function makeFeed(array $data)
    {
        $formattedDelete = [];

        foreach ($data['products'] as $sku) {
            $formattedDelete[] = $this->deleteFormatter($sku);
        }

        return $this->formatToXML($data, $formattedDelete, self::MESSAGE_TYPE);
    }

    /**
     * @param $sku
     * @return array
     */
    public function deleteFormatter($sku)
    {
        $formatted = [
            'MessageID' => $this->messageIdIncrementer,
            'OperationType' => 'Delete',
            'Product' => [
                'SKU' => $sku
            ]
        ];

        $this->messageIdIncrementer++;

        return $formatted;
    }
}