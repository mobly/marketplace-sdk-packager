<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon\Feeds;

use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class OrderFulfillmentFeed extends FeedType
{
    const MESSAGE_TYPE = 'OrderFulfillment';

    /**
     * @var int
     */
    protected $messageIdIncrementer = 1;

    /**
     * OrderFulfillmentFeed constructor.
     * @param IntegratorAbstract $integratorAbstract
     */
    public function __construct(IntegratorAbstract $integratorAbstract)
    {
        parent::__construct($integratorAbstract);
    }

    /**
     * @param array $data
     * @return mixed|string
     * @throws \DOMException
     */
    public function makeFeed(array $data)
    {
        $formattedOrders = $this->transform($data['request']['orders']);

        return trim($this->formatToXML($data, $formattedOrders, self::MESSAGE_TYPE));
    }

    /**
     * @param array $orders
     * @return array
     * @throws \Exception
     */
    protected function transform(array $orders)
    {
        $formattedOrders = [];

        foreach ($orders as $order) {
            $shippedDate = isset($order['shipped_at']) ? $order['shipped_at'] : $order['updated_at'];
            $formattedOrder = [
                'MessageID' => $this->messageIdIncrementer,
                'OperationType' => 'Update',
                'OrderFulfillment' => [
                    'AmazonOrderID' => $order['store_order_number'],
                    'FulfillmentDate' => $this->getFormattedTimestamp(
                        new \DateTime($shippedDate, new \DateTimeZone('UTC'))
                    ),
                ],
            ];

            $fulfillmentData = [];
            $fulfillmentData['CarrierCode'] = 'Other';
            $fulfillmentData['CarrierName'] = $order['items'][0]['carrier'] ?? 'MoblyHub';

            if (!empty($order['items'][0]['tracking_code'])) {
                $fulfillmentData['ShipperTrackingNumber'] = $order['items'][0]['tracking_code'];
            } else {
                $fulfillmentData['ShipperTrackingNumber'] = $order['internal_order_number'];
            }

            $formattedOrder['OrderFulfillment']['FulfillmentData'] = $fulfillmentData;
            $formattedOrder['OrderFulfillment']['Item'][] = $this->orderItemsFormatter($order);

            $formattedOrders[] = $formattedOrder;

            $this->messageIdIncrementer++;
        }

        return $formattedOrders;
    }

    /**
     * @param array $order
     * @return array
     */
    protected function orderItemsFormatter(array $order)
    {
        $formattedItems = [];

        foreach ($order['items'] as $item) {
            $formattedItem = [
                'AmazonOrderItemCode' => $item['store_item_id'],
                'Quantity' => $item['quantity'],
            ];

            if ($formattedItem['Quantity'] == 1) {
                unset($formattedItem['Quantity']);
            }

            $formattedItems[] = $formattedItem;
        }

        return $formattedItems;
    }

    /**
     * @param $dateTime
     * @return string
     * @throws \Exception
     */
    private function getFormattedTimestamp($dateTime)
    {
        if (!is_object($dateTime)) {
            if (is_string($dateTime)) {
                $dateTime = new \DateTime($dateTime);
            } else {
                throw new \Exception("Invalid date value.");
            }
        } else {
            if (!($dateTime instanceof \DateTime)) {
                throw new \Exception("Invalid date value.");
            }
        }

        return $dateTime->format(DATE_ATOM);
    }
}