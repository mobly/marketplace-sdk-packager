<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon;


use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\FeedType;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

abstract class FeedFactory
{
    /**
     * @param IntegratorAbstract $integratorAbstract
     * @return FeedType
     */
    protected abstract function getFeedInstance(IntegratorAbstract $integratorAbstract): FeedType;

    /**
     * @param array $data
     * @param IntegratorAbstract $integratorAbstract
     * @return mixed
     */
    public function getXMLFeed(array $data, IntegratorAbstract $integratorAbstract)
    {
        $feed = $this->getFeedInstance($integratorAbstract);

        return $feed->makeFeed($data);
    }
}