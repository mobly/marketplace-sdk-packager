<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon;


use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\FeedType;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\ProductFeed;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class CreateProductFeed extends FeedFactory
{
    /**
     * @return FeedType
     */
    protected function getFeedInstance(IntegratorAbstract $integratorAbstract): FeedType
    {
        return new ProductFeed($integratorAbstract);
    }
}