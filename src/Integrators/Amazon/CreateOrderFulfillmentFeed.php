<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon;

use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\FeedType;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\OrderFulfillmentFeed;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class CreateOrderFulfillmentFeed extends FeedFactory
{
    /**
     * @return FeedType
     */
    protected function getFeedInstance(IntegratorAbstract $integratorAbstract): FeedType
    {
        return new OrderFulfillmentFeed($integratorAbstract);
    }
}