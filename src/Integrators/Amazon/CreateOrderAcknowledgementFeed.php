<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon;

use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\FeedType;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\OrderAcknowledgementFeed;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\OrderFulfillmentFeed;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class CreateOrderAcknowledgementFeed extends FeedFactory
{
    /**
     * @return FeedType
     */
    protected function getFeedInstance(IntegratorAbstract $integratorAbstract): FeedType
    {
        return new OrderAcknowledgementFeed($integratorAbstract);
    }
}