<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon;


use Mobly\MarketplaceSdk\Helpers\Helper;

class Auth
{
    private $config = [
        'SignatureMethod' => 'HmacSHA256',
        'SignatureVersion' => '2',
    ];

    /**
     * @param array $parameters
     * @param $key
     * @param string $httpMethod
     * @param null $queuePath
     * @return string
     */
    public function signParameters(array $parameters, $key, string $httpMethod, $queuePath = null)
    {
        $algorithm = $this->config['SignatureMethod'];
        $parameters['SignatureMethod'] = $algorithm;
        $stringToSign = null;

        $stringToSign = $this->calculateStringToSignV2($parameters, $httpMethod, $queuePath);

        return $this->_sign($stringToSign, $key, $algorithm);
    }

    /**
     * @param array $parameters
     * @param string $httpMethod
     * @param null $queuePath
     * @return string
     */
    private function calculateStringToSignV2(array $parameters, string $httpMethod, $queuePath = null)
    {
        $parsedUrl = parse_url('https://mws.amazonservices.com/');
        $endpoint = $parsedUrl['host'];
        if (isset($parsedUrl['port']) && !is_null($parsedUrl['port'])) {
            $endpoint .= ':' . $parsedUrl['port'];
        }

        $data = $httpMethod;
        $data .= "\n";
        $data .= $endpoint;
        $data .= "\n";

        if ($queuePath) {
            $uri = $queuePath;
        } else {
            $uri = "/";
        }

        $uriEncoded = implode("/", array_map(array('Mobly\MarketplaceSdk\Helpers\Helper', 'urlencode'), explode("/", $uri)));
        $data .= $uriEncoded;
        $data .= "\n";
        uksort($parameters, 'strcmp');
        $data .= $this->_getParametersAsString($parameters);

        return $data;
    }

    /**
     * @param array $parameters
     * @return string
     */
    private function _getParametersAsString(array $parameters)
    {
        $queryParameters = [];
        foreach ($parameters as $key => $value) {
            $queryParameters[] = $key . '=' . Helper::urlencode($value);
        }
        return implode('&', $queryParameters);
    }

    /**
     * @param $data
     * @param $key
     * @param $algorithm
     * @return string
     */
    private function _sign($data, $key, $algorithm)
    {
        if ($algorithm === 'HmacSHA1') {
            $hash = 'sha1';
        } else if ($algorithm === 'HmacSHA256') {
            $hash = 'sha256';
        } else {
            throw new Exception ("Non-supported signing method specified");
        }

        return base64_encode(hash_hmac($hash, $data, $key, true));
    }
}