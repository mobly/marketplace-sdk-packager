<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon;


use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\FeedType;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\InventoryFeed;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class CreateInventoryFeed extends FeedFactory
{
    /**
     * @param IntegratorAbstract $integratorAbstract
     * @return FeedType
     */
    protected function getFeedInstance(IntegratorAbstract $integratorAbstract): FeedType
    {
        return new InventoryFeed($integratorAbstract);
    }
}