<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon;


use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\FeedType;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\ImagesFeed;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class CreateImagesFeed extends FeedFactory
{
    /**
     * @param IntegratorAbstract $integratorAbstract
     * @return FeedType
     */
    protected function getFeedInstance(IntegratorAbstract $integratorAbstract): FeedType
    {
        return new ImagesFeed($integratorAbstract);
    }
}