<?php

namespace Mobly\MarketplaceSdk\Integrators\Amazon;


use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\FeedType;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\DeleteProductFeed;
use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;

class CreateDeleteProductFeed extends FeedFactory
{
    /**
     * @param IntegratorAbstract $integratorAbstract
     * @return FeedType
     */
    protected function getFeedInstance(IntegratorAbstract $integratorAbstract): FeedType
    {
        return new DeleteProductFeed($integratorAbstract);
    }
}