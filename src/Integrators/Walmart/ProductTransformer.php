<?php

namespace Mobly\MarketplaceSdk\Integrators\Walmart;

use Mobly\MarketplaceSdk\Integrators\IntegratorAbstract;
use Mobly\MarketplaceSdk\Validators\Walmart\ProductValidator;

class ProductTransformer
{

    private $integratorAbstract;

    public function __construct(IntegratorAbstract $integratorAbstract, ProductValidator $validator)
    {
        $this->integratorAbstract = $integratorAbstract;
        $this->validator = $validator;
    }

    /**
     * @param array $data
     * @return array
     */
    public function transform(array $data)
    {
        $formattedProducts = array();

        $product = array_pop($data['products']);
        foreach ($product['skus'] as $sku)
        {
            if ($this->isValidProduct($product, $sku))
            {
                $formattedProduct = [
                    'name' => $product['name'],
                    'description' => strip_tags($product['description']),
                    'active' => empty($sku['special_price']) ? false : true,
                    'price' => (int) $sku['price'],
                    'priceDiscount' => (int) $sku['special_price'],
                    'quantity' => $sku['quantity'],
                    'ean' => $sku['ean'],
                    'urlImage' => count($product['images']) ? $product['images'][0] : '',
                    'sellerId' => $data['request']['seller_id'],
                    'height' => !empty($product['height']) ? $product['height'] : 1,
                    'length' => !empty($product['length']) ? $product['length'] : 1,
                    'width' => !empty($product['width']) ? $product['width'] : 1,
                    'weight' => !empty($product['weight']) ? $product['weight'] : 1,
                    'brand' => $product['brand'],
                    'sellerSKU' => $sku['sku']
                ];

                $partsPathCategory = explode('##', $product['path_category']);
                array_pop($partsPathCategory);
                array_shift($partsPathCategory);
                $pathCategory = array_pop($partsPathCategory);
                $formattedProduct['category'] = [
                    'name' => $pathCategory,
                    'active' => true,
                    'id_category' => $product['external_id'],
                    'sellerId' => $data['request']['seller_id'],
                ];

                foreach ($product['images'] as $key => $image) {
                    $formattedProduct['images'][] = [
                        'name' => "image{$key}",
                        'url' => $image
                    ];
                }

                foreach ($product['attributes'] as $key => $attribute) {
                    $formattedProduct['specifications'][] = [
                        'name' => $key,
                        'value' => $attribute
                    ];
                }

                $formattedProducts[] = $formattedProduct;
            }
        }
        return $formattedProducts;
    }

    public function isValidProduct($product, $sku)
    {
        $result = false;
        $this->validator->setData($product, $sku);
        try {
            $result = $this->validator->validate();
        } catch (ValidationException $exception) {
            foreach ($product['skus'] as $sku) {
                foreach ($exception->errors() as $error) {
                    foreach ($error as $msg) {
                        $aux[] = $msg;
                    }
                    $this->integratorAbstract->addErrorsWithKey($aux, $sku['sku']);
                }
            }
            $result = false;
        }
        return $result;
    }
}