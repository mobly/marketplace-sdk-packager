<?php
/**
 * Created by PhpStorm.
 * User: hermes
 * Date: 10/12/18
 * Time: 15:28
 */

namespace Mobly\MarketplaceSdk\Integrators;

use Illuminate\Validation\ValidationException;
use League\Csv\Exception;
use Mobly\MarketplaceSdk\Integrators\Magalu\CategoryTransformer;
use Mobly\MarketplaceSdk\Integrators\Magalu\OfferTransformer;
use Mobly\MarketplaceSdk\Integrators\Magalu\OrderTransform;
use Mobly\MarketplaceSdk\Integrators\Magalu\PriceTransformer;
use Mobly\MarketplaceSdk\Integrators\Magalu\ProductTransformer;
use Mobly\MarketplaceSdk\Integrators\Magalu\StockTransformer;
use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use Mobly\MarketplaceSdk\Validators\Magalu\MagaluValidator;

class MagaluIntegrator extends IntegratorAbstract
{
    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Exception
     */
    public function integrate(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);
        $magaluValidator->validate();

        $productTransformer = new ProductTransformer();
        $formattedProduct = $productTransformer->transform($data, true);

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $formattedProduct,
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws Exception
     */
    public function getStatus(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);
        $magaluValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        $client = new Client();
        $response = $this->formatResponseStatusProduct($client->request($request));

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function integrateOffers(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);
        $magaluValidator->validate();

        try {

            $offerTransformer = new OfferTransformer($this);
            $formattedOffer = $offerTransformer->transform($data);

            $request = new Request($httpMethod, $data['request']['url']);
            $request->setOptions([
                'headers' => $data['request']['headers'],
                'json' => $formattedOffer,
            ]);

            $client = new Client();
            $response = (array)$client->request($request);
            $response['errors'] = $this->getErrors();

            return $response;
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $message = json_decode($e->getMessage(), true);
            if(!empty($message['Errors'])) {
                foreach($message['Errors'] as $error) {
                    if(preg_match('/IdSku: ([A-Z|0-9\-]*)$/', $error['Message'], $matches)) {
                        if(!empty(end($matches))) {
                            $response['errors'][end($matches)] = $error['Message'];
                        }
                    }

                }
            }
            return $response;
        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function sendInvoice(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);
        $magaluValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $data['request']['data'],
        ]);

        $client = new Client();

        $response = (array)$client->request($request);
        $response['errors'] = $this->getErrors();;

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function integrateStocks(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);
        $magaluValidator->validate();

        try {

            $stockTransformer = new StockTransformer($this);
            $formattedStocks = $stockTransformer->transform($data);

            $request = new Request($httpMethod, $data['request']['url']);
            $request->setOptions([
                'headers' => $data['request']['headers'],
                'json' => $formattedStocks,
            ]);

            $client = new Client();

            $response = (array)$client->request($request);
            $response['errors'] = $this->getErrors();

            return $response;
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $message = json_decode($e->getMessage(), true);
            if(!empty($message['Errors'])) {
                foreach($message['Errors'] as $error) {
                    if(preg_match('/IdSku: ([A-Z|0-9\-]*)$/', $error['Message'], $matches)) {
                        if(!empty(end($matches))) {
                            $response['errors'][end($matches)] = $error['Message'];
                        }
                    }

                }
            }
            return $response;

        }
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool
     * @throws \Exception
     */
    public function integratePrice(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);
        $magaluValidator->validate();

        try {

            $priceTransformer = new PriceTransformer($this);
            $formattedPrices = $priceTransformer->transform($data);

            $request = new Request($httpMethod, $data['request']['url']);
            $request->setOptions([
                'headers' => $data['request']['headers'],
                'json' => $formattedPrices,
            ]);

            $client = new Client();

            $response = (array)$client->request($request);
            $response['errors'] = $this->getErrors();

            return $response;
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $message = json_decode($e->getMessage(), true);
            if(!empty($message['Errors'])) {
                foreach($message['Errors'] as $error) {
                    if(preg_match('/IdSku: ([A-Z|0-9\-]*)$/', $error['Message'], $matches)) {
                        if(!empty(end($matches))) {
                            $response['errors'][end($matches)] = $error['Message'];
                        }
                    }

                }
            }
            return $response;

        }
    }

    /**
     * @param $response
     * @return array
     */
    public function formatResponseStatusProduct($response)
    {
        $res = [
            'success' => true,
            'status' => $response->BatchInfo->Status,
            'products' => [
                'error' => [],
            ],
        ];

        foreach ($response->ValidationErrors as $error) {
            $res['products']['error'][$error['ObjectId']] = $error['Message'];
        }

        return $res;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function update(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);
        $magaluValidator->validate();

        $productTransformer = new ProductTransformer();
        $formattedProduct = $productTransformer->transform($data);

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $formattedProduct,
        ]);

        $client = new Client();
        $response = (array)$client->request($request);

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array
     */
    public function checkProduct(string $httpMethod, array $data)
    {
        $productsResults = [
            'success' => [],
            'errors' => [],
        ];

        $client = new Client();
        try {
            $request = new Request($httpMethod,
                $this->_makeUrl($data['product']['sku'], $data['request']['url']));

            $request->addOption([
                'headers' => $data['request']['headers'],
            ]);

            $response = $client->request($request);
            if($response->StockQuantity > 0 && $response->Status == true) {
                throw new \Exception('Product active');
            }

            $productsResults['success'][$data['product']['sku']] = (array)$response;
        } catch (\Exception $exception) {
            $productsResults['errors'][$data['product']['sku']] = ['message' => $exception->getMessage()];
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     * @throws Exception
     * @throws ValidationException
     * @throws \Exception
     */
    public function getOrder(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);
        $magaluValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        $client = new Client();
        $rawResponse = $client->request($request);
        $response[] = (array)$rawResponse;

        $orderTransformer = new OrderTransform();
        $response = $orderTransformer->transformOrders($response);
        $response['raw'] = $rawResponse;

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws Exception
     * @throws ValidationException
     * @throws \Exception
     */
    public function getOrders(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);
        $magaluValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
            'query' => $data['request']['params'],
        ]);

        $client = new Client();
        $rawResponse = $client->request($request);
        $orders = [json_decode(json_encode($rawResponse), true)];

        $orderTransformer = new OrderTransform();

        if (empty($orders[0]['Orders'])) {
            $orders[0]['Orders'] = [];
        }

        $response = $orderTransformer->transformOrders($orders[0]['Orders']);
        $response['raw'] = $rawResponse;

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool|mixed
     * @throws ValidationException
     * @throws Exception
     */
    public function getOrdersQueue(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);
        if ($magaluValidator->validate()) {

            $request = new Request($httpMethod, $data['request']['url']);
            $request->setOptions([
                'headers' => $data['request']['headers'],
                'query' => $data['request']['params'],
            ]);

            $client = new Client();

            $response = $client->request($request);

            return $response;
        }

        return false;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool|mixed
     * @throws ValidationException
     * @throws Exception
     */
    public function deleteOrderQueue(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);
        if ($magaluValidator->validate()) {

            $request = new Request($httpMethod, $data['request']['url']);
            $request->setOptions([
                'headers' => $data['request']['headers'],
                'json' => $data['request']['data'],
            ]);

            $client = new Client();

            $response = $client->request($request);

            return $response;
        }

        return false;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws Exception
     */
    public function setOrderToProcessing(string $httpMethod, array $data)
    {
        return $this->simpleRequest($httpMethod, $data);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws Exception
     */
    public function setOrderToShipped(string $httpMethod, array $data)
    {
        return $this->simpleRequest($httpMethod, $data);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws Exception
     */
    public function setOrderToDelivered(string $httpMethod, array $data)
    {
        return $this->simpleRequest($httpMethod, $data);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed|string
     * @throws \Exception
     */
    private function simpleRequest(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);

        $magaluValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $data['request']['data'],
        ]);

        $client = new Client();

        try {
            $response = $client->request($request);
        } catch (\Throwable $e) {
            $error = json_decode($e->getMessage(), true);

            if (!empty($error['Message'])
                && !empty($error['Errors'])
                && $error['Message'] === 'Ocorreu um erro de validação') {

                foreach ($error['Errors'] as $error) {
                    if ($error['Field'] !== 'OrderStatus'
                        && $error['Message'] != 'Não é possível atualizar um pedido para o mesmo status. DELIVERED atualizando para o status: DELIVERED') {
                        continue;
                    }

                    return '';
                }

                throw new \Exception($e->getMessage(), $e->getCode());
            } else {
                throw new \Exception($e->getMessage(), $e->getCode());
            }
        }

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|bool|mixed|
     * @throws ValidationException
     * @throws Exception
     */
    public function integrateCategories(string $httpMethod, array $data)
    {
        $categoriesResults = ['errors' => [], 'success' => []];

        $magaluValidator = new MagaluValidator($data);

        $magaluValidator->validate();
        $categoryTransformer = new CategoryTransformer();
        $formattedCategories = $categoryTransformer->transform($data);

        $client = new Client();

        foreach ($formattedCategories as $category) {

            sleep(1);

            try {

                $request = new Request($httpMethod, $data['request']['url']);
                $request->setOptions([
                    'headers' => $data['request']['headers'],
                    'json' => [$category],
                ]);

                $client->request($request);

                $categoriesResults['success'][] = $category->Id;
            } catch (\Exception $e) {
                $categoriesResults['errors'][] = $category->Id;
            }
        }

        return $categoriesResults;
    }

    /**
     * @param $replaceParam
     * @param $url
     * @return mixed
     */
    private function _makeUrl($replaceParam, $url)
    {
        return str_replace('%s', $replaceParam, $url);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws ValidationException
     * @throws Exception
     */
    public function getProducts(string $httpMethod, array $data)
    {
        $magaluValidator = new MagaluValidator($data);

        $magaluValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
            'query' => $data['request']['query'],
        ]);

        return (new Client())->request($request);
    }

    /**
     * @param string $httpMethod
     * @param $data
     * @return array
     */
    public function getProduct(string $httpMethod, $data)
    {
        $productsResults = [
            'success' => false,
        ];

        $client = new Client();
        try {
            $request = new Request($httpMethod, $data['request']['url']);

            $request->addOption([
                'headers' => $data['request']['headers'],
            ]);

            $response = $client->request($request);

            return (array)$response;
        } catch (\Exception $exception) {
            logConsoleError($exception->getMessage());
            $productsResults['message'] = $exception->getMessage();
        }

        return $productsResults;
    }
}