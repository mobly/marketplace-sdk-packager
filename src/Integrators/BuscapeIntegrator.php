<?php

namespace Mobly\MarketplaceSdk\Integrators;

use Mobly\MarketplaceSdk\Services\HttpConnection\Client;
use Mobly\MarketplaceSdk\Services\HttpConnection\Request;
use Mobly\MarketplaceSdk\Integrators\Buscape\OrderTransformer;
use Mobly\MarketplaceSdk\Integrators\Buscape\ProductTransformer;
use Mobly\MarketplaceSdk\Validators\Buscape\BuscapeValidator;
use Symfony\Component\HttpFoundation\Response;

class BuscapeIntegrator extends IntegratorAbstract
{
    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Throwable
     */
    public function integrate(string $httpMethod, array $data)
    {
        $productsResults = [
            'errors' => [],
        ];

        $transformResult = ProductTransformer::transform($data);
        $productsResults['errors'] = $transformResult['skusWithError'];

        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        $request->addOption([
            'json' => $transformResult['formattedProducts'],
        ]);

        $client = new Client();

        try {
            $client->request($request);
        } catch (\Throwable $ex) {
            if ($ex->getCode() != Response::HTTP_BAD_REQUEST) {
                throw $ex;
            }

            $invalidSkus = json_decode($ex->getMessage(), true);
            foreach ($invalidSkus as $invalidSku) {
                $productsResults['errors'][$invalidSku['sku']][] = $invalidSku['errors'];
            }
        }

        return $productsResults;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Throwable
     */
    public function update(string $httpMethod, array $data)
    {
        $productsResults = [
            'errors' => [],
        ];
        $batchMode = empty($data['request']['batch_mode']) ? null : $data['request']['batch_mode'];

        if ($batchMode) {
            $transformResult = ProductTransformer::transformForBatchUpdate($data);
            $productsResults['errors'] = $transformResult['skusWithError'];
        } else {
            $transformResult = ProductTransformer::transformForUpdate($data);
            $productsResults['errors'] = $transformResult['skusWithError'];

        }


        $request = new Request($httpMethod, $data['request']['url']);
        $request->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        $request->addOption([
            'json' => $transformResult['formattedProducts'],
        ]);

        $client = new Client();

        try {
            $results = $client->request($request);
            foreach ($results as $result) {
                $productsResults['success'][$result->sku] = $result->sku;
            }
        } catch (\Throwable $ex) {
            if ($ex->getCode() != Response::HTTP_BAD_REQUEST) {
                throw $ex;
            }

            $invalidSkus = json_decode($ex->getMessage(), true);
            foreach ($invalidSkus as $invalidSku) {
                $productsResults['errors'][$invalidSku['sku']][] = $invalidSku['errors'];
            }
        }

        return $productsResults;
    }


    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed|void
     */
    public function getStatus(string $httpMethod, array $data)
    {
        // TODO: Implement getOrders() method.
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function getOrder(string $httpMethod, array $data)
    {
        $buscapeValidator = new BuscapeValidator($data);
        $buscapeValidator->validate();

//        $data['request']['url'] = 'https://mobly.free.beeceptor.com/buscape/order';
        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
        ]);

        $client = new Client();
        $rawResponse = $client->request($request);

        $response = OrderTransformer::transform($rawResponse->orders);
        $response['raw'] = $rawResponse;

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     * @throws \Exception
     */
    public function getOrders(string $httpMethod, array $data)
    {
        $buscapeValidator = new BuscapeValidator($data);
        $buscapeValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);
//        $request = new Request($httpMethod, 'http://www.mocky.io/v2/5c955bed3600006300941f3f');

        $request->setOptions([
            'headers' => $data['request']['headers'],
            'query' => $data['request']['params'],
        ]);

        $client = new Client();
        $rawResponse = $client->request($request);

        $response = OrderTransformer::transform($rawResponse->orders);
        $response['raw'] = $rawResponse;

        return $response;
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function integrateOrderAcknowledgement(string $httpMethod, array $data)
    {
        return $this->simpleRequest($httpMethod, $data);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function sendInvoice(string $httpMethod, array $data)
    {
//        $data['request']['url'] = 'https://mobly.free.beeceptor.com/invoice';
        return $this->simpleRequest($httpMethod, $data);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    public function setOrderToShipped(string $httpMethod, array $data)
    {
//        $data['request']['url'] = 'https://mobly.free.beeceptor.com/tracking';
        return $this->simpleRequest($httpMethod, $data);
    }

    /**
     * @param string $httpMethod
     * @param array $data
     * @return array|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     */
    private function simpleRequest(string $httpMethod, array $data)
    {
        $buscapeValidator = new BuscapeValidator($data);
        $buscapeValidator->validate();

        $request = new Request($httpMethod, $data['request']['url']);

        $request->setOptions([
            'headers' => $data['request']['headers'],
            'json' => $data['request']['data'],
        ]);

        $client = new Client();
        $response = $client->request($request);

        return $response;
    }
}