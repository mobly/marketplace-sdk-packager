<?php

namespace Mobly\MarketplaceSdk\Validators\B2w;

class B2wValidator
{
    /**
     * @var array
     */
    protected $values;

    /**
     * B2wValidator constructor.
     * @param array $inputs
     */
    public function __construct(array $inputs)
    {
        $this->values = $inputs;

    }

    /**
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate()
    {
        $validator = \Validator::make($this->values, [
            'request' => 'required',
            'request.url' => 'required',
            'request.headers.X-Accountmanager-Key' => 'required',
            'request.headers.X-Api-Key' => 'required',
            'request.headers.X-User-Email' => 'required',
            'request.headers.Accept' => 'required',
            'request.headers.Content-Type' => 'required',
            'products' => 'required_if:request.type,product',
            'prices' => 'required_if:request.type,prices',
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }
}