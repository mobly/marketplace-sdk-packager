<?php

namespace Mobly\MarketplaceSdk\Validators\Colombo;

class ColomboValidator
{
    /**
     * @var array
     */
    protected $values;

    /**
     * ColomboValidator constructor.
     * @param array $inputs
     */
    public function __construct(array $inputs)
    {
        $this->values = $inputs;
    }

    /**
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate()
    {
        $validator = \Validator::make($this->values, [
            'request' => 'required',
            'request.url' => 'required',
            'request.headers.authorization' => 'required',
            'products' => 'required_if:request.type,product',
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }
}
