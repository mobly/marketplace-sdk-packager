<?php

namespace Mobly\MarketplaceSdk\Validators\Cnova;

class CnovaValidator
{
    /**
     * @var array
     */
    protected $values;

    /**
     * CnovaValidator constructor.
     * @param array $inputs
     */
    public function __construct(array $inputs)
    {
        $this->values = $inputs;
    }

    /**
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate()
    {
        $validator = \Validator::make($this->values, [
            'request' => 'required',
            'request.url' => 'required',
            'request.headers.client_id' => 'required',
            'request.headers.access_token' => 'required',
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }
}