<?php

namespace Mobly\MarketplaceSdk\Validators\Walmart;

class WalmartValidator
{
    /**
     * @var array
     */
    protected $values;

    /**
     * MagaluValidator constructor.
     * @param array $inputs
     */
    public function __construct(array $inputs)
    {
        $this->values = $inputs;
    }

    /**
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate()
    {
        $validator = \Validator::make($this->values, [
            'request' => 'required',
            'request.url' => 'required',
            'products' => 'required_if:request.type,product',
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }
}