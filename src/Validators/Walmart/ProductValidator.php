<?php

namespace Mobly\MarketplaceSdk\Validators\Walmart;

class ProductValidator
{
    /**
     * @var array
     */
    protected $product;
    protected $sku;

    /**
     * @param array $data
     */
    public function setData(array $product, array $sku)
    {
        $this->product = $product;
        $this->sku = $sku;
    }

    /**
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate()
    {
        $this->validateProduct();
        $this->validateSku();
        return true;
    }

    public function validateProduct()
    {
        $validator = \Validator::make($this->product, [
            'name' => 'required',
            'description' => 'required',
            'sku' => 'required',
            'status' => 'required',
            'images' => 'required',
            'attributes' => 'required',
            'brand' => 'required',
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }

    public function validateSku()
    {
        $validator = \Validator::make($this->sku, [
            'ean' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }
}