<?php

namespace Mobly\MarketplaceSdk\Validators\Lex;

class LexValidator
{
    /**
     * @var array
     */
    protected $values;

    /**
     * MagaluValidator constructor.
     * @param array $inputs
     */
    public function __construct(array $inputs)
    {
        $this->values = $inputs;
    }

    /**
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate()
    {
        $validator = \Validator::make($this->values, [
            'request' => 'required',
            'request.url' => 'required',
            'request.headers.Authentication' => 'required',
            'products' => 'required_if:request.type,product',
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }
}