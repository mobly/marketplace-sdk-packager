<?php

namespace Mobly\MarketplaceSdk\Validators\Carrefour;

class OfferValidator
{
    /**
     * @var array
     */
    protected $values;

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->values = $data;
    }

    /**
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate()
    {
        $validator = \Validator::make($this->values, [
            'sku' => 'required',
            'price' => 'required',
            'special_price' => 'nullable',
            'special_price_start' => 'nullable',
            'special_price_end' => 'nullable',
            'status' => 'required',
            'ean' => 'nullable',
            'quantity' => 'required'
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }
}