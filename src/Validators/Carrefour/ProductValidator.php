<?php

namespace Mobly\MarketplaceSdk\Validators\Carrefour;

class ProductValidator
{
    /**
     * @var array
     */
    protected $values;

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->values = $data;
    }

    /**
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate()
    {
        $validator = \Validator::make($this->values, [
            'name' => 'required',
            'description' => 'required',
            'sku' => 'required',
            'status' => 'required',
            'images' => 'required',
            'attributes' => 'required',
            'path_category' => 'required'
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }
}