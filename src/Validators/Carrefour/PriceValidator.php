<?php

namespace Mobly\MarketplaceSdk\Validators\Carrefour;

class PriceValidator
{
    /**
     * @var array
     */
    protected $values;

    /**
     * Validator constructor.
     * @param array $inputs
     */
    public function __construct(array $inputs)
    {
        $this->values = $inputs;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->values = $data;
    }

    /**
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate()
    {
        $validator = \Validator::make($this->values, [
            'sku' => 'required',
            'price' => 'required',
            'special_price' => 'nullable',
            'special_price_start' => 'nullable',
            'special_price_end' => 'nullable'
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }
}