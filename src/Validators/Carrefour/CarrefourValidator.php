<?php

namespace Mobly\MarketplaceSdk\Validators\Carrefour;

class CarrefourValidator
{
    /**
     * @var array
     */
    protected $values;

    /**
     * CarrefourValidator constructor.
     * @param array $inputs
     */
    public function __construct(array $inputs)
    {
        $this->values = $inputs;
    }

    /**
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate()
    {
        $validator = \Validator::make($this->values, [
            'request' => 'required',
            'request.url' => 'required',
            'request.Authorization' => 'required',
            'products' => 'required_if:request.type,product',
            'prices' => 'required_if:request.type,prices',
            'offers' => 'required_if:request.type,offer',
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }
}