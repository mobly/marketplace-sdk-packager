<?php

namespace Mobly\MarketplaceSdk\Validators\Leroy;

use Illuminate\Validation\ValidationException;

class OfferValidator
{
    /**
     * @var array
     */
    protected $values;

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->values = $data;
    }

    /**
     * @return bool
     * @throws ValidationException
     */
    public function validate()
    {
        $validator = \Validator::make($this->values, [
            'sku' => 'required',
            'price' => 'required',
            'special_price' => 'nullable',
            'special_price_start' => 'nullable',
            'special_price_end' => 'nullable',
            'status' => 'required',
            'ean' => 'nullable',
            'quantity' => 'required'
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return true;
    }
}