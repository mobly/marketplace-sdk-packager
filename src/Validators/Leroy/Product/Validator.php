<?php


namespace Mobly\MarketplaceSdk\Validators\Leroy\Product;

use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator as IlluminateValidator;
use Mobly\MarketplaceSdk\Validators\Leroy\AbstractValidator;

class Validator extends AbstractValidator
{
    protected function getRules(): array
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'sku' => 'required',
            'status' => 'required',
            'images' => 'required',
            'attributes' => 'required',
            'path_category' => 'required',
            'dimensions' => 'required',
        ];
    }
}