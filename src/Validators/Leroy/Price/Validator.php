<?php


namespace Mobly\MarketplaceSdk\Validators\Leroy\Price;

use Mobly\MarketplaceSdk\Validators\Leroy\AbstractValidator;

class Validator extends AbstractValidator
{
    protected function getRules(): array
    {
        return [
            'sku' => 'required',
            'price' => 'required',
            'special_price' => 'nullable',
            'special_price_start' => 'nullable',
            'special_price_end' => 'nullable'
        ];
    }
}