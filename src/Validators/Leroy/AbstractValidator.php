<?php


namespace Mobly\MarketplaceSdk\Validators\Leroy;

use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator as IlluminateValidator;

abstract class AbstractValidator
{
    /**
     * @var array
     */
    protected $values;

    /**
     * Validator constructor.
     * @param array $inputs
     */
    public function __construct(array $inputs)
    {
        $this->values = $inputs;
    }

    /**
     * @return bool
     * @throws ValidationException
     */
    public function validate() : bool
    {
        $validator = IlluminateValidator::make($this->values, $this->getRules());

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return true;
    }

    abstract protected function getRules() : array;
}