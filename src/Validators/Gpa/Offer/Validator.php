<?php


namespace Mobly\MarketplaceSdk\Validators\Gpa\Offer;

use Mobly\MarketplaceSdk\Validators\Gpa\AbstractValidator;

class Validator extends AbstractValidator
{
    protected function getRules(): array
    {
        return [
            'sku' => 'required',
            'price' => 'required',
            'special_price' => 'nullable',
            'special_price_start' => 'nullable',
            'special_price_end' => 'nullable',
            'status' => 'required',
            'ean' => 'nullable',
            'quantity' => 'required'
        ];
    }
}