<?php


namespace Mobly\MarketplaceSdk\Validators\Gpa;

class Validator extends AbstractValidator
{
    protected function getRules(): array
    {
        return [
            'request' => 'required',
            'request.url' => 'required',
            'request.shop_key' => 'required',
            'products' => 'required_if:request.type,product',
            'prices' => 'required_if:request.type,prices',
            'offers' => 'required_if:request.type,offer',
        ];
    }
}