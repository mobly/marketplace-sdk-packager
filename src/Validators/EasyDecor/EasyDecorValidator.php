<?php
/**
 * Created by PhpStorm.
 * User: hermes
 * Date: 07/05/19
 * Time: 17:48
 */

namespace Mobly\MarketplaceSdk\Validators\EasyDecor;


class EasyDecorValidator
{
    /**
     * @var array
     */
    protected $values;

    /**
     * EasyDecorValidator constructor.
     * @param array $inputs
     */
    public function __construct(array $inputs)
    {
        $this->values = $inputs;

    }

    /**
     * @return bool
     */
    public function validate()
    {
        $validator = \Validator::make($this->values, [
            'request' => 'required',
            'request.url' => 'required',
            'request.headers.Authorization' => 'required',
            'request.headers.Accept' => 'required',
            'request.headers.Content-Type' => 'required',
            'products' => 'required_if:request.type,product',
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }
}