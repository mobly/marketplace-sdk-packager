<?php
/**
 * Created by PhpStorm.
 * User: hermes
 * Date: 07/05/19
 * Time: 17:49
 */

namespace Mobly\MarketplaceSdk\Validators\EasyDecor;


class ProductValidator
{
    /**
     * @var
     */
    protected $values;

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->values = $data;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $validator = \Validator::make($this->values, [
            'name' => 'required',
            'description' => 'required',
            'sku' => 'required',
            'status' => 'required',
            'images' => 'required',
            'attributes' => 'required',
            'path_category' => 'required'
        ]);

        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }

        return true;
    }
}