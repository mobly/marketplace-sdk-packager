<?php

namespace Mobly\MarketplaceSdk;

use Illuminate\Support\Arr;
use Mobly\MarketplaceSdk\Exceptions\AmazonException;
use Mobly\MarketplaceSdk\Exceptions\CnovaException;
use Mobly\MarketplaceSdk\Integrators\IntegratorFactory;
use Illuminate\Validation\ValidationException;
use League\Csv\CannotInsertRecord;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class IntegratorManager
{
    // Integration methods
    const INTEGRATION_INTEGRATE = 'integrate';
    const INTEGRATION_INTEGRATE_OFFERS = 'integrateOffers';
    const INTEGRATION_INTEGRATE_CONDENSE_PRODUCT = 'integrateCondenseProduct';
    const INTEGRATION_INTEGRATE_IMAGE = 'integrateImage';
    const INTEGRATION_INTEGRATE_PRICE = 'integratePrice';
    const INTEGRATION_INTEGRATE_PRICE_V4 = 'integratePriceV4';
    const INTEGRATTION_INTEGRATE_STOCK_V4 = 'integrateStockv4';
    const INTEGRATION_INTEGRATE_INVENTORY = 'integrateInventory';
    const INTEGRATION_INTEGRATE_ORDER_FULFILLMENT = 'integrateOrderFulfillment';
    const INTEGRATION_INTEGRATE_ORDER_ACKNOWLEDGEMENT = 'integrateOrderAcknowledgement';
    const INTEGRATION_GET_OFFER = 'getOffer';
    const INTEGRATION_SEND_OPT = 'sendOpt';
    const INTEGRATION_STATUS = 'getStatus';
    const INTEGRATION_ORDERS = 'getOrders';
    const INTEGRATION_ORDER = 'getOrder';
    const INTEGRATION_ORDER_ITEM = 'getOrderItems';
    const INTEGRATION_TRACKING_CODE = 'sendTrackingCode';
    const INTEGRATION_INVOICE = 'sendInvoice';
    const INTEGRATION_ORDER_TO_SHIPPED = 'setOrderToShipped';
    const INTEGRATION_ORDER_TO_DELIVERED = 'setOrderToDelivered';
    const INTEGRATION_ORDER_TO_PROCESSING = 'setOrderToProcessing';
    const INTEGRATION_ACCESS_TOKEN = 'getAccessToken';
    const INTEGRATION_REFRESH_ACCESS_TOKEN = 'refreshAccessToken';
    const INTEGRATION_UPDATE = 'update';
    const INTEGRATION_LINK_SKUS = 'linkSkus';
    const INTEGRATION_UPDATE_DESCRIPTION = 'updateDescription';
    const INTEGRATION_SYNC_ERRORS = 'getSyncErrors';
    const INTEGRATION_URLS = 'getURLs';
    const INTEGRATION_PATCH_SYNC_ERRORS = 'patchSyncErrors';
    const INTEGRATION_DELETE_PRODUCTS = 'deleteProducts';
    const INTEGRATION_REQUEST_ORDERS_QUEUE = 'getOrdersQueue';
    const INTEGRATION_DELETE_ORDERS_QUEUE = 'deleteOrderQueue';
    const INTEGRATION_INTEGRATE_STOCKS = 'integrateStocks';
    const INTEGRATION_INTEGRATE_CATEGORIES = 'integrateCategories';
    const INTEGRATION_GET_CATEGORIES_ATTRIBUTES = 'getCategoriesAttributes';
    const INTEGRATION_GET_CATEGORIES = 'getCategories';
    const INTEGRATION_FETCH_ATTRIBUTES = 'fetchAttributes';
    const INTEGRATION_FETCH_ATTRIBUTES_VALUES = 'fetchAttributesValues';
    const INTEGRATION_CHECK_PRODUCT = 'checkProduct';
    const INTEGRATION_CHECK_STOCKS = 'getStocks';
    const INTEGRATION_CHECK_PRICES = 'checkPrice';
    const INTEGRATION_UPDATE_STATUS = 'updateStatus';
    const INTEGRATION_UPDATE_DIMENSIONS = 'updateDimensions';
    const INTEGRATION_PRODUCTS = 'getProducts';
    const INTEGRATION_PRODUCT_ACTIONS = 'productActions';
    const INTEGRATION_STORE_AUTH = 'storeAuth';
    const INTEGRATION_PRODUCT = 'getProduct';
    const INTEGRATION_FIND_PRODUCTS = 'findProducts';
    const INTEGRATION_PRODUCTS_BY_PAGE_CURSOR = 'fetchProductsByPageCursor';
    const INTEGRATION_SHIPMENT_DATA = 'getShipmentData';
    const INTEGRATION_CUSTOMER_DATA = 'getCustomerData';
    const INTEGRATION_PAYMENT_DATA = 'getPaymentData';
    const INTEGRATION_FETCH_TICKET = 'fetchTicket';
    const INTEGRATION_GET_TICKET = 'getTicket';
    const INTEGRATION_GET_TICKET_MESSAGES = 'getTicketMessages';
    const INTEGRATION_GET_REASON = 'getReason';
    const INTEGRATION_SEND_NEW_MESSAGE = 'sendNewMessage';
    const INTEGRATION_UPDATE_EXPEDITION = 'updateExpedition';
    const INTEGRATION_UPDATE_ATTRIBUTES = 'updateAttributes';

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @param array $data
     * @param string $storeCode
     * @param string $integrationMethod
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(array $data, string $storeCode, string $integrationMethod)
    {
        return $this->sellerRequest($data, $storeCode, $integrationMethod, Request::METHOD_GET);
    }

    /**
     * @param array $data
     * @param string $storeCode
     * @param string $integrationMethod
     * @return \Illuminate\Http\JsonResponse
     */
    public function post(array $data, string $storeCode, string $integrationMethod)
    {
        return $this->sellerRequest($data, $storeCode, $integrationMethod, Request::METHOD_POST);
    }

    /**
     * @param array $data
     * @param string $storeCode
     * @param string $integrationMethod
     * @return \Illuminate\Http\JsonResponse
     */
    public function put(array $data, string $storeCode, string $integrationMethod)
    {
        return $this->sellerRequest($data, $storeCode, $integrationMethod, Request::METHOD_PUT);
    }

    /**
     * @param array $data
     * @param string $storeCode
     * @param string $integrationMethod
     * @return \Illuminate\Http\JsonResponse
     */
    public function patch(array $data, string $storeCode, string $integrationMethod)
    {
        return $this->sellerRequest($data, $storeCode, $integrationMethod, Request::METHOD_PATCH);
    }

    /**
     * @param array $data
     * @param string $storeCode
     * @param string $integrationMethod
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(array $data, string $storeCode, string $integrationMethod)
    {
        return $this->sellerRequest($data, $storeCode, $integrationMethod, Request::METHOD_DELETE);
    }

    /**
     * @param array $data
     * @param string $storeCode
     * @param string $integrationMethod
     * @param string $httpMethod
     * @return \Illuminate\Http\JsonResponse
     */
    private function sellerRequest(
        array $data,
        string $storeCode,
        string $integrationMethod,
        string $httpMethod)
    {
        $this->data = array_merge($this->data, $data);

        try {
            $integrator = IntegratorFactory::createIntegrator($storeCode);
            $response = $integrator->{$integrationMethod}($httpMethod, $this->data);

            return response()->json([
                'success' => true,
                'response' => $response,
            ]);
        } catch (ValidationException $ex) {
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage(),
                'errors' => $ex->errors(),
                'status_code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (CannotInsertRecord $ex) {
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage(),
                'records' => $ex->getRecord(),
                'status_code' => $ex->getCode(),
                'store_name' => $storeCode,
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (AmazonException $ex) {
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage(),
                'status_code' => $ex->getCode(),
            ], Response::HTTP_BAD_REQUEST);
        } catch (CnovaException $ex) {
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage(),
                'status_code' => $ex->getCode(),
            ], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $ex) {

            if (
                strpos($ex->getMessage(), 'Já existe um produto com o') !== false ||
                strpos($ex->getMessage(), 'Já existe um sku com o IdSku informado') !== false
            ) {

//                $skus = [];
//
//                foreach(Arr::get($data, 'products' , []) as $sku) {
//                    $skus[] = Arr::get($sku, 'skus.0.sku', null);
//                }
//
//                foreach(Arr::get($data, 'offers' , []) as $sku) {
//                    $skus[] = Arr::get($sku, 'skus.0.sku', null);
//                }

                return response()->json([
                    'success' => true,
                    'response' => [],
                ]);
            }

            //FIX to improve product create for MGL
            if (
                $storeCode == 'MGL' && (
                    strpos($ex->getMessage(), 'Não existe um produto com o') !== false
                )
            ) {
//                foreach(Arr::get($data, 'offers' , []) as $sku) {
//                    $skus[] = Arr::get($sku, 'skus.0.sku', null);
//                }

                return response()->json([
                    'success' => true,
                    'back_to_product_creation' => true,
                    'response' => [],
                ]);
            }

            return response()->json([
                'success' => false,
                'message' => $ex->getMessage(),
                'status_code' => $ex->getCode(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
