# Marketplace SDK packager

This is a packager to integrate connectopus into marketplaces.

## For test after development

Upload your branch to the server and run this command in `marketplace-manager` project  

```bash
php composer_2.phar require "mobly/marketplace-sdk":"dev-[your-branch-name]"
```

### Example
For branch `feature/sm-800`
```bash
php composer_2.phar require "mobly/marketplace-sdk":"dev-feature/sm-800"
```

