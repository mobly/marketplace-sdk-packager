<?php
/**
 * Created by PhpStorm.
 * User: raylan_campos
 * Date: 04/12/18
 * Time: 12:02
 */

namespace Tests\MercadoLivre\Unit;

use Mobly\MarketplaceSdk\Integrators\MercadoLivre\Product;
use PHPUnit\Framework\TestCase;


class ProductTest extends TestCase
{
    public function testFormatProductsSuccess()
    {
        $formatProducts = new Product();
        $response = $formatProducts->formatProducts($this->dataToFormat());
        $this->assertTrue($response == $this->dataFomattedSuccess());
    }

    public function testFormatProductsFail()
    {
        $formatProducts = new Product();
        $response = $formatProducts->formatProducts($this->dataToFormat());
        $this->assertArrayHasKey('message', $response[1][123456]);
    }

    public function testFormatProductsToUpdateSuccess()
    {
        $formatProducts = new Product();
        $response = $formatProducts->formatProductsToUpdate($this->dataToFormat()['products']);
        $this->assertTrue($response == $this->dataFomattedToUpdateSuccess());
    }

    public function testFormatProductsToUpdateFail()
    {
        $formatProducts = new Product();
        $response = $formatProducts->formatProductsToUpdate($this->dataToFormat()['products']);
        $this->assertArrayHasKey('message', $response[1][123456]);
    }

    public function testFormatProductsToUpdateDescriptionSuccess()
    {
        $formatProducts = new Product();
        $response = $formatProducts->formatProductsToUpdateDescription($this->dataToFormat()['products']);
        $this->assertTrue($response == $this->dataFomattedToUpdateDescriptionSuccess());
    }

    public function testFormatProductsToUpdateDescriptionFail()
    {
        $formatProducts = new Product();
        $response = $formatProducts->formatProductsToUpdateDescription($this->dataToFormat()['products']);
        $this->assertArrayHasKey('message', $response[1][123456]);
    }

    public static function dataToFormat()
    {
        return $dataToFormat = [
            'products' => [
                [
                    'name' => 'Product Name',
                    'description' => 'Product Description',
                    'store_category_external_id' => '123456',
                    'images' => [],
                    'dimensions' => [
                        'length' => 12,
                        'width' => 12,
                        'height' => 12,
                        'weight' => 12,
                    ],
                    'skus' => [
                        [
                            'sku' => '123456',
                            'quantity' => 12,
                            'price' => 12,
                            'shipping_mode' => 'me1',
                            'store_sku' => [
                                123
                            ]
                        ]
                    ]
                ]
            ],
            'request' => [
                'listing_types' => [
                    [
                        'listing_type_id' => 12
                    ]
                ]
            ]
        ];
    }

    public static function dataFomattedSuccess()
    {
        return $dataFomattedSuccess = [
            [
                '123456' => [
                    [
                        'status' => 'active',
                        'title' => 'Product Name',
                        'official_store_id' => '124110866',
                        'category_id' => '123456',
                        'condition' => 'new',
                        'price' => 12,
                        'currency_id' => 'BRL',
                        'available_quantity' => 12,
                        'listing_type_id' => [
                            'listing_type_id' => 12
                        ],
                        'description' => [
                            'plain_text' => 'Product Description'
                        ],
                        'pictures' => [],
                        'shipping' => [
                            'mode' => 'me1',
                            'local_pick_up' => false,
                            'dimensions' => '12x12x12,12',
                        ]
                    ]
                ]
            ],
            []
        ];
    }

    public static function dataFomattedToUpdateSuccess()
    {
        return $dataFomattedSuccess = [
            [
                '123456' => [
                    [
                        'title' => 'Product Name',
                        'price' => 12,
                        'currency_id' => 'BRL',
                        'available_quantity' => 12,
                        'plain_text' => 'Product Description',
                        'pictures' => [],
                        'shipping' => [
                            'mode' => 'me1',
                            'local_pick_up' => false,
                            'dimensions' => '12x12x12,12',
                        ],
                        'store_sku' => 123,
                    ]
                ]
            ],
            []
        ];
    }

    public static function dataFomattedToUpdateDescriptionSuccess()
    {
        return $dataFomattedSuccess = [
            [
                '123456' => [
                    [
                        'plain_text' => 'Product Description',
                        'store_sku' => 123,
                    ]
                ]
            ],
            []
        ];
    }

}