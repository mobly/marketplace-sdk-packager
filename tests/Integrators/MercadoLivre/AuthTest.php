<?php
/**
 * Created by PhpStorm.
 * User: raylan_campos
 * Date: 04/12/18
 * Time: 12:02
 */

namespace Tests\MercadoLivre\Unit;

use Mobly\MarketplaceSdk\Integrators\MercadoLivre\Auth;
use PHPUnit\Framework\TestCase;


class AuthTest extends TestCase
{
    public function testAuthSuccess()
    {
        $getAccessToken = new Auth('5802448845229068', 'Vg8qasTG8rGyJjVidZAV2rdMjOwRMLaO');
        $response = $getAccessToken->getAccessToken($this->authParams());
        $this->assertEquals($this->authArrayKeys(), array_keys((array)$response));
    }

    public function testAuthFail()
    {
        $getAccessToken = new Auth('5802448845229068', 'Vg8qasTG8rGyJjVidZAV2rdMjOwRMLaO');
        $response = $getAccessToken->getAccessToken($this->authParams());
        $this->assertArrayHasKey('message', (array)$response);
    }

    public function testRefreshAccessTokenSuccess()
    {
        $getAccessToken = new Auth('5802448845229068', 'Vg8qasTG8rGyJjVidZAV2rdMjOwRMLaO', 'TG-5c0830a4e464e60006612be5-127363430');
        $response = $getAccessToken->refreshAccessToken($this->authParams());
        $this->assertEquals($this->authArrayKeys(), array_keys((array)$response));
    }

    public function testRefreshAccessTokenFalse()
    {
        $getAccessToken = new Auth('5802448845229068', 'Vg8qasTG8rGyJjVidZAV2rdMjOwRMLaO', 'TG-5c0830a4e464e60006612be5-127363430');
        $response = $getAccessToken->refreshAccessToken($this->authParams());
        $this->assertArrayHasKey('message', (array)$response);
    }

    public static function authParams()
    {
        return $authParams = [
            'verify' => '',
            'certPath' => '',
        ];
    }

    public static function authArrayKeys()
    {
        return $authArrayKeys = [
            'access_token',
            'token_type',
            'expires_in',
            'scope',
            'user_id',
            'refresh_token'
        ];
    }

}