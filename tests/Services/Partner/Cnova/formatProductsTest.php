<?php

namespace Tests\Services\Partner\Cnova;

use Mobly\MarketplaceSdk\Integrators\Cnova\ProductTransformer;
use PHPUnit\Framework\TestCase;

class formatProductsTest extends TestCase
{
    public function testEmpty()
    {
        $productEntity = new ProductTransformer();
        $responseProducts = $productEntity->transform($this->getProductsDefault());
        $this->assertNotEmpty($responseProducts[0], 'Empty return');
        return $responseProducts;
    }

    /**
     * @param array $responseProducts
     * @depends testEmpty
     */
    public function testStructure(array $responseProducts)
    {
        foreach ($responseProducts[0] as $success)
        {
            dd($success);
            foreach ($success as $sku) {
                $keysArray = array_keys($sku);
                sort($keysArray);
                $this->assertEquals($this->getStructureExpectedDefault(), $keysArray);
            }
        }
    }

    /**
     * @return array
     */
    public function getStructureExpectedDefault()
    {
        $expectedArray = [
            'idItem',
            'titulo',
            'descricao',
            'marca',
            'idCategoria',
            'skus',
        ];
        sort($expectedArray);
        return $expectedArray;
    }

    /**
     * @return array
     */
    public function getProductsDefault()
    {
        $products = [
            'products' => [[
                'name' => 'Produto 1',
                'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
                'sku' => '2142343253',
                'status' => 1,
                'chain_time' => null,
                'brand' => 'Mobly',
                'images' => [
                    'https://www.lipsum.com/images/advert.png',
                    'https://www.lipsum.com/images/advert.png',
                    'https://www.lipsum.com/images/advert.png',
                    'https://www.lipsum.com/images/advert.png',
                ],
                'path_category' => '##categoria 1##categoria 2##',
                'external_id' => 2,
                'superAttribute' => 'ttt',
                'attributes' => [
                    'cor' => 'branco'
                ],
                'store_category_external_id' => 2,
                'dimensions' => [
                    'weight' => 20,
                    'height' => 25,
                    'width' => 50,
                    'length' => 40,
                ],
                'skus' => [[
                    'sku' => "1",
                    'sku_id' => 12,
                    'status' => 1,
                    'quantity' => 1,
                    'price' => 222.98,
                    'promotional_price' => 0,
                    'cost' => 23,
                    'ean' => "wpeirwer09i99",
                    'nbm' => "teste",
                ]],
            ]]
        ];
        return $products;
    }
}