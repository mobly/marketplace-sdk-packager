<?php

namespace Tests\Services\Partner\Amazon;

use PHPUnit\Framework\TestCase;
use Mobly\MarketplaceSdk\Integrators\AmazonIntegrator;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\PriceFeed;
use Carbon\Carbon;

class CreatePriceFeedTest extends TestCase
{
    public function testCreatePriceFeed()
    {
        $data = [
            'products' => [
                'chain_time' => 25,
                'skus' => [
                    [
                        'sku' => 'SKU_FAKE_1',
                        'quantity' => 10,
                        'special_price' => 10,
                        'price' => 20,
                        'special_price_start' => Carbon::now(),
                        'special_price_end' => Carbon::parse('+2 weeks')
                    ],
                    [
                        'sku' => 'SKU_FAKE_2',
                        'quantity' => 15,
                        'special_price' => 15,
                        'price' => 25,
                        'special_price_start' => Carbon::now(),
                        'special_price_end' => Carbon::parse('+2 weeks')
                    ]
                ]
            ]
        ];

        $amazonIntegrator = new AmazonIntegrator();
        $priceFeed = new PriceFeed($amazonIntegrator);
        $response = $priceFeed->priceFormatter($data['products']);

        $expectedResponse = [
            [
                'MessageID' => 1,
                'Price' => [
                    '_attributes' => [
                        'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                        'xmlns:xsd' => 'http://www.w3.org/2001/XMLSchema'
                    ],
                    'SKU' => 'SKU_FAKE_1',
                    'StandardPrice' => [
                        ['_attributes' => ['currency' => 'BRL']],
                        20
                    ],
                    'Sale' => [
                        'StartDate' => Carbon::now()->toAtomString(),
                        'EndDate' => Carbon::parse('+2 weeks')->toAtomString(),
                        'SalePrice' => [
                            ['_attributes' => ['currency' => 'BRL']],
                            10
                        ]
                    ]
                ]
            ],
            [
                'MessageID' => 2,
                'Price' => [
                    '_attributes' => [
                        'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                        'xmlns:xsd' => 'http://www.w3.org/2001/XMLSchema'
                    ],
                    'SKU' => 'SKU_FAKE_2',
                    'StandardPrice' => [
                        ['_attributes' => ['currency' => 'BRL']],
                        25
                    ],
                    'Sale' => [
                        'StartDate' => Carbon::now()->toAtomString(),
                        'EndDate' => Carbon::parse('+2 weeks')->toAtomString(),
                        'SalePrice' => [
                            ['_attributes' => ['currency' => 'BRL']],
                            15
                        ]
                    ]
                ]
            ]
        ];

        $this->assertEquals($expectedResponse, $response);
    }
}