<?php

namespace Tests\Services\Partner\Amazon;

use PHPUnit\Framework\TestCase;
use Mobly\MarketplaceSdk\Integrators\AmazonIntegrator;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\ImagesFeed;
use Spatie\ArrayToXml\ArrayToXml;

class CreateImagesFeedTest extends TestCase
{
    public function testCreateImagesFeed()
    {
        $data = [
            'request' => [
                'MerchantIdentifier' => 'FAKE_MERCHANT_IDENTIFIER',
            ],
            'products' => [
                'SKU_FAKE_1' => [
                    'skus' => [
                        [
                            'sku' => 'SKU_FAKE_1_A',
                        ],
                    ],
                    'images' => [
                        '/var/www/images/fake_image1.jpg',
                    ],
                ],
            ],
        ];

        $amazonIntegrator = new AmazonIntegrator();
        $imagesFeed = new ImagesFeed($amazonIntegrator);
        $response = $imagesFeed->makeFeed($data);

        $formattedResponse = [
            'Header' => [
                'DocumentVersion' => '1.01',
                'MerchantIdentifier' => 'FAKE_MERCHANT_IDENTIFIER',
            ],
            'MessageType' => 'ProductImage',
            'Message' => [
                'MessageID' => 1,
                'OperationType' => 'Update',
                'ProductImage' => [
                    'SKU' => 'SKU_FAKE_1_A',
                    'ImageType' => 'Main',
                    'ImageLocation' => '/var/www/images/fake_image1.jpg',
                ],
            ],
        ];

        $dom = (new ArrayToXml($formattedResponse, [
            'rootElementName' => 'AmazonEnvelope',
            '_attributes' => [
                'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                'xsi:noNamespaceSchemaLocation' => 'amznenvelope.xsd'
            ]
        ]))->toDom();
        $dom->encoding = 'UTF-8';
        $expectedResponse = $dom->saveXML();

        $this->assertXmlStringEqualsXmlString($expectedResponse, $response);
    }
}