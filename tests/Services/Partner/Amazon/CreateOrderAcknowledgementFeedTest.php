<?php

namespace Tests\Services\Partner\Amazon;

use PHPUnit\Framework\TestCase;
use Mobly\MarketplaceSdk\Integrators\AmazonIntegrator;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\OrderAcknowledgementFeed;

class CreateOrderAcknowledgementFeedTest extends TestCase
{
    public function testCreateOrderAcknowledgementFeed()
    {
        $data = [];

        $amazonIntegrator = new AmazonIntegrator();
        $orderAcknowledgementFeed = new OrderAcknowledgementFeed($amazonIntegrator);
        $response = $orderAcknowledgementFeed->transform($data);

        $expectedResponse = [];

        $this->assertEquals($expectedResponse, $response);
    }
}