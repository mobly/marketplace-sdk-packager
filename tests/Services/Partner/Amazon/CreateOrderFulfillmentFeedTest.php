<?php

namespace Tests\Services\Partner\Amazon;

use PHPUnit\Framework\TestCase;
use Mobly\MarketplaceSdk\Integrators\AmazonIntegrator;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\OrderFulfillmentFeed;
use Spatie\ArrayToXml\ArrayToXml;

class CreateOrderFulfillmentFeedTest extends TestCase
{
    public function testCreateOrderFulfillmentFeed()
    {

        $data = [
            'request' => [
                'MerchantIdentifier' => 'FAKE_MERCHANT_IDENTIFIER',
                'orders' => [
                    [
                        'store_order_number' => '00001111',
                        'shipped_at' => '2018-12-01 00:00:00',
                        'items' => [
                            [
                                'store_item_id' => '0011',
                                'quantity' => 1,
                                'carrier' => 'corr',
                                'tracking_code' => 'AABBCCDD'
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $amazonIntegrator = new AmazonIntegrator();
        $orderFulfillmentFeed = new OrderFulfillmentFeed($amazonIntegrator);
        $response = $orderFulfillmentFeed->makeFeed($data);

        $formattedResponse = [
            'Header' => [
                'DocumentVersion' => '1.01',
                'MerchantIdentifier' => 'FAKE_MERCHANT_IDENTIFIER',
            ],
            'MessageType' => 'OrderFulfillment',
            'Message' => [
                'MessageID' => 1,
                'OperationType' => 'Update',
                'OrderFulfillment' => [
                    'AmazonOrderID' => '00001111',
                    'FulfillmentDate' => '2018-12-01T00:00:00+00:00',
                    'FulfillmentData' => [
                      'CarrierName' => 'corr',
                      'ShipperTrackingNumber' => 'AABBCCDD',
                    ],
                    'Item' => [
                      'AmazonOrderItemCode' => '0011',
                    ],
                ],
            ],
        ];

        $dom = (new ArrayToXml($formattedResponse, [
            'rootElementName' => 'AmazonEnvelope',
            '_attributes' => [
                'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                'xsi:noNamespaceSchemaLocation' => 'amznenvelope.xsd',
            ],
        ]))->toDom();
        $dom->encoding = 'UTF-8';
        $expectedResponse = $dom->saveXML();

        $this->assertXmlStringEqualsXmlString($expectedResponse, $response);
    }
}