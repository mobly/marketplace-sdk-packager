<?php

namespace Tests\Services\Partner\Amazon;

use PHPUnit\Framework\TestCase;
use Mobly\MarketplaceSdk\Integrators\AmazonIntegrator;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\InventoryFeed;

class CreateInventoryFeedTest extends TestCase
{
    public function testCreateInventoryFeed()
    {
        $data = [
            'products' => [
                'chain_time' => 20,
                'skus' => [
                    [
                        'sku' => 'SKU_FAKE_1',
                        'quantity' => 10,
                    ],
                    [
                        'sku' => 'SKU_FAKE_2',
                        'quantity' => 15
                    ]
                ]
            ]
        ];

        $amazonIntegrator = new AmazonIntegrator();
        $inventoryFeed = new InventoryFeed($amazonIntegrator);
        $response = $inventoryFeed->inventoryFormatter($data['products']);

        $expectedResponse = [
            [
                'MessageID' => 1,
                'OperationType' => 'Update',
                'Inventory' => [
                    'SKU' => 'SKU_FAKE_1',
                    'Quantity' => 10,
                    'FulfillmentLatency' => 20
                ]
            ],
            [
                'MessageID' => 2,
                'OperationType' => 'Update',
                'Inventory' => [
                    'SKU' => 'SKU_FAKE_2',
                    'Quantity' => 15,
                    'FulfillmentLatency' => 20
                ]
            ]
        ];

        $this->assertEquals($expectedResponse, $response);
    }
}