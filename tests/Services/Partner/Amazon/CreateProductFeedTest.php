<?php

namespace Tests\Services\Partner\Amazon;

use PHPUnit\Framework\TestCase;
use Mobly\MarketplaceSdk\Integrators\AmazonIntegrator;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\ProductFeed;

class CreateProductFeedTest extends TestCase
{
    public function testCreateProductFeed()
    {
        $product = [];

        $amazonIntegrator = new AmazonIntegrator();
        $productFeed = new ProductFeed($amazonIntegrator);
        $response = $productFeed->productFormatter($product);

        $expectedResponse = []; //todo: mock
        $this->assertEquals($expectedResponse, $response);
    }
}