<?php

namespace Tests\Services\Partner\Amazon;

use PHPUnit\Framework\TestCase;
use Mobly\MarketplaceSdk\Integrators\AmazonIntegrator;
use Mobly\MarketplaceSdk\Integrators\Amazon\Feeds\CondenseProductFeed;

class CreateCondenseProductFeedTest extends TestCase
{
    public function testCreateCondenseProductFeed()
    {
        $product = [];

        $amazonIntegrator = new AmazonIntegrator();
        $condenseProductFeed = new CondenseProductFeed($amazonIntegrator);
        $response = $condenseProductFeed->productFormatter($product);

        $expectedResponse = [];

        $this->assertEquals($expectedResponse, $response);
    }
}