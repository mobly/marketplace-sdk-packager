<?php

namespace Tests\Services\Partner\B2W;


use Mobly\MarketplaceSdk\Integrators\B2w\Order;
use PHPUnit\Framework\TestCase;

class formatOrdersTest extends TestCase
{
    public function testEmpty()
    {
        $orderEntity = new Order();
        $orders = $orderEntity->transformOrders($this->getOrdersDefault());
        $this->assertNotEmpty($orders['orders'], 'No orders');
        return $orders;
    }

    /**
     * @param array $orders
     * @depends testEmpty
     * @return mixed
     */
    public function testOrder(array $orders)
    {
        foreach ($orders['orders'] as $order) {
            $keysOrder = array_keys($order);
            sort($keysOrder);
            $this->assertEquals($this->getStructureOrderExpectedDefault(), $keysOrder);
            return $order;
        }
    }

    /**
     * @param array $order
     * @depends testOrder
     */
    public function testOrderItem(array $order)
    {
        foreach ($order['items'] as $item) {
            $keysItem = array_keys($item);
            sort($keysItem);
            $this->assertEquals($this->getStructureOrderItemExpectedDefault(), $keysItem);
        }
    }

    /**
     * @return array
     */
    public function getStructureOrderExpectedDefault()
    {
        $expectedArray = [
            'store_order_number',
            'store_last_updated_at',
            'store_order_status',
            'customer_first_name',
            'customer_document',
            'customer_email',
            'customer_phone',
            'customer_gender',
            'billing_postcode',
            'billing_street',
            'billing_street_number',
            'billing_neighborhood',
            'billing_complement',
            'billing_city',
            'billing_state',
            'billing_country',
            'shipping_postcode',
            'shipping_street',
            'shipping_street_number',
            'shipping_neighborhood',
            'shipping_complement',
            'shipping_city',
            'shipping_state',
            'shipping_country',
            'shipping_price',
            'total_price',
            'items'
        ];
        sort($expectedArray);
        return $expectedArray;
    }

    /**
     * @return array
     */
    public function getStructureOrderItemExpectedDefault()
    {
        $expectedArray = [
            'store_item_id',
            'sku',
            'sku_name',
            'unit_price',
            'shipping_price',
            'quantity',
            'total_commission'
        ];
        sort($expectedArray);
        return $expectedArray;
    }

    /**
     * @return array
     */
    public function getOrdersDefault() {
        $orders = [[
            "code" => "Marketplace-000000001",
            "channel" => "Marketplace",
            "placed_at" => "2016-06-10T09:46:04-03:00",
            "updated_at" => "2016-06-15T09:46:04-03:00",
            "total_ordered" => 107.68,
            "interest" => 2.69,
            "shipping_cost" => 15.0,
            "shipping_method" => "Econômico",
            "estimated_delivery" => "2016-06-20T09:46:04-03:00",
            "shipping_address" => [
                "street" => "Rua Sacadura Cabral",
                "number" => "130",
                "detail" => "foo",
                "neighborhood" => "Centro",
                "city" => "Rio de Janeiro",
                "region" => "RJ",
                "country" => "BR",
                "postcode" => "20081262",
                "reference" => "próximo hospital municipal",
                "complement" => "Bloco A - Apto 53",
                "phone" => "11 00000000",
                "secondary_phone" => "11 000000000"
            ],
            "billing_address" => [
                "street" => "Rua Sacadura Cabral",
                "number" => "130",
                "detail" => "Sala 404",
                "neighborhood" => "Centro",
                "city" => "Rio de Janeiro",
                "region" => "RJ",
                "country" => "BR",
                "postcode" => "20081262",
                "reference" => "próximo hospital municipal",
                "complement" => "Bloco A - apto 53",
                "full_name" => "foo",
                "secondary_phone" => "11 000000000",
                "phone" => "11 00000000"
            ],
            "customer" => [
                "name" => "Comprador Exemplo",
                "email" => "comprador@exemplo.com.br",
                "date_of_birth" => "1993-03-03",
                "gender" => "male",
                "vat_number" => "76860543817",
                "phones" => [
                    "2137223902",
                    "2137223902",
                    "2137223902"
                ],
                "state_registration" => "100000000001"
            ],
            "items" => [[
                "id" => "sku001-01",
                "product_id" => "SEU SKU",
                "name" => "Produto exemplo",
                "qty" => 1,
                "original_price" => 99.99,
                "special_price" => 89.99,
                "shipping_cost" => "2.0"
            ]],
            "status" => [
                "code" => "shipped",
                "label" => "Pedido enviado",
                "type" => "SHIPPED"
            ],
            "invoices" => [[
                "key" => "44444444444444444444444444444444444444444444",
                "number" => "444444444",
                "line" => "444",
                "issue_date" => "2016-06-13T16:43:07-03:00"
            ]],
            "shipments" => [[
                "code" => "ENVIO-54321",
                "items" => [[
                    "sku" => "SEU SKU",
                    "qty" => 1
                ]],
                "tracks" => [[
                    "code" => "SS123456789BR",
                    "carrier" => "Direct Express logistica Integrada S/A",
                    "method" => "Direct E-Direct"
                ]]
            ]],
            "sync_status" => "SYNCED",
            "calculation_type" => "b2wentregacorreios",
            "shipping_carrier " => "Direct E-Direct",
            "tags" => [[
                "tags" => [
                    "fraud_risk_detected",
                    "fraud_risk_detected",
                    "fraud_risk_detected"
                ]
            ]],
            "payments" => [
                "value" => 29.9,
                "transaction_date" => "foo",
                "status" => "foo",
                "parcels" => "2",
                "method" => "credit_card",
                "description" => "Cartao",
                "card_issuer" => "foo",
                "autorization_id" => "foo"
            ],
            "estimated_delivery_shift" => "2016-06-20T09:46:04-03:00"
        ]];

        return $orders;
    }
}